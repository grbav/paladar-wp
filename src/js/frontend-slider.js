'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {

	//img slider
	if ($('.img-slider').length) { 
		$('.img-slider').owlCarousel({
			items: 1,
			nav: true,
			navText: [ '<svg role="img" class="icon icon-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="icons/icons.svg#icon-arrow-left"></use></svg>',
	                  '<svg role="img" class="icon icon-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="icons/icons.svg#icon-arrow-right"></use></svg>'
	        ],
			dots: true
		});
	}

	//img slider with caption
	if ($('.img-slider-caption').length) { 
		$('.img-slider-caption').owlCarousel({
			items: 1,
			nav: true,
			loop: true,
			navText: [ "<svg role='img' class='icon icon-arrow-left'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='icons/icons.svg#icon-arrow-left'></use></svg>",
	                  "<svg role='img' class='icon icon-arrow-right'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='icons/icons.svg#icon-arrow-right'></use></svg>"
	        ],
			dots: true,
			onChange: subtitleCallback,
			afterInit:subtitleCallback
		});
	}

	//media slider 
	if ($('.media-slider').length) {
		$('.media-slider').on('initialized.owl.carousel changed.owl.carousel', function(e) {
		    if (!e.namespace)  {
		      return;
		    }
		    var carousel = e.relatedTarget;
		    $('.media-slider-count').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
		  }).owlCarousel({
		    items: 1,
			nav: true,
			navText: [ "<svg role='img' class='icon icon-arrow-left'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='icons/icons.svg#icon-arrow-left'></use></svg>",
	                  "<svg role='img' class='icon icon-arrow-right'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='icons/icons.svg#icon-arrow-right'></use></svg>"
	        ],
	    	animateOut: 'fadeOut'
		  });
	}

	
}); //end of document ready


//get picture subtitle for slider
function subtitleCallback(event){
	setTimeout(function(){
		var subtitle_content = $('.img-slider-caption .owl-item.active .picture-subtitle').html();
		$('.subtitle-text').html(subtitle_content);
	}, 1 );
}