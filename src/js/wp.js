'use strict';
/**
 * wp.js
 *
 * Some things needs extra tweaks in WordPress
 */
var $ = window.jQuery;
var $theme_icons = HOUSE.icons;


//ON DOCUMENT READY
$(document).ready(function() {

 	/*
	 	1. Check if site has top bar
	 	2. Hide top bar on scroll
	*/
	if ($('.top-bar').length) {
		$('body').addClass('has-top-bar');

		var topBarHeight = $('.top-bar').outerHeight();

		$(window).on('scroll', function() {
			
			if (topBarHeight < $(window).scrollTop()) {
		
				$('html').addClass('hide-top-bar');

			} else {
				
				$('html').removeClass('hide-top-bar');
			}

		}).trigger('scroll');
	}

	//img slider
	if ($('.img-slider').length) { 
		$('.img-slider').owlCarousel({
			items: 1,
			nav: true,
			navText: [ '<svg role="img" class="icon icon-arrow-left"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + $theme_icons + 'icons.svg#icon-arrow-left"></use></svg>',
	                  '<svg role="img" class="icon icon-arrow-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + $theme_icons + 'icons.svg#icon-arrow-right"></use></svg>'
	        ],
			dots: true
		});
	}

	//img slider with caption
	if ($('.img-slider-caption').length) { 
		$('.img-slider-caption').owlCarousel({
			items: 1,
			nav: true,
			loop: true,
			navText: [ "<svg role='img' class='icon icon-arrow-left'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='" + $theme_icons + "icons.svg#icon-arrow-left'></use></svg>",
	                  "<svg role='img' class='icon icon-arrow-right'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='" + $theme_icons + "icons.svg#icon-arrow-right'></use></svg>"
	        ],
			dots: true,
			onChange: subtitleCallback,
			afterInit:subtitleCallback
		});
	}

	//get picture subtitle for slider
	function subtitleCallback(event){
		setTimeout(function(){
			var subtitle_content = $('.img-slider-caption .owl-item.active .picture-subtitle').html();
			$('.subtitle-text').html(subtitle_content);
		}, 1 );
	}


	//media slider 
	if ($('.media-slider').length) {
		$('.media-slider').on('initialized.owl.carousel changed.owl.carousel', function(e) {
		    if (!e.namespace)  {
		      return;
		    }
		    var carousel = e.relatedTarget;
		    $('.media-slider-count').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
		  }).owlCarousel({
		    items: 1,
			nav: true,
			navText: [ "<svg role='img' class='icon icon-arrow-left'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='"+$theme_icons+"icons.svg#icon-arrow-left'></use></svg>",
	                  "<svg role='img' class='icon icon-arrow-right'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='"+$theme_icons+"icons.svg#icon-arrow-right'></use></svg>"
	        ],
	    	animateOut: 'fadeOut'
		  });
	}


	$('.media-slider-wrap').next().attr('id', 'next');

	// Click on first tab on menu page
	$('.page-template-template-menu .tabs__nav li').first().find('a').click();

	// Remove attr rows from reservation form textarea
	$('#reservation-msg').removeAttr('rows');

	// Remove attr rows from contact form textarea
	$('.page-template-template-contact #content textarea').removeAttr('rows');

	/**
   	 * Call map function if map exists
   	 */
  	if($('#map').length) {
    	initMap();
  	}
	    

});


//WINDOW ONLOAD
$(window).load(function() {

  // WINDOW RESIZE
  $(window).on('resize', function() {

  }).trigger('resize');

});

function initMap() {
	var $lat = parseFloat($('#map').attr('data-lat'));
    var $lng = parseFloat($('#map').attr('data-lng'));
    // Create a map object and specify the DOM element for display.
    var myLatLng = {lat: $lat, lng: $lng};

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: myLatLng
    });

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
    });
}