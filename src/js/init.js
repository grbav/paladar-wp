'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {

	//SVG for Everybody (ie9+, ...)
	svg4everybody();

	var bumpIt = function() {
      $('body').css('margin-bottom', $('.footer-main').height());
    },
    didResize = false;

	bumpIt();

	$(window).resize(function() {
	  didResize = true;
	});
	setInterval(function() {
	  if(didResize) {
	    didResize = false;
	    bumpIt();
	  }
	}, 250);

	//Menu item has children responsive
	$('.menu-item-has-children').on('click', function() {
		$(this).toggleClass('is-active').find('.sub-menu').slideToggle();
	});

	// Add an arrow for mobile navigation
	var mainNavigation = function() {
	    var $menuUl    = $('.menu');
	    var $parent    = $menuUl.find('.menu-item-has-children');
	    var $link      = $('.menu .menu-item-has-children > a');
	    var $iconArrow = '<span class="icon-arrow"></span>';

	    // parent's link text is wrapped in <span>
	    $.each($link, function() {
	        var $text = $(this).html();
	        var $new = '<span>' + $text + '</span> ';

	        $(this).addClass('parent-link').html($new);
	    });

	    //there's an arrow icon appended to parent's link
	    $.each($parent, function() {
	        $(this).find('.parent-link').after($iconArrow);
	    });
	}

	//Call mainNavigation function
  	mainNavigation();


	//search open burger
	$('.js-btn-burger').click(function() {
		$('.responsive-navigation').toggleClass('active');
		$('.burger').toggleClass('active');
	});

	//search open trigger
	$('.js-btn-search').click(function() {
		$('.search-wrap').removeClass('inactive').addClass('active');
		$('.content-overlay').addClass('active');
	});

	//search close on content-overlay
	$('.content-overlay').on('click', function() {
		$('.search-wrap').removeClass('active').addClass('inactive');
		$(this).removeClass('active');
	})

	//search close trigger
	$('.btn-close-search').click(function() {
		$('.search-wrap').removeClass('active').addClass('inactive');
		$('.content-overlay').removeClass('active');
	});

	//Open reservation
	$('.js-reservation-trigger').click(function() {
		$('html, body').animate({ scrollTop: 0}, 1000);
		$('.reservation').addClass('active');
		$('.content-overlay').addClass('active');
	});

	//Close reservation
	$('.btn-close-reservation').click(function() {
		$('.reservation').removeClass('active');
		$('.content-overlay').removeClass('active');
	});

	//newsletter trigger
	$('.js-subscribe-trigger').on('click',function() {
		$('.newsletter').addClass('newsletter-opened');

		//when css animation is completed focus input
		setTimeout(function() {
			$('.newsletter .input').focus();
		}, 1000);
	});

	$('.js-newsletter-close').on('click',function() {
		$('.newsletter').removeClass('newsletter-opened');
	});

	//Back to top button
	$('.back-to-top').on('click', function (e) {
	    e.preventDefault();
	    $("html, body").animate({scrollTop: 0}, "slow");
	    return false;
	});

	//media sliderscroll to next section
    $('.scroll-to-next').on('click',function (e) {
        e.preventDefault();

        var target = this.hash;
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 1000, 'swing', function () {
            window.location.hash = target;
        });
    });


	//selectize plugin call

	  if ($('select').length) {

		    var $select = $('select').selectize();
		    /**
		     * Disable typing into selected input field
		     */
		    $select[0].selectize.$control_input.on( 'keydown', function(e) {
	        	var key = e.charCode || e.keyCode;

	        	if ( key == 8 ) {
		            return true;
	        	} else {
		            e.preventDefault();
	        	}
		    });
		}


	//datepicker default
	if($('.datepicker').length) {
	     $('.datepicker').datepicker({
	        maxDate: "+2Y",
	        showWeek: false,
	        weekHeader: 'Wk',
	        minDate: 0
	    });
	    /*
		Add width to calendar widget to be the
		same as daptepicker form field width
		*/
	     var calendarWidth = $('.datepicker').width();
  		$('.ui-datepicker').css('min-width', calendarWidth);
	}


	//timepicker
	if($('.timepicker').length) {
	     $('.timepicker').timepicker({
	         timeFormat: 'h:mm p',
	         interval: 60,
	         minTime: '10',
	         maxTime: '6:00pm',
	         defaultTime: '11',
	         startTime: '10:00',
	         dynamic: false,
	         dropdown: true,
	         // scrollbar: true
	     });
	}

	//Tabs funtion
	if($('.tabs, .filter-nav').length) {

	    $('.tabs .tabs__item').hide();
	    $('.tabs').each(function(){
	      var $this = $(this);
	      $this.find('.tabs__item:first').show().addClass('is-active');
	      $this.find('tabs__item ul li:first a').addClass('is-active');
	    });

	    $('.tabs__nav li a').on('click', function(){
	        var $this = $(this);
	        var tab_item = $this.parents(".tabs").find('.tabs__item');
	        tab_item.removeClass('is-active');
	        $this.addClass('is-active').parent().siblings().find('a').removeClass('is-active');
	        var currentTab = $this.attr('href');
	        tab_item.hide();
	        $(currentTab).fadeIn('fast').addClass('is-active');
	        return false;
	    });
	}

	//order menu show more or less info
	$('.item-info').on('click', function() {
		$(this).toggleClass('is-active').parents('.order-menu-item-wrap').find('.item-image-full').delay(500).stop().slideToggle();
		$(this).parents('.order-menu-item-wrap').toggleClass('is-active');
		$(this).parents('.order-menu-item-wrap').siblings().removeClass('is-active').find('.item-image-full').slideUp();
		$(this).parents('.order-menu-item-wrap').siblings().find('.item-info').removeClass('is-active');
	});

	$window.on('scroll resize', check_if_in_view);
	$window.trigger('scroll');

	if ($('.popup-gallery').length) {
		$('.popup-gallery').magnificPopup({
		 	delegate: 'a',
		  	type: 'image',
		  	closeBtnInside: false,
		  	removalDelay: 300, //delay removal by X to allow out-animation
		  	mainClass: 'mfp-zoom-in',
		  	closeMarkup: "<button title='%title%' type='button' class='mfp-close mfp-close-btn'></button>",
		    callbacks: {
		        open: function() {
		            //overwrite default prev + next function. Add timeout for css3 crossfade animation
		            $.magnificPopup.instance.next = function() {
		                var self = this;
		                self.wrap.removeClass('mfp-image-loaded');
		                setTimeout(function() { $.magnificPopup.proto.next.call(self); }, 120);
		            }
		            $.magnificPopup.instance.prev = function() {
		                var self = this;
		                self.wrap.removeClass('mfp-image-loaded');
		                setTimeout(function() { $.magnificPopup.proto.prev.call(self); }, 120);
		            }
		        },
		        imageLoadComplete: function() {
		            var self = this;
		            setTimeout(function() { self.wrap.addClass('mfp-image-loaded'); }, 16);
		        }
		    },
		  closeOnContentClick: true,
		  midClick: true, // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},

		});
	}


	// Gallery filter - toggle nav
	$('.filter-responsive-element').click(function() {
		$('.filter-nav').slideToggle();
		$('.filter-responsive-element-burger').toggleClass('is-active');
	});

	// Gallery filter - On page load - get active filter item text and add it to navigation indicator
	(function() {
		var currentItemText = $('.filter-nav li:first-child .filter-button').text();
		$('.filter-responsive-element-holder').text(currentItemText);
	})();

	// Gallery filter - On filter item select change value for navigation holder
	$('.filter-button').on('click', function() {
		var currentItemText = $(this).text();
		$('.filter-responsive-element-holder').text(currentItemText);
		$('.filter-responsive-element-burger').removeClass('is-active');
	});


	// close subscribe message if error occurred
	$('.subscribe-message__close').click(function() {
		$('.subscribe-message-wrap').fadeOut();
	});

	//if user subscribed successfully show message and after 2 seconds automatically fade out popup
	if (!($('.subscribe-message').is('.subscribe-message--error1'))) {
		$('.subscribe-message-wrap').delay(2000).fadeOut();
	}

	//Call function for video play
	  if ( $('.video-wrap').length) {
	    videoToggle();
	  }


	var isScrolled = function() {

		$(window).scroll(function () {
		
	  		var scrollTop = $(window).scrollTop(); //scrool top distance

		    if (scrollTop > 0) {
		      	$('body').addClass('scrolled');
		  	} else {
		      	$('body').removeClass('scrolled');
		  	}

		});
	};

	isScrolled();




}); //end of document ready


//WINDOW ONLOAD
$(window).load(function() {

	var $container = $('.grid').isotope();

  	if ($('.grid').length) {
    	$('.grid').isotope(); //solve problem with initial grid height with responsive elements
  	}


  	//on page load add class is-active to the first filter option
  	$('.filter-nav li:first button').addClass('is-active');

  	// filter items on button click, add class active on selected item
  	$('#filters').on( 'click', 'button', function() {
      	var $this = $(this);
      	var filterValue = $(this).attr('data-filter');
      	$container.isotope({ filter: filterValue });
      	$this.addClass('is-active').parent().siblings().find('button').removeClass('is-active');
  	});

  	var $filterButtons = $('.filter .filter-button');

	//Preloader
	$('.preloader').fadeOut(1200);

  // WINDOW RESIZE
  $(window).on('resize', function() {

  	//Gallery filter - close filter if click outside
	if (window.matchMedia("(max-width: 570px)").matches) {
	    $(document).on('click', function(event) {
	      if (!$(event.target).closest('.filter-responsive-element').length) {
	        $('.filter-nav').stop().slideUp();
	        $('.filter-responsive-element-burger').removeClass('is-active');
	      }
	    });
	}

  	//Alow timeline to go out of main container (to the end of the screen)
  	if ($('.grid-blog').length) {
  	  var screen_width = $(window).width();
  	  var container_width = $('.container').width();
  	  var result = (screen_width - container_width) / 2;
  	  $('.grid-item-double .wp-post-image').css('margin-left', - result);
  	}

  }).trigger('resize');

});


//Check if elment is in the view function
var $animation_elements = $('.animation');
var $window = $(window);

function check_if_in_view() {
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);

  $.each($animation_elements, function() {
    var $element = $(this);
    var element_height = $element.outerHeight();
    var element_top_position = $element.offset().top;
    var element_bottom_position = (element_top_position + element_height);

    //check to see if this current container is within viewport
    if ((element_bottom_position >= window_top_position) &&
      (element_top_position <= window_bottom_position)) {
      $element.addClass('in-view');
    }
  });
}


//On click hide video thumb and play video (used on step 3 page)
  function videoToggle() {

      $('.video-wrap').on("click", function (e) {
        e.preventDefault();
        // get the wrapper
        var wrapper = $(this);
        // get the video url and id
        var url = $(wrapper).attr('data-url');
        var id = $(wrapper).attr('data-id');

        if ( url.indexOf( 'youtu' ) !==-1 ) {
          // set the src
          var src = 'https://www.youtube.com/embed/' + id + '?enablejsapi=1&autoplay=1';
          // whole player
          var player = '<div id="video-wrap" class="embedded embedded--16by9 video-wrapper"><iframe id="video-player" src="' + src + '" frameborder="0" allowfullscreen></iframe></div>';
        } else if ( url.indexOf( 'vimeo' ) !==-1 ) {
          // set the src
          var src = 'https://player.vimeo.com/video/' + id + '?autoplay=1';
          // whole player
          var player = '<div id="video-wrap" class="embedded embedded--16by9 video-wrapper"><iframe id="video-player" src="' + src + '" frameborder="0" allowfullscreen></iframe></div>';
        }

        //hide video image
        $(this).fadeOut(600);

        // play the video
        setTimeout(function() {
          $(wrapper).after(player);

        }, 600);

      });
  }







