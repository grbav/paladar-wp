'use strict';

var $ = window.jQuery;

//ON DOCUMENT READY
$(document).ready(function() {

}); //end of document ready



//WINDOW ONLOAD
$(window).load(function() {

  var $container = $('.grid').isotope();

  if ($('.grid').length) {
    $('.grid').isotope(); //solve problem with initial grid height with responsive elements
  }
    
  
  //on page load add class is-active to the first filter option
  $('.filter-nav li:first button').addClass('is-active');

  // filter items on button click, add class active on selected item
  $('#filters').on( 'click', 'button', function() {
      var $this = $(this);
      var filterValue = $(this).attr('data-filter');
      $container.isotope({ filter: filterValue });
      $this.addClass('is-active').parent().siblings().find('button').removeClass('is-active');
  });

  var $filterButtons = $('.filter .filter-button');

  //masonry function for update filter count
  function updateFilterCounts()  {
    // get filtered item elements
    var itemElems = $container.isotope('getFilteredItemElements');
    var $itemElems = $( itemElems );
    $filterButtons.each( function( i, button ) {
      var $button = $( button );
      var filterValue = $button.attr('data-filter');
      if ( !filterValue ) {
        // do not update 'any' buttons
        return;
      }
      var count = $itemElems.filter( filterValue ).length;
      $button.find('.filter-count').text(count);
    });
  }

  updateFilterCounts();

  // WINDOW RESIZE
  $(window).on('resize', function() {


  }).trigger('resize');

});



