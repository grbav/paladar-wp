<?php
/**
 * The default template for displaying content events.
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>

<div class="grid-item <?php echo house_set_event_class(); ?>">
	<div class="event-item">

		<?php
			/**
			 * Get the featured image
			 * if one is set
			 */
			if ( has_post_thumbnail() ) :

				echo '<a href="' . get_permalink() . '"><div class="event-item-image" style="background-image: url(' . get_the_post_thumbnail_url( get_the_ID(), 'large' ) . ');"></div></a><!-- /.event-item-image -->';

			endif;
		?>

		<div class="event-time">
			<time><?php echo get_the_date(); ?></time>
			<time>
				<?php
					/**
					 * Get starting time, which is the post publish time
					 */
					echo get_post_time( 'h:i A' );
					/**
					 * Get ending time, if any
					 */
					acf_field( 'end_time', true, ' - ' );
				?>
			</time>
		</div><!-- /.event-date -->

		<div class="event-desc">

			<?php
				/**
				 * Get event's title
				 */
				$title = esc_attr( sprintf( __( 'Permalink to %s', 'house' ), the_title_attribute( 'echo=0' ) ) );
				the_title( sprintf( '<h3 class="event-title"><a href="%1$s" title="%2$s" rel="bookmark">', esc_url( get_permalink() ), $title ), '</a></h3>' );

				/**
				 * Get location (taxonomies)
				 */
				acf_field( 'event_location', true, '<small class="event-location">', '</small>' );
			?>

		</div><!-- /.event-desc -->
		<div class="event-more-info">
			<a href="<?php the_permalink(); ?>"><?php _e( 'View details', 'house' ); ?></a>

			<?php
				/**
				 * Get share links
				 */
				echo house_share_post( 'event' );
			?>
		</div><!-- /.event-more-info -->

	</div><!-- /.event-item -->
</div>