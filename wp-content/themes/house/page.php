<?php
/**
 * The template for displaying all pages by default.
 *
 * This template is without sidebar. For adding sidebars on each side, use page templates.
 *
 */
get_header(); ?>

	<div class="container">
		<div class="entry-content">

			<?php
				/**
				 * Get flexible content fields if any
				 */
				if ( function_exists( 'get_field' ) && get_field( 'default_content_fields' ) ) :

					get_template_part( 'partials/flexible-templates/sections', 'default' );

				endif; // function_exists( 'get_field' ) && get_field( 'default_sections' ) ?>

		</div>
	</div><!-- /.container -->

<?php get_footer();