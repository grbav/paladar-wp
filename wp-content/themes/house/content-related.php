<?php
/**
 * The default template for displaying content.
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>

<div class="grid-item">
	<div class="article">
		<div class="entry-header">
			<a href="<?php the_permalink(); ?>">
				<?php
					/**
					 * Get the featured image
					 * if one is set
					 */
					if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					}
				?>
			</a>
			<a href="<?php the_permalink(); ?>"><?php the_title( '<h2 class="entry-title">', '</h2>' ); ?></a>
		</div><!-- /.entry-header -->
		<div class="entry-footer">
			<span class="posted-on">
				<?php
					/**
					 * Publish date
					 * Translators: format, before, after, echo
					 */
					echo '<time class="entry-date" datetime="' .  get_the_date( 'c' ). '">' . get_the_date( 'F d, Y' ) . '</time>';
				?>
			</span>
		</div>
		<div class="entry-content">
			<?php
				/**
				 * Post excerpt - textarea, no formatting, 100 characters limit
				 * Translators: field id, echo (false returns value), before, after, post id
				 */
				acf_field( 'paragraph', true, '<p>', '</p>' ); ?>
			<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'house' ); ?></a>
		</div><!-- /.entry-content -->		
	</div><!-- /.article -->
</div>
