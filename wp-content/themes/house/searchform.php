<?php
/**
 * The template for displaying search form
 *
 * @package WordPress
 */
?>
<form role="search" method="get" id="searchform" class="searchform search-form fade-in-bottom" action="<?php echo home_url( '/' ); ?>">
    <label>
    	<label class="screen-reader-text" for="s"><?php _e( 'Search', 'house' ); ?></label>
        <input type="text" value="" name="s" id="s" class="search-field input" placeholder="<?php esc_attr_e( 'Search', 'house' ); ?>"  />
    </label>
    <span><?php _e( 'Press Enter to begin your search', 'house' ); ?></span>
</form>