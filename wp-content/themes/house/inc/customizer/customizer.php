<?php
/**
 * Customizer for House theme
 *
 * @package WordPress
 */
/**
 * Register customizer
 *
 * Adds postMessage support for site title and description for the Customizer.
 * This function is attached to 'customize_register' action hook.
 *
 * @param WP_Customize_Manager $wp_customize The Customizer object.
 */
function house_customize_register( $wp_customize ) {
	/**
	 * Failsafe is safe
	 */
	if ( ! isset( $wp_customize ) ) {
		return;
	}

	/**
	 * Add Theme Options panel
	 *
	 * @uses $wp_customize->add_panel() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_panel/
	 * @link $wp_customize->add_panel() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_panel
	 */
	$wp_customize->add_panel( 'house_options_panel', array(
		'title'       => __( 'Theme Options', 'house' ),
		'description' => __( 'Configure your theme settings', 'house' ),
	) );

	/**
	 * Add Header Section for Theme Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'header_options_section', array(
		'title' => __( 'Header options', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Add Footer Section for Theme Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'footer_options_section', array(
		'title' => __( 'Footer options', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Add Social profiles links for Theme Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'social_profiles_section', array(
		'title' => __( 'Social Profiles', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Add MailChimp Section for Theme Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'mailchimp_options_section', array(
		'title' => __( 'MailChimp options', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Payment Section for Theme Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'payment_options_section', array(
		'title' => __( 'Payment options', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Working Time for Theme Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section( 'working_time_options_section', array(
		'title' => __( 'Working Time options', 'house' ),
		'panel' => 'house_options_panel',
	) );

	/**
	 * Header Custom Text setting
	 *
	 * - Setting: Header Custom Text
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure user's text displayed in the site header.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'header_custom_text', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Header Custom Text control
	 *
	 * - Control: Basic: Text
	 * - Setting: Header Custom Text
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Header Custom Text setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'header_custom_text', array(
		'label'       => __( 'Header Custom Text', 'house' ),
		'description' => __( 'Text to appear somewhere in header.', 'house' ),
		'section'     => 'header_options_section',
		'settings'    => 'header_custom_text',
		'type'        => 'text',
	) );

	/**
	 * Top Menu Section setting.
	 *
	 * - Setting: Top Menu Section
	 * - Control: select
	 * - Sanitization: select
	 * 
	 * Uses a radio select to configure top menu section
	 * 
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'top_menu',
		// $args
		array(
			'default'			=> 'no',
			'type'				=> 'theme_mod',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_select'
		)
	);

	/**
	 * Top Menu Section control.
	 *
	 * - Control: Basic: Radio
	 * - Setting: Top Menu Section
	 * - Sanitization: select
	 * 
	 * Register the core "radio" control to be used to configure the Top Menu Section setting.
	 * 
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'top_menu',
		// $args
		array(
			'settings'		=> 'top_menu',
			'section'		=> 'header_options_section',
			'type'			=> 'radio',
			'label'			=> __( 'Top Menu Section', 'house' ),
			'description'	=> __( 'Do you want display top menu section at the top of the pages?', 'house' ),
			'choices'		=> array(
				'yes' => __( 'Yes', 'house' ),
				'no' => __( 'No', 'house' )
			)
		)
	);

	/**
	 * Top bar phone number
	 *
	 * - Setting: Top bar phone number
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Top bar phone number.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'topbar_phone_number', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Top bar phone number control
	 *
	 * - Control: Basic: Text
	 * - Setting: Top bar phone number
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Top bar phone number setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'topbar_phone_number', array(
		'label'       => __( 'Phone number', 'house' ),
		'description' => __( 'Phone number displayed somewhere in the top bar.', 'house' ),
		'section'     => 'header_options_section',
		'settings'    => 'topbar_phone_number',
		'type'        => 'text',
	) );

	/**
	 * Top bar E-mail
	 *
	 * - Setting: Top bar E-mail
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Top bar E-mail.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'topbar_email', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Top bar E-mail control
	 *
	 * - Control: Basic: Text
	 * - Setting: Top bar E-mail
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Top bar E-mail setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'topbar_email', array(
		'label'       => __( 'E-mail', 'house' ),
		'description' => __( 'E-mail displayed somewhere in the top bar.', 'house' ),
		'section'     => 'header_options_section',
		'settings'    => 'topbar_email',
		'type'        => 'text',
	) );

	/**
	 * Top bar Button Link setting .
	 *
	 * - Setting: Top bar Button Link
	 * - Control: url
	 * - Sanitization: url
	 * 
	 * Uses a URL text field to configure the user's top bar button link URL displayed in the top bar.
	 * 
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'topbar_btn_url',
		// $args
		array(
			'default'			=> '',
			'type'				=> 'theme_mod',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_url'
		)
	);

	/**
	 * Top bar Button Link control.
	 *
	 * - Control: Text: URL
	 * - Setting: Top bar Button Link
	 * - Sanitization: url
	 * 
	 * Register the core "url" text control to be used to configure the Top bar Button Link setting.
	 * 
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'topbar_btn_url',
		// $args
		array(
			'settings'		=> 'topbar_btn_url',
			'section'		=> 'header_options_section',
			'type'			=> 'url',
			'label'			=> __( 'Button Link', 'house' ),
			'description'	=> __( 'Button Link URL to be displayed in the top bar.', 'house' )
		)
	);

	/**
	 * Top bar Button label
	 *
	 * - Setting: Top bar Button label
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Top bar Button label.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'topbar_btn_label', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Top bar Button label control
	 *
	 * - Control: Basic: Text
	 * - Setting: Top bar Button label
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Top bar Button label setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'topbar_btn_label', array(
		'label'       => __( 'Button Label', 'house' ),
		'description' => __( 'Button label displayed somewhere in the top bar.', 'house' ),
		'section'     => 'header_options_section',
		'settings'    => 'topbar_btn_label',
		'type'        => 'text',
	) );

	/**
	 * Footer Address Text setting
	 *
	 * - Setting: Footer Address Text
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the user's address text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'footer_address_text', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Footer Address Text setting
	 *
	 * - Control: Basic: Text
	 * - Setting: Footer Address Text
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Footer Address Text setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'footer_address_text', array(
		'label'       => __( 'Footer Address Text', 'house' ),
		'description' => __( 'Text to appear somewhere in footer.', 'house' ),
		'section'     => 'footer_options_section',
		'settings'    => 'footer_address_text',
		'type'        => 'text',
	) );

	/**
	 * Footer Phone Text setting
	 *
	 * - Setting: Footer Phone Text
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the user's Phone text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'footer_phone_text', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Footer Phone Text setting
	 *
	 * - Control: Basic: Text
	 * - Setting: Footer Phone Text
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Footer Phone Text setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'footer_phone_text', array(
		'label'       => __( 'Footer Phone Text', 'house' ),
		'description' => __( 'Text to appear somewhere in footer.', 'house' ),
		'section'     => 'footer_options_section',
		'settings'    => 'footer_phone_text',
		'type'        => 'text',
	) );

	/**
	 * Footer Email Text setting
	 *
	 * - Setting: Footer Email Text
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the user's email text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'footer_email_text', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_email'
	) );

	/**
	 * Footer Email Text setting
	 *
	 * - Control: Basic: Text
	 * - Setting: Footer Email Text
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Footer Email Text setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'footer_email_text', array(
		'label'       => __( 'Contact Email', 'house' ),
		'description' => __( 'Contact email address to be displayed in the site footer.', 'house' ),
		'section'     => 'footer_options_section',
		'settings'    => 'footer_email_text',
		'type'        => 'email',
	) );

	/**
	 * Social Profiles Facebook
	 *
	 * - Setting: Social Profiles Facebook
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_facebook', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Facebook control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles Facebook
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_facebook', array(
		'label'       => __( 'Facebook', 'house' ),
		'description' => __( 'Add your Facebook profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_facebook',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles Twitter
	 *
	 * - Setting: Social Profiles Twitter
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_twitter', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Twitter control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_twitter', array(
		'label'       => __( 'Twitter', 'house' ),
		'description' => __( 'Add your Twitter profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_twitter',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles Instagram
	 *
	 * - Setting: Social Profiles Instagram
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_instagram', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Instagram control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_instagram', array(
		'label'       => __( 'Instagram', 'house' ),
		'description' => __( 'Add your Instagram profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_instagram',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles Google plus
	 *
	 * - Setting: Social Profiles Google plus
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_google', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Google plus control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_google', array(
		'label'       => __( 'Google plus', 'house' ),
		'description' => __( 'Add your Google plus profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_google',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles Pinterest
	 *
	 * - Setting: Social Profiles Pinterest
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_pinterest', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Pinterest control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_pinterest', array(
		'label'       => __( 'Pinterest', 'house' ),
		'description' => __( 'Add your Pinterest profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_pinterest',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles YouTube
	 *
	 * - Setting: Social Profiles YouTube
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_youtube', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * YouTube control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_youtube', array(
		'label'       => __( 'YouTube', 'house' ),
		'description' => __( 'Add your YouTube profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_youtube',
		'type'        => 'url',
	) );

	/**
	 * Social Profiles Vimeo
	 *
	 * - Setting: Social Profiles Vimeo
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_vimeo', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Vimeo control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_vimeo', array(
		'label'       => __( 'Vimeo', 'house' ),
		'description' => __( 'Add your Vimeo profile link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_vimeo',
		'type'        => 'url',
	) );

	/**
	 * Other 1 Social Profiles
	 *
	 * - Setting: Other 1 Social Profiles
	 * - Control: text
	 * - Sanitization: text
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_other_1_label', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Other 1 Social control
	 *
	 * - Control: Basic: Text
	 * - Setting: Other 1 Social
	 * - Sanitization: text
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_other_1_label', array(
		'label'       => __( 'New Social Profile 1', 'house' ),
		'description' => __( 'Add new social profile 1 label.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_other_1_label',
		'type'        => 'text',
	) );

	/**
	 * Other 1 Social Profiles
	 *
	 * - Setting: Other 1 Social Profiles
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_other_1', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Other 1 control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_other_1', array(
		'description' => __( 'Add new social profile 1 link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_other_1',
		'type'        => 'url',
	) );

	/**
	 * Other 2 Social Profiles
	 *
	 * - Setting: Other 2 Social Profiles
	 * - Control: text
	 * - Sanitization: text
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_other_2_label', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Other 2 Social control
	 *
	 * - Control: Basic: Text
	 * - Setting: Other 2 Social
	 * - Sanitization: text
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_other_2_label', array(
		'label'       => __( 'New Social Profile 2', 'house' ),
		'description' => __( 'Add new social profile 2 label.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_other_2_label',
		'type'        => 'text',
	) );

	/**
	 * Other 2 Social Profiles
	 *
	 * - Setting: Other 2 Social Profiles
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_other_2', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Other 2 control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_other_2', array(
		'description' => __( 'Add new social profile 2 link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_other_2',
		'type'        => 'url',
	) );

	/**
	 * Other 3 Social Profiles
	 *
	 * - Setting: Other 3 Social Profiles
	 * - Control: text
	 * - Sanitization: text
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_other_3_label', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Other 3 Social control
	 *
	 * - Control: Basic: Text
	 * - Setting: Other 3 Social
	 * - Sanitization: text
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_other_3_label', array(
		'label'       => __( 'New Social Profile 3', 'house' ),
		'description' => __( 'Add new social profile 3 label.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_other_3_label',
		'type'        => 'text',
	) );

	/**
	 * Other 3 Social Profiles
	 *
	 * - Setting: Other 3 Social Profiles
	 * - Control: url
	 * - Sanitization: url
	 *
	 * Uses a text field to configure the user's copyright text displayed in the site footer.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'social_profiles_other_3', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_url'
	) );

	/**
	 * Other 3 control
	 *
	 * - Control: Text: URL
	 * - Setting: Social Profiles
	 * - Sanitization: url
	 *
	 * Register the core "url" text control to be used to configure the Social Profiles setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'social_profiles_other_3', array(
		'description' => __( 'Add new social profile 3 link.', 'house' ),
		'section'     => 'social_profiles_section',
		'settings'    => 'social_profiles_other_3',
		'type'        => 'url',
	) );

	/**
	 * Mailchimp API Key setting
	 *
	 * - Setting: Mailchimp API Key
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp API Key.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_api_key', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp API Key control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp API Key
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp API Key setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_api_key', array(
		'label'       => __( 'Mailchimp API Key', 'house' ),
		'description' => __( 'Paste your Mailchimp API Key here.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_api_key',
		'type'        => 'text',
	) );

	/**
	 * Mailchimp List ID setting
	 *
	 * - Setting: Mailchimp List ID
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp List ID.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_list_id', array(
		'default'           => '',
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp List ID control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp List ID
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp List ID setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_list_id', array(
		'label'       => __( 'Mailchimp List ID', 'house' ),
		'description' => __( 'Paste your Mailchimp List ID here.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_list_id',
		'type'        => 'text',
	) );

	/**
	 * Mailchimp Thank you message setting
	 *
	 * - Setting: Mailchimp Thank you message
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp Thank you message.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_thank_you_message', array(
		'default'           => __( 'Thank you for subscribing.', 'house' ),
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp Thank you message control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp Thank you message
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp Thank you message setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_thank_you_message', array(
		'label'       => __( 'Mailchimp "Thank you" message', 'house' ),
		'description' => __( 'Wtite your "Thank you" message as success feedback for subscriber.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_thank_you_message',
		'type'        => 'text',
	) );

	/**
	 * Mailchimp Empty field message setting
	 *
	 * - Setting: Mailchimp Empty field message
	 * - Control: text
	 * - Sanitization: text
	 *
	 * Uses a text field to configure the Mailchimp Empty field message.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'mailchimp_empty_field_message', array(
		'default'           => __( 'Address field is empty. Please type your email address and try submitting again.', 'house' ),
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'house_sanitize_nohtml'
	) );

	/**
	 * Mailchimp Empty field message control
	 *
	 * - Control: Basic: Text
	 * - Setting: Mailchimp Empty field message
	 * - Sanitization: text
	 *
	 * Register the core "text" control to be used to configure the Mailchimp Empty field message setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'mailchimp_empty_field_message', array(
		'label'       => __( 'Mailchimp "Empty field" message', 'house' ),
		'description' => __( 'Wtite your "Empty field" message as error feedback for subscriber.', 'house' ),
		'section'     => 'mailchimp_options_section',
		'settings'    => 'mailchimp_empty_field_message',
		'type'        => 'textarea',
	) );

	/**
	 * Currency setting
	 *
	 * - Setting: Currency
	 * - Control: select
	 * - Sanitization: select
	 *
	 * Uses a dropdown select to currency.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'currency',
		// $args
		array(
			'default'			=> 'dollar',
			'type'				=> 'theme_mod',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_select'
		)
	);

	/**
	 * Currency Select control.
	 *
	 * - Control: Basic: Select
	 * - Setting: Color Scheme
	 * - Sanitization: select
	 *
	 * Register the core "select" control to be used to configure the currency setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'currency',
		// $args
		array(
			'settings'		=> 'currency',
			'section'		=> 'payment_options_section',
			'type'			=> 'select',
			'label'			=> __( 'Currency', 'house' ),
			'description'	=> __( 'Select the currency.', 'house' ),
			'choices'		=> array(
				'dollar' => '$',
				'euro' => '€',
				'pound' => '£',
			)
		)
	);

	/**
	 * Working day - opening time hours setting.
	 *
	 * - Setting: Working day - opening time
	 * - Control: select
	 * - Sanitization: select
	 *
	 * Uses a dropdown select to configure the Theme Working day - opening time.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'working_day_open_hours',
		// $args
		array(
			'default'			=> '',
			'type'				=> 'theme_mod',
			'transport'         => 'postMessage',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_select'
		)
	);

	/**
	 * Working day - opening time hours.
	 *
	 * - Control: Basic: Select
	 * - Setting: Working day - opening time
	 * - Sanitization: select
	 *
	 * Register the core "select" control to be used to configure the Working day - opening time setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'working_day_open_hours',
		// $args
		array(
			'settings'		=> 'working_day_open_hours',
			'section'		=> 'working_time_options_section',
			'type'			=> 'select',
			'label'			=> __( 'Working day - opening time', 'house' ),
			'description'	=> __( 'HOURS.', 'house' ),
			'choices'		=> array(
				'07' => __( '7AM', 'house' ),
				'08' => __( '8AM', 'house' ),
				'09' => __( '9AM', 'house' ),
				'10' => __( '10AM', 'house' ),
				'11' => __( '11AM', 'house' ),
				'12' => __( '12PM', 'house' ),
				'13' => __( '1PM', 'house' ),
				'14' => __( '2PM', 'house' ),
				'15' => __( '3PM', 'house' ),
				'16' => __( '4PM', 'house' ),
				'17' => __( '5PM', 'house' ),
				'18' => __( '6PM', 'house' ),
				'19' => __( '7PM', 'house' ),
				'20' => __( '8PM', 'house' ),
			)
		)
	);

	/**
	 * Working day - opening time minutes setting.
	 *
	 * - Setting: Working day - opening time
	 * - Control: select
	 * - Sanitization: select
	 *
	 * Uses a dropdown select to configure the Theme Working day - opening time.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'working_day_open_minutes',
		// $args
		array(
			'default'			=> '',
			'type'				=> 'theme_mod',
			'transport'         => 'postMessage',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_select'
		)
	);

	/**
	 * Working day - opening minutes time.
	 *
	 * - Control: Basic: Select
	 * - Setting: Working day - opening time
	 * - Sanitization: select
	 *
	 * Register the core "select" control to be used to configure the Working day - opening time setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'working_day_open_minutes',
		// $args
		array(
			'settings'		=> 'working_day_open_minutes',
			'section'		=> 'working_time_options_section',
			'type'			=> 'select',
			'description'	=> __( 'MINUTES.', 'house' ),
			'choices'		=> array(
				'00' => __( '00', 'house' ),
				'15' => __( '15', 'house' ),
				'30' => __( '30', 'house' ),
				'45' => __( '45', 'house' ),
			)
		)
	);

	/**
	 * Working day - closing time setting.
	 *
	 * - Setting: Working day - closing time
	 * - Control: select
	 * - Sanitization: select
	 *
	 * Uses a dropdown select to configure the Theme Working day - closing time.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'working_day_close_hours',
		// $args
		array(
			'default'			=> '',
			'type'				=> 'theme_mod',
			'transport'         => 'postMessage',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_select'
		)
	);

	/**
	 * Working day - closing time.
	 *
	 * - Control: Basic: Select
	 * - Setting: Working day - closing time
	 * - Sanitization: select
	 *
	 * Register the core "select" control to be used to configure the Working day - closing time setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'working_day_close_hours',
		// $args
		array(
			'settings'		=> 'working_day_close_hours',
			'section'		=> 'working_time_options_section',
			'type'			=> 'select',
			'label'			=> __( 'Working day - closing time', 'house' ),
			'description'	=> __( 'HOURS', 'house' ),
			'choices'		=> array(
				'15' => __( '3PM', 'house' ),
				'16' => __( '4PM', 'house' ),
				'17' => __( '5PM', 'house' ),
				'18' => __( '6PM', 'house' ),
				'19' => __( '7PM', 'house' ),
				'20' => __( '8PM', 'house' ),
				'21' => __( '9PM', 'house' ),
				'22' => __( '10PM', 'house' ),
				'23' => __( '11PM', 'house' ),
				'00' => __( '12AM', 'house' ),
				'01' => __( '1AM', 'house' ),
				'02' => __( '2AM', 'house' ),
				'03' => __( '3AM', 'house' ),
			)
		)
	);

	/**
	 * Working day - closing time minutes setting.
	 *
	 * - Setting: Working day - closing time
	 * - Control: select
	 * - Sanitization: select
	 *
	 * Uses a dropdown select to configure the Theme Working day - closing time.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'working_day_close_minutes',
		// $args
		array(
			'default'			=> '',
			'type'				=> 'theme_mod',
			'transport'         => 'postMessage',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_select'
		)
	);

	/**
	 * Working day - closing minutes time.
	 *
	 * - Control: Basic: Select
	 * - Setting: Working day - closing time
	 * - Sanitization: select
	 *
	 * Register the core "select" control to be used to configure the Working day - closing time setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'working_day_close_minutes',
		// $args
		array(
			'settings'		=> 'working_day_close_minutes',
			'section'		=> 'working_time_options_section',
			'type'			=> 'select',
			'description'	=> __( 'MINUTES.', 'house' ),
			'choices'		=> array(
				'00' => __( '00', 'house' ),
				'15' => __( '15', 'house' ),
				'30' => __( '30', 'house' ),
				'45' => __( '45', 'house' ),
			)
		)
	);

	/**
	 * Weekend - opening time setting.
	 *
	 * - Setting: Weekend - opening time
	 * - Control: select
	 * - Sanitization: select
	 *
	 * Uses a dropdown select to configure the Theme Weekend - opening time.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'weekend_day_open_hours',
		// $args
		array(
			'default'			=> '',
			'type'				=> 'theme_mod',
			'transport'         => 'postMessage',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_select'
		)
	);

	/**
	 * Weekend - opening time.
	 *
	 * - Control: Basic: Select
	 * - Setting: Weekend - opening time
	 * - Sanitization: select
	 *
	 * Register the core "select" control to be used to configure the Weekend - opening time setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'weekend_day_open_hours',
		// $args
		array(
			'settings'		=> 'weekend_day_open_hours',
			'section'		=> 'working_time_options_section',
			'type'			=> 'select',
			'label'			=> __( 'Weekend - opening time', 'house' ),
			'description'	=> __( 'HOURS', 'house' ),
			'choices'		=> array(
				'07' => __( '7AM', 'house' ),
				'08' => __( '8AM', 'house' ),
				'09' => __( '9AM', 'house' ),
				'10' => __( '10AM', 'house' ),
				'11' => __( '11AM', 'house' ),
				'12' => __( '12PM', 'house' ),
				'13' => __( '1PM', 'house' ),
				'14' => __( '2PM', 'house' ),
				'15' => __( '3PM', 'house' ),
				'16' => __( '4PM', 'house' ),
				'17' => __( '5PM', 'house' ),
				'18' => __( '6PM', 'house' ),
				'19' => __( '7PM', 'house' ),
				'20' => __( '8PM', 'house' ),
			)
		)
	);

	/**
	 * Weekend day - opening time minutes setting.
	 *
	 * - Setting: Weekend day - opening time
	 * - Control: select
	 * - Sanitization: select
	 *
	 * Uses a dropdown select to configure the Theme Weekend day - opening time.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'weekend_day_open_minutes',
		// $args
		array(
			'default'			=> '',
			'type'				=> 'theme_mod',
			'transport'         => 'postMessage',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_select'
		)
	);

	/**
	 * Weekend day - opening minutes time.
	 *
	 * - Control: Basic: Select
	 * - Setting: Weekend day - opening time
	 * - Sanitization: select
	 *
	 * Register the core "select" control to be used to configure the Weekend day - opening time setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'weekend_day_open_minutes',
		// $args
		array(
			'settings'		=> 'weekend_day_open_minutes',
			'section'		=> 'working_time_options_section',
			'type'			=> 'select',
			'description'	=> __( 'MINUTES.', 'house' ),
			'choices'		=> array(
				'00' => __( '00', 'house' ),
				'15' => __( '15', 'house' ),
				'30' => __( '30', 'house' ),
				'45' => __( '45', 'house' ),
			)
		)
	);

	/**
	 * Weekend - closing time setting.
	 *
	 * - Setting: Weekend - closing time
	 * - Control: select
	 * - Sanitization: select
	 *
	 * Uses a dropdown select to configure the Theme Weekend - closing time.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'weekend_day_close_hours',
		// $args
		array(
			'default'			=> '',
			'type'				=> 'theme_mod',
			'transport'         => 'postMessage',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_select'
		)
	);

	/**
	 * Weekend - closing time.
	 *
	 * - Control: Basic: Select
	 * - Setting: Weekend - closing time
	 * - Sanitization: select
	 *
	 * Register the core "select" control to be used to configure the Weekend - closing time setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'weekend_day_close_hours',
		// $args
		array(
			'settings'		=> 'weekend_day_close_hours',
			'section'		=> 'working_time_options_section',
			'type'			=> 'select',
			'label'			=> __( 'Weekend - closing time', 'house' ),
			'description'	=> __( 'HOURS', 'house' ),
			'choices'		=> array(
				'15' => __( '3PM', 'house' ),
				'16' => __( '4PM', 'house' ),
				'17' => __( '5PM', 'house' ),
				'18' => __( '6PM', 'house' ),
				'19' => __( '7PM', 'house' ),
				'20' => __( '8PM', 'house' ),
				'21' => __( '9PM', 'house' ),
				'22' => __( '10PM', 'house' ),
				'23' => __( '11PM', 'house' ),
				'00' => __( '12AM', 'house' ),
				'01' => __( '1AM', 'house' ),
				'02' => __( '2AM', 'house' ),
				'03' => __( '3AM', 'house' ),
			)
		)
	);

	/**
	 * Weekend day - close time minutes setting.
	 *
	 * - Setting: Weekend day - close time
	 * - Control: select
	 * - Sanitization: select
	 *
	 * Uses a dropdown select to configure the Theme Weekend day - close time.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'weekend_day_close_minutes',
		// $args
		array(
			'default'			=> '',
			'type'				=> 'theme_mod',
			'transport'         => 'postMessage',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_select'
		)
	);

	/**
	 * Weekend day - close minutes time.
	 *
	 * - Control: Basic: Select
	 * - Setting: Weekend day - close time
	 * - Sanitization: select
	 *
	 * Register the core "select" control to be used to configure the Weekend day - close time setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'weekend_day_close_minutes',
		// $args
		array(
			'settings'		=> 'weekend_day_close_minutes',
			'section'		=> 'working_time_options_section',
			'type'			=> 'select',
			'description'	=> __( 'MINUTES.', 'house' ),
			'choices'		=> array(
				'00' => __( '00', 'house' ),
				'15' => __( '15', 'house' ),
				'30' => __( '30', 'house' ),
				'45' => __( '45', 'house' ),
			)
		)
	);

	/**
	 * Set Selective Refresh for blog name and description
	 *
	 * All settings with postMessage transport need custom javascript
	 * definitions as well. @see js/customizer.js
	 */
	$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		/**
		 * Site name
		 */
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'            => '.site-title a',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_blogname',
		));
		/**
		 * Site description
		 */
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'            => '.site-description',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_blogdescription',
		));
		/**
		 * Header custom text
		 */
		$wp_customize->selective_refresh->add_partial( 'header_custom_text', array(
			'selector'            => '.header-custom-text',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_header_custom_text',
		));
		/**
		 * Top bar phone number
		 */
		$wp_customize->selective_refresh->add_partial( 'topbar_phone_number', array(
			'selector'            => '#top-bar-phone',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_phone_number',
		));
		/**
		 * Top bar email
		 */
		$wp_customize->selective_refresh->add_partial( 'topbar_email', array(
			'selector'            => '#top-bar-email',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_topbar_email',
		));
		/**
		 * Top bar button
		 */
		$wp_customize->selective_refresh->add_partial( 'topbar_btn_label', array(
			'selector'            => '#top-bar-btn',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_topbar_btn_label',
		));
		/**
		 * Footer address
		 */
		$wp_customize->selective_refresh->add_partial( 'footer_address_text', array(
			'selector'            => '.footer-top .address',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_footer_address_text',
		));
		/**
		 * Footer phone
		 */
		$wp_customize->selective_refresh->add_partial( 'footer_phone_text', array(
			'selector'            => '.footer-top .phone',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_footer_phone_text',
		));
		/**
		 * Footer email
		 */
		$wp_customize->selective_refresh->add_partial( 'footer_email_text', array(
			'selector'            => '.footer-top .email a',
			'container_inclusive' => false,
			'render_callback'     => 'house_customize_partial_footer_email_text',
		));
	}
}

add_action( 'customize_register', 'house_customize_register', 11 );

/**
 * Render the site title for the selective refresh partial.
 *
 * @see house_customize_register()
 *
 * @return void
 */
function house_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @see house_customize_register()
 *
 * @return void
 */
function house_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Render the header custom text for the selective refresh partial.
 *
 * @see house_customize_register()
 *
 * @return void
 */
function house_customize_partial_header_custom_text() {
	echo get_theme_mod( 'header_custom_text' );
}
/**
 * Render the top bar phone number for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_phone_number() {
	$link = '<a href="tel:' . get_theme_mod( 'topbar_phone_number' ) . '">' . get_theme_mod( 'topbar_phone_number' ) . '</a>';
	echo $link;
}
/**
 * Render the top bar email for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_topbar_email() {
	$link = '<a href="mailto:' . get_theme_mod( 'topbar_email' ) . '">' . get_theme_mod( 'topbar_email' ) . '</a>';
	echo $link;
}
/**
 * Render the top bar button for the selective refresh partial.
 *
 * @return void
 */
function house_customize_partial_topbar_btn_label() {
	$link = '<a href="mailto:' . get_theme_mod( 'topbar_btn_url' ) . '">' . get_theme_mod( 'topbar_btn_label' ) . '</a>';
	echo $link;
}
/**
 * Render the footer address for the selective refresh partial.
 *
 * @see house_customize_register()
 *
 * @return void
 */
function house_customize_partial_footer_address_text() {
	echo get_theme_mod( 'footer_address_text' );
}
/**
 * Render the footer phone for the selective refresh partial.
 *
 * @see house_customize_register()
 *
 * @return void
 */
function house_customize_partial_footer_phone_text() {
	echo get_theme_mod( 'footer_phone_text' );
}
/**
 * Render the footer email for the selective refresh partial.
 *
 * @see house_customize_register()
 *
 * @return void
 */
function house_customize_partial_footer_email_text() {
	$link = '<a href="mailto:' . get_theme_mod( 'footer_email_text' ) . '">' . get_theme_mod( 'footer_email_text' ) . '</a>';
	echo $link;
}
/**
 * No-HTML sanitization callback
 *
 * - Sanitization: nohtml
 * - Control: text, textarea, password
 *
 * Sanitization callback for 'nohtml' type text inputs. This callback sanitizes `$nohtml`
 * to remove all HTML.
 *
 * NOTE: wp_filter_nohtml_kses() can be passed directly as `$wp_customize->add_setting()`
 * 'sanitize_callback'. It is wrapped in a callback here merely for example purposes.
 *
 * @see wp_filter_nohtml_kses() https://developer.wordpress.org/reference/functions/wp_filter_nohtml_kses/
 *
 * @param string $nohtml The no-HTML content to sanitize.
 * @return string Sanitized no-HTML content.
 */
function house_sanitize_nohtml( $nohtml ) {
	return wp_filter_nohtml_kses( $nohtml );
}
/**
 * Email sanitization callback example.
 *
 * - Sanitization: email
 * - Control: text
 * 
 * Sanitization callback for 'email' type text controls. This callback sanitizes `$email`
 * as a valid email address.
 * 
 * @see sanitize_email() https://developer.wordpress.org/reference/functions/sanitize_key/
 * @link sanitize_email() https://codex.wordpress.org/Function_Reference/sanitize_email
 *
 * @param string               $email   Email address to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string The sanitized email if not null; otherwise, the setting default.
 */
function house_sanitize_email( $email, $setting ) {
	// Strips out all characters that are not allowable in an email address.
	$email = sanitize_email( $email );
	
	// If $email is a valid email, return it; otherwise, return the default.
	return ( ! is_null( $email ) ? $email : $setting->default );
}
/**
 * URL sanitization callback
 *
 * - Sanitization: url
 * - Control: text, url
 *
 * Sanitization callback for 'url' type text inputs. This callback sanitizes `$url` as a valid URL.
 *
 * NOTE: esc_url_raw() can be passed directly as `$wp_customize->add_setting()` 'sanitize_callback'.
 * It is wrapped in a callback here merely for example purposes.
 *
 * @see esc_url_raw() https://developer.wordpress.org/reference/functions/esc_url_raw/
 *
 * @param string $url URL to sanitize.
 * @return string Sanitized URL.
 */
function house_sanitize_url( $url ) {
	return esc_url_raw( $url );
}
/**
 * Select sanitization callback example.
 *
 * - Sanitization: select
 * - Control: select, radio
 *
 * Sanitization callback for 'select' and 'radio' type controls. This callback sanitizes `$input`
 * as a slug, and then validates `$input` against the choices defined for the control.
 *
 * @see sanitize_key()               https://developer.wordpress.org/reference/functions/sanitize_key/
 * @see $wp_customize->get_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/get_control/
 *
 * @param string               $input   Slug to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
 */
function house_sanitize_select( $input, $setting ) {

	// Ensure input is a slug.
	$input = sanitize_key( $input );

	// Get list of choices from the control associated with the setting.
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// If the input is a valid key, return it; otherwise, return the default.
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

/**
 * Customize preview init
 *
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 * This function is attached to 'customize_preview_init' action hook.
 */
function house_customize_preview_js() {
	wp_enqueue_script( 'house-customizer', get_template_directory_uri() . '/inc/customizer/js/customizer.js', array( 'jquery','customize-preview' ), '1.0.0', true );
}
add_action( 'customize_preview_init', 'house_customize_preview_js' );