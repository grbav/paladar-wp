/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

	// Site title
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	// Site description
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );
	// Header custom text
	wp.customize( 'header_custom_text', function( value ) {
		value.bind( function( to ) {
			$( '.header-custom-text' ).text( to );
		} );
	} );
	// Footer address
	wp.customize( 'footer_address_text', function( value ) {
		value.bind( function( to ) {
			$( '.footer-top .address' ).text( to );
		} );
	} );
	// Footer phone
	wp.customize( 'footer_phone_text', function( value ) {
		value.bind( function( to ) {
			$( '.footer-top .phone' ).text( to );
		} );
	} );
	// Footer email
	wp.customize( 'footer_email_text', function( value ) {
		value.bind( function( to ) {
			var $link = '<a href="mailto:' + to + '">' + to + '</a>';
			$( '.footer-top .email a' ).html( $link );
		} );
	} );
	// Reservation Infor Section Title
	wp.customize( 'reservation_info_title', function( value ) {
		value.bind( function( to ) {
			$( 'h1.reservation-heading' ).text( to );
		} );
	} );
	// Reservation Phone Number
	wp.customize( 'reservation_phone_number', function( value ) {
		value.bind( function( to ) {
			$( '.reservation-phone-number' ).text( to );
		} );
	} );

} )( jQuery );
