<?php
/**
 * Customizer for Reservation panel
 *
 * @package WordPress
 */
/**
 * Register customizer
 *
 * Adds postMessage support for site title and description for the Customizer.
 * This function is attached to 'customize_register' action hook.
 *
 * @param WP_Customize_Manager $wp_customize The Customizer object.
 */
function house_customize_reservation_register( $wp_customize ) {
	/**
	 * Failsafe is safe
	 */
	if ( ! isset( $wp_customize ) ) {
		return;
	}

	/**
	 * Add Panel for General Settings.
	 *
	 * @uses $wp_customize->add_panel() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_panel/
	 * @link $wp_customize->add_panel() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_panel
	 */
	$wp_customize->add_panel(
		// $id
		'house_reservation_panel',
		// $args
		array(
			'priority' 			=> 110,
			'capability' 		=> 'edit_theme_options',
			'theme_supports'	=> '',
			'title' 			=> __( 'Reservation', 'house' ),
			'description' 		=> __( 'Configure reservation settings', 'house' ),
		)
	);

	/**
	 * Add Header Section for General Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	$wp_customize->add_section(
		// $id
		'header_reservation_section',
		// $args
		array(
			'title'			=> __( 'Reservation section', 'house' ),
			'panel'			=> 'house_reservation_panel'
		)
	);

	/**
	 * Add Book Form Section for General Options.
	 *
	 * @uses $wp_customize->add_section() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_section/
	 * @link $wp_customize->add_section() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_section
	 */
	// $wp_customize->add_section(
	// 	// $id
	// 	'header_reservation_section',
	// 	// $args
	// 	array(
	// 		'title'			=> __( 'Reservation Book Form', 'house' ),
	// 		'panel'			=> 'house_reservation_panel'
	// 	)
	// );

	/**
	 * Reservation background image
	 *
	 * Uses the media manager to upload and select an image to be used as the site logo.
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'reservation_bg_image',	array(
		'default'			=> '',
		'type'				=> 'theme_mod',
		'capability'		=> 'edit_theme_options',
		'sanitize_callback'	=> 'house_sanitize_image'
	));

	/**
	 * Image Upload control.
	 *
	 * Control: Image Upload
	 * Setting: Reservation background image
	 * Sanitization: image
	 *
	 * Register "WP_Customize_Color_Control" to be used to configure the Link Color setting.
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 *
	 * @uses WP_Customize_Image_Control() https://developer.wordpress.org/reference/classes/wp_customize_image_control/
	 * @link WP_Customize_Image_Control() https://codex.wordpress.org/Class_Reference/WP_Customize_Image_Control
	 */
	$wp_customize->add_control(
		new WP_Customize_Image_Control(	$wp_customize, 'reservation_bg_image', array(
			'settings'		=> 'reservation_bg_image',
			'section'		=> 'header_reservation_section',
			'label'			=> __( 'Reservation Info Background Image', 'house' ),
			'description'	=> __( "Select image to be used as background for <strong>Reservation Info</strong> section. This section holds info on restaurant working time and phone number. <strong>Please note that this section won't be visible if no image is selected.</strong>", "house" )
		))
	);

	/**
	 * Reservation info title
	 *
	 * - Control: text
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'reservation_info_title', array(
		'default'			=> '',
		'type'				=> 'theme_mod',
		'transport'         => 'postMessage',
		'capability'		=> 'edit_theme_options',
		'sanitize_callback'	=> 'house_sanitize_nohtml'
	));

	/**
	 * Reservation info title
	 *
	 * - Control: Basic: Text
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'reservation_info_title', array(
		'settings'		=> 'reservation_info_title',
		'section'		=> 'header_reservation_section',
		'type'			=> 'text',
		'label'			=> __( 'Reservation Info Section Title', 'house' ),
		'description'	=> __( 'E.G. <em>Make a reservation</em>', 'house' )
	));

	/**
	 * Reservation phone number
	 *
	 * - Control: text
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'reservation_phone_number',	array(
		'default'			=> '',
		'type'				=> 'theme_mod',
		'transport'         => 'postMessage',
		'capability'		=> 'edit_theme_options',
		'sanitize_callback'	=> 'house_sanitize_nohtml'
	));

	/**
	 * Reservation phone number
	 *
	 * - Control: Basic: Text
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'reservation_phone_number', array(
		'settings'		=> 'reservation_phone_number',
		'section'		=> 'header_reservation_section',
		'type'			=> 'text',
		'label'			=> __( 'Reservation Phone Number', 'house' ),
		'description'	=> __( 'Add phone number for reservation and any other text before or after the number. (e.g. "Call us on 00 123 456")', 'house' )
	));
	/**
	 * Reservation phone number
	 *
	 * - Control: text
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'reservation_opening_hours_text',	array(
		'default'			=> '',
		'type'				=> 'theme_mod',
		'transport'         => 'postMessage',
		'capability'		=> 'edit_theme_options',
		'sanitize_callback'	=> 'sanitize_textarea_field'
	));

	/**
	 * Reservation phone number
	 *
	 * - Control: Basic: Text
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'reservation_opening_hours_text', array(
		'settings'		=> 'reservation_opening_hours_text',
		'section'		=> 'header_reservation_section',
		'type'			=> 'textarea',
		'label'			=> __( 'Reservation Opening Hours Text', 'house' ),
		'description'	=> __( '<p>Opening hours text. Breaklines will be preserved in output. You can use following placeholders, which will be replaced with times you set in Working Time Options:</p><p><strong>[workday_start]</strong>, <strong>[workday_end]</strong>, <strong>[weekend_start]</strong> and <strong>[weekend_end]</strong></p>', 'house' )
	));

	/**
	 * Reservation Booking form heading
	 *
	 * - Control: text
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting( 'reservation_book_form_heading', array(
		'default'			=> '',
		'type'				=> 'theme_mod',
		'transport'         => 'postMessage',
		'capability'		=> 'edit_theme_options',
		'sanitize_callback'	=> 'house_sanitize_html'
	));

	/**
	 * Reservation Booking form heading
	 *
	 * - Control: Basic: Text
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control( 'reservation_book_form_heading', array(
		'settings'		=> 'reservation_book_form_heading',
		'section'		=> 'header_reservation_section',
		'type'			=> 'text',
		'label'			=> __( 'Booking Form Heading.', 'house' ),
		'description'	=> __( 'Add heading before booking form.', 'house' )
	));

	/**
	 * Reservation Booking form to user
	 *
	 * - Control: text
	 *
	 * @uses $wp_customize->add_setting() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_setting/
	 * @link $wp_customize->add_setting() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_setting
	 */
	$wp_customize->add_setting(
		// $id
		'reservation_book_form',
		// $args
		array(
			'default'			=> '',
			'type'				=> 'theme_mod',
			'capability'		=> 'edit_theme_options',
			'sanitize_callback'	=> 'house_sanitize_html'
		)
	);

	/**
	 * Reservation Booking form
	 *
	 * - Control: Basic: Text
	 *
	 * @uses $wp_customize->add_control() https://developer.wordpress.org/reference/classes/wp_customize_manager/add_control/
	 * @link $wp_customize->add_control() https://codex.wordpress.org/Class_Reference/WP_Customize_Manager/add_control
	 */
	$wp_customize->add_control(
		// $id
		'reservation_book_form',
		// $args
		array(
			'settings'		=> 'reservation_book_form',
			'section'		=> 'header_reservation_section',
			'type'			=> 'text',
			'label'			=> __( 'Reservation Book Form.', 'house' ),
			'description'	=> __( 'Copy Contact form 7 - ID here!', 'house' )
		)
	);

	/**
	 * Selective refresh
	 */
	if ( isset( $wp_customize->selective_refresh ) ) {
		/**
		 * Reservation Info Section Title
		 */
		$wp_customize->selective_refresh->add_partial( 'reservation_info_title', array(
			'selector'            => 'h1.reservation-heading',
			'container_inclusive' => false,
			'render_callback'     => 'house_partial_reservation_title',
		));
		/**
		 * Reservation Phone Number
		 */
		$wp_customize->selective_refresh->add_partial( 'reservation_phone_number', array(
			'selector'            => '.reservation-phone-number',
			'container_inclusive' => false,
			'render_callback'     => 'house_partial_reservation_phone_number',
			'fallback_refresh'    => false
		));
		/**
		 * Reservation Opening Hours Text
		 */
		$wp_customize->selective_refresh->add_partial( 'reservation_opening_hours_text', array(
			'selector'            => '.reservation-opening-hours',
			'container_inclusive' => false,
			'render_callback'     => 'house_partial_reservation_opening_hours_text',
			'fallback_refresh'    => false
		));
	}
}

add_action( 'customize_register', 'house_customize_reservation_register', 11 );

/**
 * Render Reservation Info Title on selective refresh
 */
function house_partial_reservation_title() {
	echo get_theme_mod( 'reservation_info_title' );
}
/**
 * Render Reservation Phone Number on selective refresh
 */
function house_partial_reservation_phone_number() {
	echo get_theme_mod( 'reservation_phone_number' );
}
/**
 * Render Reservation Opening Hours Text on selective refresh
 */
function house_partial_reservation_opening_hours_text() {
	echo house_restaurant_open_times_text( '<p class="reservation-opening-hours">', '</p>' );
}

/**
 * Image sanitization callback example.
 *
 * Checks the image's file extension and mime type against a whitelist. If they're allowed,
 * send back the filename, otherwise, return the setting default.
 *
 * - Sanitization: image file extension
 * - Control: text, WP_Customize_Image_Control
 *
 * @see wp_check_filetype() https://developer.wordpress.org/reference/functions/wp_check_filetype/
 *
 * @param string               $image   Image filename.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string The image filename if the extension is allowed; otherwise, the setting default.
 */
function house_sanitize_image( $image, $setting ) {
	/*
	 * Array of valid image file types.
	 *
	 * The array includes image mime types that are included in wp_get_mime_types()
	 */
    $mimes = array(
        'jpg|jpeg|jpe' => 'image/jpeg',
        'gif'          => 'image/gif',
        'png'          => 'image/png',
        'bmp'          => 'image/bmp',
        'tif|tiff'     => 'image/tiff',
        'ico'          => 'image/x-icon'
    );
	// Return an array with file extension and mime_type.
    $file = wp_check_filetype( $image, $mimes );
	// If $image has a valid mime_type, return it; otherwise, return the default.
    return ( $file['ext'] ? $image : $setting->default );
}

/**
 * HTML sanitization callback example.
 *
 * - Sanitization: html
 * - Control: text, textarea
 *
 * Sanitization callback for 'html' type text inputs. This callback sanitizes `$html`
 * for HTML allowable in posts.
 *
 * NOTE: wp_filter_post_kses() can be passed directly as `$wp_customize->add_setting()`
 * 'sanitize_callback'. It is wrapped in a callback here merely for example purposes.
 *
 * @see wp_filter_post_kses() https://developer.wordpress.org/reference/functions/wp_filter_post_kses/
 *
 * @param string $html HTML to sanitize.
 * @return string Sanitized HTML.
 */
function house_sanitize_html( $html ) {
	return wp_filter_post_kses( $html );
}