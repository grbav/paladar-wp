<?php
/**
 * Featured images
 *
 * Get default and theme defined image sizes for featured images.
 *
 * @package WordPress
 */
/**
 * House featured image
 *
 * Get post/page featured image. Optional set images size and
 * whether the image should link to post.
 * @link https://developer.wordpress.org/reference/functions/the_post_thumbnail/
 *
 * Uses Justin Tadlock's 'Get The Image' plugin.
 * @see inc/images/get-the-image.php
 *
 * @param  string  $size   Image size id, 'thumbnail', 'medium', 'large' etc
 * @param  boolean $link   Shoud image link to post or be just image
 * @param  boolean $echo   Return/echo the image
 * @return string          Returns/echoes image markup
 */
function house_featured_image( $size = 'full', $link = true, $echo = true ) {
	$args = array(
		/* Post the image is associated with. */
		'post_id'            => get_the_ID(),

		/* Methods of getting an image (in order). */
		'meta_key'           => array( $size ), // array|string
		'featured'           => true,
		'attachment'         => false, // if no thumbnail then it gets first attached image
		'scan'               => false,
		'callback'           => null,
		'default'            => false,

		/* Attachment-specific arguments. */
		'size'               => isset( $_wp_additional_image_sizes[$size] ) ? $size : $size,

		/* Format/display of image. */
		'link_to_post'       => $link, // just image or image in link
		'image_class'        => '',
		'width'              => false,
		'height'             => false,
		'before'             => '',
		'after'              => '',

		/* Captions. */
		'caption'            => false, // Default WP [caption] requires a width.

		/* Return/echo image. */
		'echo'               => $echo,
	);

	return get_the_image( $args );
}

/**
 * Get featured image src
 *
 * Previous function house_featured_image() returns whole <img> tag.
 * Sometimes we need just src for the featured image.
 *
 * @param  int $post_id Post id
 * @param  string $size    Image size
 * @return string          Returns src for the featured image
 */
function get_featured_image_src( $post_id, $size = 'thumbnail' ) {
	$thumbnail_id = get_post_thumbnail_id( $post_id );

	if ( $thumbnail_id ) {
		$thumbnail_url = wp_get_attachment_image_src( $thumbnail_id, $size );
		$src = $thumbnail_url[0];
		return $src;
	}
}

/**
 * Featured image sizes
 *
 * Get all registered image sizes for the featured image of the current post-
 * must be used inside the loop.
 *
 * @uses wp_get_attachment_image_src()
 * @link https://developer.wordpress.org/reference/functions/wp_get_attachment_image_src/
 *
 * Returns an array (or false) for each size:
 *     $array[0] url,
 *     $array[1] width,
 *     $array[2] height,
 *     $array[3] bool - is_intermediate (is image cropped/resized - true) or is original size - false
 *
 * @return array Returns featured image array with all registered sizes
 */
function house_featured_image_sizes() {
	/**
	 * Get featured image id
	 * @link https://codex.wordpress.org/Function_Reference/get_post_thumbnail_id
	 * @var int
	 */
	$attachment_id = get_post_thumbnail_id();
	/**
	 * Declare custom array
	 * @var array
	 */
	$attachment = array();

	if ( $attachment_id ) {
		/**
		 * Add 'full' size
		 */
		$attachment['sizes'] = array(
			'full' => wp_get_attachment_image_src( $attachment_id, 'full', false )
		);
		/**
		 * Add all other registered sizes, default and custom
		 */
		foreach ( get_intermediate_image_sizes() as $size ) {
			$attachment['sizes'][$size] = wp_get_attachment_image_src( $attachment_id, $size, false );
		}
	}

	return $attachment;
}
/**
 * Get featured image size
 *
 * Get featured image width and height for specified image size and current post -
 * must be used inside the loop.
 *
 * @param  string $registered_size    Registered image size, default or custom
 * @return array                      Returns array with width and height for specified image size
 */
function house_get_featured_image_size( $registered_size = 'full' ) {
	/**
	 * Define our custom array
	 * @var array
	 */
	$sizes = array();
	/**
	 * Get all sizes for featured image
	 * @var array
	 */
	$attachment = house_featured_image_sizes();
	/**
	 * If we have any featured image and specified image size
	 */
	if ( $attachment && $attachment['sizes'][$registered_size] ) {
		/**
		 * Get just width and height
		 * @var array
		 */
		$sizes = array(
			'width' => $attachment['sizes'][$registered_size][1],
			'height' => $attachment['sizes'][$registered_size][2]
		);
	}

	return $sizes;
}