<?php
/**
 * Here include all custom post type related functions
 */

 // cpt and taxonomy helper functions
 include( get_template_directory() . '/inc/cpt/taxonomies-cpt-functions.php' );
 // register recipes custom post type
 include( get_template_directory() . '/inc/cpt/register-cpt-recepies.php' );  
 // register events custom post type
 include( get_template_directory() . '/inc/cpt/register-cpt-events.php' );
 // register menu custom post type
 include( get_template_directory() . '/inc/cpt/register-cpt-menu.php' );
