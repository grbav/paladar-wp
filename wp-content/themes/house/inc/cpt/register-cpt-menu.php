<?php
/**
 * Register menu custom post type
 * Translators: cpt name (always in singular!), args
 * @var CustomPostType
 */

/**
 * Set supports array
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#supports
 * @var array
 */
$supports = array(
	'title',
	'editor',
	'author',
	'thumbnail'
);
/**
 * Register custom post type with custom options
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
 * @var array
 */
$options = array(
	'public'        => true,
	'menu_position' => 5,
	'supports'      => $supports
);

/**
 * Create the custm post type
 *
 * Translators: cpt name (always in singular!), args
 * @var CustomPostType
 */
$cpt = new CustomPostType( 'menu', $options );
/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
$cpt->menu_icon( 'dashicons-index-card' );
/**
 * Dashboard posts listing columns
 * @link https://github.com/jjgrainger/wp-custom-post-type-class#columns
 */
$cpt->columns( array(
	'cb'       => '<input type="checkbox" />',
	'title'    => __( 'Title', 'house' ),
	'date'     => __( 'Date', 'house' ),
	'featured' => __( 'Featured Image', 'house' ),
));
/**
 * Populate custom columns
 *
 * Default 'icon' column is using 'thumbnail' size which is too big, so we are creating
 * custom column with our predefined smaller image size (100px x 75px).
 *
 * @link https://github.com/jjgrainger/wp-custom-post-type-class#populating-columns
 */
$cpt->populate_column( 'featured', function( $column, $post ) {
	if ( has_post_thumbnail( $post ) ) {
		$src = get_featured_image_src( $post, 'featured-preview' );
		echo '<img src="' . $src . '" />';
	}
});

/**
 * Custom taxonomies
 * @var string
 */
$taxonomy = 'Meal';

/**
 * Register custom taxonomy, hierarchical by default.
 */
$cpt->register_taxonomy( $taxonomy );

/**
 * Make custom taxonomy non hierarchical
 */
$cpt->taxonomy_settings[$taxonomy]['hierarchical'] = 1;