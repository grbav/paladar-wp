<?php
/**
 * Register events custom post type
 * Translators: cpt name (always in singular!), args
 * @var CustomPostType
 */

/**
 * Set supports array
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#supports
 * @var array
 */
$supports = array(
	'title',
	'editor',
	'author',
	'thumbnail'
);
/**
 * Register custom post type with custom options
 *
 * @link https://codex.wordpress.org/Function_Reference/register_post_type#Arguments
 * @var array
 */
$options = array(
	'public'        => true,
	'show_in_rest'  => true,
	'menu_position' => 5,
	'supports'      => $supports
);

/**
 * Create the custm post type
 *
 * Translators: cpt name (always in singular!), args
 * @var CustomPostType
 */
$cpt = new CustomPostType( 'event', $options );
/**
 * Set menu icon for custom post type
 *
 * @link https://developer.wordpress.org/resource/dashicons/
 */
$cpt->menu_icon( 'dashicons-calendar' );
/**
 * Dashboard posts listing columns
 * @link https://github.com/jjgrainger/wp-custom-post-type-class#columns
 */
$cpt->columns( array(
	'cb'         => '<input type="checkbox" />',
	'title'      => __( 'Title', 'house' ),
	'date'       => __( 'Date', 'house' ),
	'start_time' => __( 'Start Time', 'house' ),
	'end_time'   => __( 'End Time', 'house' ),
	'featured'   => __( 'Featured Image', 'house' ),
));
/**
 * Populate custom columns
 *
 * Default 'icon' column is using 'thumbnail' size which is too big, so we are creating
 * custom column with our predefined smaller image size (100px x 75px).
 *
 * @link https://github.com/jjgrainger/wp-custom-post-type-class#populating-columns
 */
$cpt->populate_column( 'featured', function( $column, $post ) {
	if ( has_post_thumbnail( $post ) ) {
		$src = get_featured_image_src( $post, 'featured-preview' );
		echo '<img src="' . $src . '" />';
	}
});
$cpt->populate_column( 'start_time', function( $column, $post ) {
	echo get_post_time( 'h:i a' );
});
$cpt->populate_column( 'end_time', function( $column, $post ) {
	acf_field( 'end_time' );
});
/**
 * Remove action on future events for making them 'scheduled'
 */
remove_action( 'future_event', '_future_post_hook' );
add_filter( 'wp_insert_post_data', 'house_set_future_events_to_pubish', 10, 2 );
/**
 * Set future events to 'publish' so that we can display them.
 * This function is attached to 'wp_insert_post_data' filter hook.
 *
 * @param  array $data      An array of slashed post data
 * @param  array $postarr   An array of sanitized, but otherwise unmodified post data
 * @return array            Returns filtered array
 */
function house_set_future_events_to_pubish( $data, $postarr ) {
	if ( $data['post_status'] == 'future' && $data['post_type'] == 'event' ) {
		$data['post_status'] = 'publish';
	}
	return $data;
}
/**
 * Query for all events, future and past
 * @return obj WP_Query
 */
function house_get_all_events_query() {
	$args = array(
		'post_type' => 'event',
		'posts_per_page' => -1,
	);

	$query = new WP_Query( $args );

	return $query;
}
/**
 * Count all events, future and past
 * @see  house_get_all_events_query()
 *
 * Used for displaying the number of all events in
 * events filters 'partials/navigations/events-filter.php'
 *
 * @return int Returns number of event posts
 */
function house_count_all_events() {
	$query = house_get_all_events_query();
	return $query->found_posts;
}
/**
 * Query for future events
 * @return obj WP_Query
 */
function house_get_future_events_query() {
	$args = array(
		'post_type'      => 'event',
		'posts_per_page' => -1,
		'date_query' => array(
			array(
				'column' => 'post_date_gmt',
				'after'  => date('Y-m-d H:i'),
			),
		),
	);

	$query = new WP_Query( $args );

	return $query;
}
/**
 * Count future events
 * @see  house_get_future_events_query()
 *
 * Used for displaying the number of future events in
 * events filters 'partials/navigations/events-filter.php'
 *
 * @return int Returns number of event posts
 */
function house_count_future_events() {
	$query = house_get_future_events_query();
	return $query->found_posts;
}
/**
 * Query for past events
 * @return obj WP_Query
 */
function house_get_past_events_query() {
	$args = array(
		'post_type'      => 'event',
		'posts_per_page' => -1,
		'date_query' => array(
			array(
				'column' => 'post_date_gmt',
				'before'  => date('Y-m-d H:i'),
			),
		),
	);

	$query = new WP_Query( $args );

	return $query;
}
/**
 * Count past events
 * @see  house_get_past_events_query()
 *
 * Used for displaying the number of past events in
 * events filters 'partials/navigations/events-filter.php'
 *
 * @return int Returns number of event posts
 */
function house_count_past_events() {
	$query = house_get_past_events_query();
	return $query->found_posts;
}
/**
 * Set event class, 'coming' or 'past'
 * based on event's publish date and time
 * and compared to now.
 *
 * @param  int $post_id   Event post id
 * @return string         Returns 'coming' or 'past' string
 */
function house_set_event_class( $post_id = null ) {
	if ( $post_id ) {
		$id = $post_id;
	} else {
		$id = get_the_ID();
	}
	/**
	 * Get today date
	 */
	$today = date( "Y-m-d" );
	/**
	 * Get event published date
	 */
	$post_date = get_the_date( "Y-m-d", $id );
	/**
	 * Event happened before today
	 */
	if ( $today > $post_date ) :
		$class = 'past';
	/**
	 * Event date is today
	 */
	elseif ( $today == $post_date ) :
		/**
		 * Get right now hour and minute
		 */
		$now = date( "H:i" );
		/**
		 * Get event publish time, hour and minute
		 */
		$post_time = get_the_date( "H:i", $id );
		/**
		 * Event already started earlier today
		 */
		if ( $now > $post_time ) :
			/**
			 * Get event end time
			 */
			$end = strtotime( get_field( 'end_time', $id ) );
			if ( $end ) :
				$end = date( "H:i", $end );
				/**
				 * Event is finished earlier today
				 */
				if ( $now > $end ) :
					$class = 'past';
				/**
				 * Event has already began
				 * but still not finished
				 */
				else :
					$class = 'coming';
				endif;
			/**
			 * If end time field is empty
			 * treat event as passed
			 */
			else :
				$class = 'past';
			endif; // $end
		/**
		 * Event is starting this very moment
		 */
		elseif ( $now == $post_time ) :
			$class = 'coming';
		/**
		 * Event will start later today
		 */
		else :
			$class = 'coming';
		endif; // $now check
	/**
	 * Event date will happen
	 * some other day in the future
	 */
	else :

		$class = 'coming';

	endif; // $today check

	return $class;
}