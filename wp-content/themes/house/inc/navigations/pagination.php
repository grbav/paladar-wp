<?php
/**
 * Paginations
 *
 * Functions for rendering various paginations.
 *
 * @package WordPress
 */
/**
 * Textual pagination
 *
 * Rendering pagination on archive pages in text form (Newer posts, Older posts)
 */
function house_content_nav( $html_id ) {
	global $wp_query;

	$html_id = esc_attr( $html_id );

	if ( $wp_query->max_num_pages > 1 ) : ?>
		<div class="pagination-wrapper">
			<nav id="<?php echo $html_id; ?>" class="pagination text-pagination" role="navigation">
				<h3 class="assistive-text"><?php _e( 'Post navigation', 'house' ); ?></h3>
				<div class="nav-previous"><?php previous_posts_link( __( '<span class="meta-nav">&larr;</span> Newer posts', 'house' ) ); ?></div>
				<div class="nav-next"><?php next_posts_link( __( 'Older posts <span class="meta-nav">&rarr;</span>', 'house' ) ); ?></div>
			</nav><!-- #<?php echo $html_id; ?> .navigation -->
		</div>
	<?php endif;
}
/**
 * Numbered pagination
 *
 * Rendering pagination on archive pages in numbered form.
 *
 * @link http://codex.wordpress.org/Function_Reference/paginate_links
 * @link http://wordpress.stackexchange.com/questions/33583/using-paginate-links-to-generate-1-2-3-10-20-30-40-55-pagination
 */
function house_content_pagination( $html_id ) {
	global $wp_query, $globalSite;

	$html_id = esc_attr( $html_id );

	$prev_text = sprintf( __( '%1$s %2$s', 'house' ),
		$prev_arrow = '<span class="meta-nav">&lsaquo;</span>',
		$prev = '<span class="pag-text prev-text">' . __( 'Prev', 'house' ) . '</span>'
	);

	$next_text = sprintf( __( '%1$s %2$s', 'house' ),
		$next = '<span class="pag-text next-text">' . __( 'Next', 'house' ) . '</span>',
		$next_arrow = '<span class="meta-nav">&rsaquo;</span>'
	);

	$first_text = sprintf( __( '%1$s %2$s', 'house' ),
		$first_arrow = '<span class="meta-nav">&laquo;</span>',
		$first = '<span class="pag-text first-text">' . __( 'First', 'house' ) . '</span>'
	);

	$last_text = sprintf( __( '%1$s %2$s', 'house' ),
		$last = '<span class="pag-text last-text">' . __( 'Last', 'house' ) . '</span>',
		$last_arrow = '<span class="meta-nav">&raquo;</span>'
	);

	$total = $wp_query->max_num_pages;

	if ( $total > 1 ) :
		// Get the current page
		if ( !$current_page = get_query_var('paged') )
			$current_page = 1;
		// Structure of 'format' depends on whether we're using pretty permalinks
		$permalinks = get_option('permalink_structure');

		// Fix url for search pages
		if ( is_search() ) {
			$s = get_search_query();
			$base = empty( $permalinks ) ? $globalSite['home'] . '&page=%#%?s=' . $s :  $globalSite['home'] . 'page/%#%/?s=' . $s;
			$format = '';
		}
		else {
			$base = get_pagenum_link(1) . '%_%';
			$format = empty( $permalinks ) ? '&page=%#%' : 'page/%#%/';
		}

		$currently = sprintf( __( 'Page %1$s of %2$s', 'house' ),
			'<span class="current-number">' . $current_page . '</span>',
			'<span class="total-number">' . $total . '</span>'
		);

		if ( $total > 10 ) {
			$show_all = false;
		}
		else {
			$show_all = true;
		}

		$first_link = get_pagenum_link( 1 );
		$last_link 	= get_pagenum_link( $total );

		$pages = paginate_links( array(
			'base' 			=> $base,
			'format' 		=> $format,
			'current' 		=> $current_page,
			'total' 		=> $total,
			'show_all' 		=> $show_all,
			'end_size' 		=> 1, // show last page, if last is the current also show one before
			'mid_size' 		=> 1, // show one page before and one page after the current
			'prev_next' 	=> true,
			'prev_text' 	=> $prev_text,
			'next_text' 	=> $next_text,
			'type' 			=> 'array'
		) ); ?>

		<div class="pagination-wrapper">
			<nav id="<?php echo $html_id; ?>" class="pagination number-pagination" role="navigation">
				<h3 class="assistive-text"><?php _e( 'Post navigation', 'house' ); ?></h3>
				<ul class="page-numbers">
				<?php if ( $current_page != 1 ) {
					echo '<li><a class="first page-numbers" href="' . $first_link . '">' . $first_text . '</a></li>';
				}
				foreach ( $pages as $link ) {
					echo '<li>' . $link . '</li>';
				}
				if ( $current_page != $total ) {
					echo '<li><a class="last page-numbers" href="' . $last_link . '">' . $last_text . '</a></li>';
				} ?>
				</ul>
			</nav><!-- #<?php echo $html_id; ?> .paginated.navigation -->
		</div>
	<?php endif;
}
/**
 * Simple numbered pagination
 *
 * Rendering pagination on archive pages in numbered form. Simple pagination has no 'first' and 'last' item,
 * no text 'Page $current of $total' and has only arrows for 'next' and 'prev' items.
 *
 * @link http://codex.wordpress.org/Function_Reference/paginate_links
 * @link http://wordpress.stackexchange.com/questions/33583/using-paginate-links-to-generate-1-2-3-10-20-30-40-55-pagination
 *
 * @param  string  $html_id  Id for pagination nav
 * @param  obj     $query    WP_Query object
 * @param  string  $class    Custom classes for pagination ul
 * @return string            Returns pagination markup
 */
function house_simple_content_pagination( $html_id, $query = null, $class = null ) {
	global $wp_query, $globalSite;

	$html_id = esc_attr( $html_id );

	$prev_text = house_svg_icon( 'arrow-left', 'icon-arrow' );

	$next_text = house_svg_icon( 'arrow-right', 'icon-arrow' );

	if ( empty( $query ) ) {
		$total = $wp_query->max_num_pages;
	} else {
		$total = $query->max_num_pages;
	}

	if ( $total > 1 ) :
		// Get the current page
		if ( !$current_page = get_query_var( 'paged' ) )
			$current_page = 1;
		// Structure of 'format' depends on whether we're using pretty permalinks
		$permalinks = get_option( 'permalink_structure' );

		// Fix url for search pages
		if ( is_search() ) {
			$s = get_search_query();
			$base = empty( $permalinks ) ? $globalSite['home'] . '&page=%#%?s=' . $s :  $globalSite['home'] . 'page/%#%/?s=' . $s;
			$format = '';
		}
		else {
			$base = get_pagenum_link(1) . '%_%';
			$format = empty( $permalinks ) ? '&page=%#%' : 'page/%#%/';
		}

		if ( $total > 10 ) {
			$show_all = false;
		}
		else {
			$show_all = true;
		}

		$pages = paginate_links( array(
			'base' 			=> $base,
			'format' 		=> $format,
			'current' 		=> $current_page,
			'total' 		=> $total,
			'show_all' 		=> $show_all,
			'end_size' 		=> 1, // show last page, if last is the current also show one before
			'mid_size' 		=> 1, // show one page before and one page after the current
			'prev_next' 	=> true,
			'prev_text' 	=> $prev_text,
			'next_text' 	=> $next_text,
			'type' 			=> 'array'
		) ); ?>

		<div class="pagination-wrapper">
			<nav id="<?php echo $html_id; ?>" class="pagination number-pagination" role="navigation">
				<h3 class="assistive-text"><?php _e( 'Post navigation', 'house' ); ?></h3>
				<ul class="pagination page-numbers <?php echo $class; ?>">
				<?php
					foreach ( $pages as $link ) :
						echo '<li class="page-number">' . $link . '</li>';
					endforeach; // $pages as $link
				?>
				</ul>
			</nav><!-- #<?php echo $html_id; ?> .paginated.navigation -->
		</div>
	<?php endif;
}