<?php
/**
 * Video functions
 *
 * Build custm functionality based on YouTube and Vimeo API functions.
 *
 * @package WordPress
 */
/**
 * Get url host
 *
 * Check the url host.
 *
 * Basically, we want to know where is video coming from - YouTube or Vimeo.
 *
 * @param  string $url Url
 * @return string      Returns 'host' part of url - domain
 */
function get_url_host( $url ) {
	$parse = parse_url( $url );
	$host = $parse['host'];

	return $host;
}
/**
 * Set video image
 *
 * Check where is video coming from and set its image.
 *
 * @param string $url         Video url
 * @return string             Returns video image src
 */
function set_video_image_src( $url ) {
	/**
	 * Check the host
	 * @var string
	 */
	$host = get_url_host( $url );
	/**
	 * Declare src
	 * @var string
	 */
	$src = '';

	if ( strpos( $host, 'youtu' ) !== false ) {
		/**
		 * Get youtube image src
		 * @var string
		 */
		$src = get_youtube_image( $url );

	} elseif ( strpos( $host, 'vimeo' ) !== false ) {
		/**
		 * Get vimeo image src
		 * @var string
		 */
		$src = get_vimeo_image( $url, 'large' );
	}

	return $src;
}
/**
 * Video image src
 *
 * Echo video image src.
 *
 * @param string $url         Video url
 * @return string             Echoes video image src
 */
function video_image( $url ) {
	echo set_video_image_src( $url );
}
/**
 * Set iframe src
 *
 * The only difference between youtube and vimeo <iframe> is in src attribute.
 * Let's build it now depending on video host.
 *
 * @param string $url         Video url
 * @param string $iframe_id   HTML id for <iframe>
 * @param bool $autoplay      Include autoplay control for player, true or false
 *                            (sometimes we need to include it via js and not to have it here at all)
 * @param string $loop        Loop control for player, '0' and '1' are valid values
 * @return string             Returns <iframe> ready url
 */
function set_iframe_src( $url, $iframe_id = 'video-player', $autoplay = false, $loop = '0' ) {
	/**
	 * Check the host
	 * @var string
	 */
	$host = get_url_host( $url );
	/**
	 * Declare src
	 * @var string
	 */
	$src = '';
	/**
	 * Show/hide autoplay control
	 * @var bool
	 */
	if ( $autoplay === true ) {
		$play = '&autoplay=1';
	} else {
		$play = '';
	}

	if ( strpos( $host, 'youtu' ) !== false ) {
		/**
		 * Get video id
		 * @var string
		 */
		$video_id = get_youtube_video_id( $url );
		/**
		 * Build youtube iframe src
		 * @var string
		 */
		$src = 'https://www.youtube.com/embed/' . $video_id . '?loop=' . $loop . '&enablejsapi=1' . $play;

	} elseif ( strpos( $host, 'vimeo' ) !== false ) {
		/**
		 * Get video id
		 * @var string
		 */
		$video_id = get_vimeo_video_id( $url );
		/**
		 * Build vimeo iframe src
		 * @var string
		 */
		$src = 'https://player.vimeo.com/video/' . $video_id . '?loop=' . $loop . '&api=1&player_id=' . $iframe_id . $play;
	}

	return $src;
}
/**
 * Build video iframe
 *
 * Build video iframe.
 *
 * @param string $url         Video url
 * @param string $iframe_id   HTML id for <iframe>
 * @param string $width       Width for the iframe
 * @param string $height      Height for the iframe
 * @param bool $autoplay      Include autoplay control for player, true or false
 *                            (sometimes we need to include it via js and not to have it here at all)
 * @param string $loop        Loop control for player, '0' and '1' are valid values
 * @return string             Returns <iframe> markup
 */
function build_video_iframe( $url, $iframe_id = 'video-player', $width = '100%', $height = '100%', $autoplay = false, $loop = '0' ) {
	$src = set_iframe_src( $url, $iframe_id, $autoplay, $loop );

	if ( $src ) {
		$iframe = '<iframe class="embedded__item"
						id="' . $iframe_id . '"
						src="' . $src . '"
						width="' . $width . '"
						height="' . $height . '"
						frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
					</iframe>';
	} else {
		$iframe = '';
	}

	return $iframe;
}

/**
 * Video iframe
 *
 * Echo video iframe.
 *
 * @param string $url         Video url
 * @param string $iframe_id   HTML id for <iframe>, needed for Vimeo videos, optional for YouTube
 * @param string $width       Width for the iframe
 * @param string $height      Height for the iframe
 * @param bool $autoplay      Include autoplay control for player, true or false
 *                            (sometimes we need to include it via js and not to have it here at all)
 * @param string $loop        Loop control for player, '0' and '1' are valid values
 * @return string             Echoes <iframe> markup
 */
function video_iframe( $url, $iframe_id = 'video-player', $width = '100%', $height = '100%', $autoplay = false, $loop = '0' ) {
	echo build_video_iframe( $url, $iframe_id, $width, $height, $autoplay, $loop );
}

/**
 * Set video ID
 *
 * Check where is video coming from and set its id.
 *
 * @param string $url         Video url
 * @return string             Returns video id
 */
function set_video_id( $url ) {
	/**
	 * Check the host
	 * @var string
	 */
	$host = get_url_host( $url );
	/**
	 * Declare id
	 * @var string
	 */
	$id = '';

	if ( strpos( $host, 'youtu' ) !== false ) {
		/**
		 * Get youtube image id
		 * @var string
		 */
		$id = get_youtube_video_id( $url );

	} elseif ( strpos( $host, 'vimeo' ) !== false ) {
		/**
		 * Get vimeo image id
		 * @var string
		 */
		$id = get_vimeo_video_id( $url, 'large' );
	}

	return $id;
}