<?php
/**
 * Instagram functions
 *
 * Custom functionality for getting instagram feed
 *
 * Uses third party script 'Instagram PHP Scraper'.
 * @link https://packagist.org/packages/raiym/instagram-php-scraper
 * @link https://github.com/raiym/instagram-php-scraper
 *
 * @package WordPress
 * @subpackage Instagram PHP Scraper
 */

/**
 * These lines are mandatory.
 */
$file = get_template_directory() . '/inc/api/vendor/autoload.php';

/**
 * Let's not kill all if someone forgot to run composer,
 * or is frontend :P
 */
if ( ! file_exists( $file ) ) {
	return;
}

require_once( $file );

use InstagramScraper\Instagram;

function house_get_instagram_media( $account = '', $number = 0 ) {
	$instagram = new Instagram;
	$media = $instagram->getMedias( $account, $number );

	return $media;	
}