<?php
/**
 * TinyMCE functions
 *
 * @package WordPress
 */
/**
 * Hooks
 */
add_action( 'init', 'house_buttons' );
/**
 * Hook buttons to correct filters
 */
function house_buttons() {
	// check user permissions
	if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
		return;
	}
	/**
	 * Add filters
	 */
    add_filter( 'mce_external_plugins', 'house_add_buttons' );
    add_filter( 'mce_buttons', 'house_register_buttons' );
}
/**
 * Declare script for new button
 * @param  array $plugin_array Plugin array
 * @return array               Returns updated array
 */
function house_add_buttons( $plugin_array ) {
    $plugin_array['house'] = get_template_directory_uri() . '/inc/shortcodes/js/tinymce.js';
    return $plugin_array;
}
/**
 * Register new button in the editor
 * @param  array $buttons Array of existing buttons
 * @return array          Updated array of buttons
 */
function house_register_buttons( $buttons ) {
	/**
	 * If any custom buttons, just push them to array
	 * e.g. '|', 'custom_button'
	 *
	 * Don't forget to add these to ACF WYSIWYG
	 * /inc/plugins/acf.php
	 */
    array_push( $buttons, '|' );
    return $buttons;
}