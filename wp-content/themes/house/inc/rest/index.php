<?php
/**
 * Here include all REST API related custom functions
 *
 * @package WordPress
 */
include( get_template_directory() . '/inc/rest/events.php' );