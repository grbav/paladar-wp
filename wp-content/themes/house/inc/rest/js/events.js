'use strict';
/**
 * AJAX script for categories filter
 */
var $ = window.jQuery;

var $events_container   = HOUSE_EVENTS.container;
var $events_url_all     = HOUSE_EVENTS.all;
var $events_url_future  = HOUSE_EVENTS.future;
var $events_url_past    = HOUSE_EVENTS.past;
var $events_trigger_all = HOUSE_EVENTS.trigger_all;
var $events_all_pages   = HOUSE_EVENTS.total_pages_all;

var $total_pages_all = +$events_all_pages - 1;
var $count_page = 1;
var $count_button_click = 0;

//WINDOW ONLOAD
$(window).load(function() {

	var $ajax_container = $($events_container);

	// masonry has problem calculating image size loaded with ajax which causes
	// items to overlap
	// defining 'itemSelector' again in ajax call seems to fix the issue
	// @link https://github.com/desandro/masonry/issues/369#issuecomment-41149679
	$ajax_container.imagesLoaded( function() {
		$ajax_container.isotope({
			itemSelector: '.grid-item'
		});
	});
});

//ON DOCUMENT READY
$(document).ready(function() {

	/**
	 * AJAX call for all events
	 */
	function house_load_all_events() {

		$count_page++;
		// The AJAX
		$.ajax({
			dataType: 'json',
			url: $events_url_all + '&page=' + $count_page,
			complete: function(xhr) {
				// https://stackoverflow.com/a/30891843
				// an easy way to get headers from rest response
				// can be useful for paginations
				var $number_of_posts = xhr.getResponseHeader('X-WP-Total');
				var $number_of_pages = xhr.getResponseHeader('X-WP-TotalPages');
			}
		})
		/**
		 * When we have response
		 */
		.done(function(response){

			$.each( response, function(index, object) {
				var $ajax_container = $($events_container);
				/**
				 * Get our custom filed 'markup'
				 * This field is added in '.php' file in order to
				 * avoid writing markup here
				 */
				var $markup = object.markup;
				var $loadedData = $($markup);

				// load images before isotope is called
				// so that items don't overlap
				$ajax_container.append( $loadedData ).imagesLoaded( function() {
					$ajax_container.isotope( 'appended', $loadedData, function () {
						$ajax_container.isotope( 'reLayout' );
					});
				});
			});
		})
		/**
		 * On error
		 */
		.fail( function(jqXHR, textStatus) {
			/**
			 * Show the errors in console
			 */
			console.log(textStatus);
			console.log(jqXHR);
		});

		return false;
	}
	/**
	 * Performing AJAX call for all events when button is clicked
	 */
	$($events_trigger_all).on( 'click', function(e) {
		e.preventDefault();

		// Count clicks on button
		$count_button_click++;

		// Hide/disable the button if no more 'pages'
		// (total number of pages - 1 is equal to number of clicks)
		if ( $count_button_click === $total_pages_all ) {
			$($events_trigger_all).hide();
		}
		house_load_all_events();
	});

});
