<?php
/**
 * Categories filters - REST API functionality
 *
 * @link https://developer.wordpress.org/rest-api/
 * @package WordPress
 */
// Force a short-init since we just need core WP, not the entire framework stack
if ( ! defined( 'SHORTINIT' ) ) {
	define( 'SHORTINIT', true );
}
/**
 * Hooks
 */
add_action( 'wp_enqueue_scripts', 'house_events_enqueue_scripts' );
/**
 * Helper function for building rest url
 * @param  array $args   Array of arguments
 * @return string        Returns rest url
 */
function house_events_add_query_arg( $args ) {
	/**
	 * Build json URL
	 *
	 * @link https://developer.wordpress.org/reference/functions/add_query_arg/
	 * @link https://developer.wordpress.org/reference/functions/rest_url/
	 * @var string
	 */
	$url = add_query_arg( $args, rest_url( 'wp/v2/event') );

	return $url;
}
/**
 * Prepare json query url
 * @return string Returns json url
 */
function house_all_events_rest_query() {
	/**
	 * Set up the query variables for category ID and posts per page
	 * @var array
	 */
	$args = array(
		'per_page' => get_option( 'posts_per_page' ),
	);

	return $args;
}
function house_future_events_rest_query() {

	$args = array(
		'per_page' => get_option( 'posts_per_page' ),
		'after'    => date( 'Y-m-d H:i:s' )
	);

	return $args;
}
function house_past_events_rest_query() {

	$args = array(
		'per_page' => get_option( 'posts_per_page' ),
		'before'   => date( 'Y-m-d H:i:s' )
	);

	return $args;
}
add_action( 'rest_api_init', 'house_event_register_fields' );
/**
 * Register custom rest fields
 *
 * This function is attached to 'rest_api_init' action hook.
 *
 * @link https://developer.wordpress.org/rest-api/extending-the-rest-api/modifying-responses/
 * @return obj Returns custom rest fields
 */
function house_event_register_fields() {
	/**
	 * Register 'markup' field
	 *
	 * @param string|array $object_type  The name of the object the field is being registered to, here 'event'
	 * @param string $attribute  The name of the field. This name will be used to define the key in the response object.
	 * @param array $args  An array with keys that define the callback functions
	 */
	register_rest_field( 'event', 'markup', array(
		'get_callback'    => 'house_event_markup',
		'update_callback' => null,
		'schema'          => null
	));
}
/**
 * Markup - Custom rest field
 *
 * Populate custom json field with markup to be used for response.
 * @link https://developer.wordpress.org/rest-api/extending-the-rest-api/modifying-responses/
 *
 * @param  array $object                Returned post json object
 * @param  string $field_name           Name of field.
 * @param  WP_REST_Request $request     Current request
 * @return mixed
 */
function house_event_markup( $object, $field_name, $request ) {
	/**
	 * Start the markup
	 * @var string
	 */
	$output = '';
	/**
	 * Post id
	 * @var int
	 */
	$post_id = $object['id'];
	/**
	 * Featured image id
	 * @var int
	 */
	$post_attachment_id = $object['featured_media'];
	/**
	 * Post title
	 * @var string
	 */
	$post_title = $object['title']['rendered'];
	/**
	 * Post publish date and time
	 * @var string|date
	 */
	$post_datetime = $object['date'];
	/**
	 * Post permalink
	 * @var string
	 */
	$post_permalink = $object['link'];
	/**
	 * ACF event end time
	 * @var date
	 */
	$event_end_time = get_field( 'end_time', $post_id );
	$event_location = get_field( 'event_location', $post_id );

	$output .= '<div class="grid-item ' . house_set_event_class( $post_id ) . '">';
	$output .= '<div class="event-item">';

	/**
	 * Build featured image markup if we have attachment
	 *
	 * If we have featured image then 'featured_media'
	 * id will be higher than 0
	 */
	if ( $post_attachment_id > 0 ) :
		/**
		 * Get featured image
		 * @var array
		 */
		$featured_img_array = wp_get_attachment_image_src( $post_attachment_id, 'large' );
		/**
		 * Get the src
		 * @var string
		 */
		$featured_img_src = $featured_img_array[0];
		$featured_img_markup = '<a href="' . $post_permalink . '">';
		$featured_img_markup .= '<div class="event-item-image" style="background-image: url(' . $featured_img_src . ');"></div>';
		$featured_img_markup .= '</a>';
		/**
		 * Add featured image markup to our output
		 */
		$output .= $featured_img_markup;

	endif; // $post_attachment_id > 0

	$output .= '<div class="event-time">';
	$output .= '<time>' . date( "F j, Y" , strtotime( $post_datetime ) ) . '</time>';
	$output .= '<time>' . date( "h:i A" , strtotime( $post_datetime ) );
	if ( $event_end_time ) :
		$output .= ' - ' . date( "h:i A" , strtotime( $event_end_time ) );
	endif; // $event_end_time
	$output .= '</time>';
	$output .= '</div><!-- event-time -->';

	$output .= '<div class="event-desc">';
	$output .= '<h3 class="event-title"><a href="' . $post_permalink . '" rel="bookmark">' . $post_title . '</a></h3>';
	if ( $event_location ) :
		$output .= '<small class="event-location">' . $event_location . '</small>';
	endif; // $event_location
	$output .= '</div><!-- event-desc -->';

	$output .= '<div class="event-more-info">';
	$output .= '<a href="' . $post_permalink . '" rel="bookmark">' . esc_html__( 'View details', 'house' ) . '</a>';
	$output .= house_share_post( 'event', $post_id );
	$output .= '</div><!-- event-more-info -->';

	$output .= '</div><!-- event-item -->';
	$output .= '</div><!-- grid-item -->';

	return $output;
}
/**
 * Enqueue scripts
 *
 * Properly enqueue scripts for ajax filter, only on posts page - index.php.
 * This function is attached to 'wp_enqueue_scripts' action hook.
 *
 * @link https://developer.wordpress.org/reference/functions/wp_enqueue_scripts/
 */
function house_events_enqueue_scripts() {
	if ( is_page_template( 'templates/template-events.php' ) ) :
		wp_enqueue_script( 'imagesloaded' );
		wp_enqueue_script( 'events-rest', get_template_directory_uri() . '/inc/rest/js/events.js', array( 'jquery' ), '0.1', true );
		/**
		 * Initial query used for all events in template-events.php
		 * @var obj|Error new WP_Query
		 */
		$query_all = house_get_all_events_posts();

		/**
		 * Register a global variable to be used in js files.
		 * Use the last enqueued - 'events-rest' - so that all files are covered.
		 *
		 * @link https://developer.wordpress.org/reference/functions/wp_localize_script/
		 */
		wp_localize_script( 'events-rest', 'HOUSE_EVENTS', array(
			'all'             => house_events_add_query_arg( house_all_events_rest_query() ),
			'future'          => house_events_add_query_arg( house_future_events_rest_query() ),
			'past'            => house_events_add_query_arg( house_past_events_rest_query() ),
			'trigger_all'     => '#load-all-events',
			'container'       => '#container-events',
			'total_pages_all' => $query_all->max_num_pages
		));
	endif; // is_page_template( 'templates/template-events.php' )
}