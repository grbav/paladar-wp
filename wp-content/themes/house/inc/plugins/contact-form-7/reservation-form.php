<?php
/**
 * Contact Form 7 customizations
 *
 * Create all default customizations here. Contact Form 7 lacks dev's documentation
 * in both places, website and code. However, there are some helpful links out there,
 * plus reading the plugin' code itself can make workarounds.
 *
 * @link https://github.com/jgrossi/contact-form-7-hooks
 * @link http://xaviesteve.com/3298/wordpress-contact-form-7-hook-unofficial-developer-documentation-and-examples/
 * @link https://plugins.trac.wordpress.org/browser/contact-form-7-accessible-defaults/trunk/contact-form-7-accessible-form.php
 * @link http://contactform7.com/2015/03/28/custom-validation/
 * @link http://code.tutsplus.com/tutorials/mini-guide-to-contact-form-7--wp-25086
 *
 * @package WordPress
 * @subpackage Contact Form 7
 */
/**
 * Reservation form template
 *
 * Create reservation form template. Default template can be found in plugin's
 * files 'wp-content/plugins/contact-form-7/includes/contact-form-template.php'
 *
 * @see WPCF7_ContactFormTemplate::form()
 *
 * @return string Returns form markup
 */
function reservation_form_template() {
	/**
	 * Set name
	 * @var string
	 */
	$name = '[text* reservation-name id:reservation-name class:input class:input--primary placeholder "Name and surename"]';
	/**
	 * Set phone
	 * @var string
	 */
	$phone = '[tel reservation-phone id:reservation-phone class:input class:input--primary placeholder "Contact Phone"]';
	/**
	 * Set email
	 * @var string
	 */
	$email = '[email* reservation-email id:reservation-email class:input class:input--primary placeholder "Email"]';
	/**
	 * Set number of guests
	 * @var string
	 */
	$guests = '[select reservation-guests id:reservation-guests "1 guest" "2 guests" "3 guests" "4 guests" "5 guests" "6 guests" "7 guests" "8 guests" ]';
	/**
	 * Set date
	 * @var string
	 */
	$date = '[text* reservation-date id:reservation-date class:input class:input--primary class:datepicker placeholder "Reservation Date"]';
	/**
	 * Set time
	 * @var string
	 */
	$time = '[select* reservation-time id:reservation-time "10:00 AM" "10:30 AM" "11:00 AM" "11:30 AM" "12:00 AM" "12:30 PM" "1:00 PM"]';
	/**
	 * Set textarea
	 * @var string
	 */
	$message = '[textarea reservation-msg id:reservation-msg class:input class:input--textarea placeholder "Additional requirements"]';
	/**
	 * Set submit button
	 * @var string
	 */
	$submit = '[submit id:reservation-submit class:btn class:btn--secondary "Book now"]';
	/**
	 * Anti-spam field
	 * Hidden field that should stay empty if user is human
	 * @uses Contact Form 7 Honeypot plugin
	 */
	$spam = '[honeypot reservation-website]';

	/**
	 * Add filters so that we can easily change custom markup by
	 * wrapping fields or adding any markup before or after each field.
	 *
	 * Adding more fields is, obviously, possible, but then we need to modify
	 * email settings as well. For this purpose it's better to create new form and email templates.
	 */
	$form .= '<div class="flex-row">';
	$form .= '<div class="col-6">' . $name . '</div><!-- /.col-6 -->';
	$form .= '<div class="col-6">' . $phone . '</div><!-- /.col-6 -->';
	$form .= '<div class="col-6">' . $email . '</div><!-- /.col-6 -->';
	$form .= '<div class="col-6">' . $guests . '</div><!-- /.col-6 -->';
	$form .= '<div class="col-6"><div class="input-wrap">' . $date . '</div><!-- /.input-wrap --></div><!-- /.col-6 -->';
	$form .= '<div class="col-6"><div class="input-wrap">' . $time . '</div><!-- /.input-wrap --></div><!-- /.col-6 -->';
	// Throw in hidden field is Honeypot plugin is active.
	// Wrapping into <div> will prevent CF7 to wrap field into <p>
	// and apply margins and other unwanted styling
	if ( house_is_plugin_active( 'contact-form-7-honeypot/honeypot.php' ) ) {
		$form .= '<div>' . $spam . '</div>';
	}
	$form .= '<div class="col-12">' . $message . '</div><!-- /.col-12 -->';
	$form .= '<div class="col-12"><div class="text-center">' . $submit . '</div><!-- /.float-center --></div><!-- /.col -->';
	$form .= '</div><!-- /.flex-row -->';

	return $form;
}
/**
 * Reservation form sender email
 *
 * Set the 'from' email address to be used in email template. Default template can be found in plugin's
 * files 'wp-content/plugins/contact-form-7/includes/contact-form-template.php'
 *
 * @see WPCF7_ContactFormTemplate::from_email()
 *
 * @return string Returns 'from' email address
 */
function reservation_form_sender_email() {
	$admin_email = get_option( 'admin_email' );
	$sitename = strtolower( $_SERVER['SERVER_NAME'] );

	if ( wpcf7_is_localhost() ) {
		return $admin_email;
	}

	if ( substr( $sitename, 0, 4 ) == 'www.' ) {
		$sitename = substr( $sitename, 4 );
	}

	if ( strpbrk( $admin_email, '@' ) == '@' . $sitename ) {
		return $admin_email;
	}

	return 'contact@' . $sitename;
}
/**
 * Reservation form email to client
 *
 * Settings for custom email template that will be sent to client
 * as direct message from site visitor. Default template can be found in plugin's
 * files 'wp-content/plugins/contact-form-7/includes/contact-form-template.php'
 *
 * @see WPCF7_ContactFormTemplate::mail()
 *
 * @link http://contactform7.com/special-mail-tags/
 * @return array Returns array of email settings
 */
function reservation_form_email_to_client() {
	/**
	 * Set 'to'
	 * @var string
	 */
	$recipient = get_option( 'admin_email' );
	/**
	 * Set 'from'
	 * @var string
	 */
	$sender = sprintf( '%1$s <%2$s>', get_bloginfo( 'name' ), simple_form_sender_email() );
	/**
	 * Set subject
	 * @var string
	 */
	$subject = sprintf(	_x( 'Message from %2$s to %1$s ', 'mail subject', 'house' ),
		get_bloginfo( 'name' ),
		'[reservation-name]'
	);
	/**
	 * Set additional headers
	 * @var string
	 */
	$additional_headers = 'Reply-To: [reservation-email]';
	/**
	 * Set email body
	 * @var string
	 */
	$body = sprintf( __( 'You have received new message on %s.', 'house' ), get_bloginfo( 'name' ) ) . "\n\n"
			. __( 'Sender has provided following info:', 'house' ) . "\n\n"
			. sprintf( __( 'Name: %s', 'house' ), '[reservation-name]' ) . "\n"
			. sprintf( __( 'Phone: %s', 'house' ), '[reservation-phone]' ) . "\n"
			. sprintf( __( 'Email: %s', 'house' ), '[reservation-email]' ) . "\n"
			. sprintf( __( 'Guests number: %s', 'house' ), '[reservation-guests]' ) . "\n"
			. sprintf( __( 'Reservation date: %s', 'house' ), '[reservation-date]' ) . "\n"
			. sprintf( __( 'Reservation time: %s', 'house' ), '[reservation-time]' ) . "\n"
			. __( 'Message:', 'house' ) . "\n"
			. '[reservation-msg]' . "\n\n\n"
			. sprintf( __( 'This e-mail was sent from %1$s, on %2$s at %3$s.', 'house' ),
				'[_url]',
				'[_date]',
				'[_time]'
			);

	$template = array(
		'subject'            => $subject,
		'sender'             => $sender,
		'body'               => $body,
		'recipient'          => $recipient,
		'additional_headers' => $additional_headers,
		'attachments'        => '',
		'use_html'           => 0,
		'exclude_blank'      => 1
	);

	return $template;
}
/**
 * Custom Autoresponder
 *
 * Settings for custom email template that will be sent to site visitor
 * as autoresponder. Default template can be found in plugin's files
 * 'wp-content/plugins/contact-form-7/includes/contact-form-template.php'
 *
 * @see WPCF7_ContactFormTemplate::mail_2()
 *
 * @return array Returns array of email settings
 */
function reservation_form_autoresponder() {
	/**
	 * Set 'to'
	 * @var string
	 */
	$recipient = '[reservation-email]';
	/**
	 * Set 'from'
	 * @var string
	 */
	$sender = sprintf( '%1$s <%2$s>', get_bloginfo( 'name' ), simple_form_sender_email() );
	/**
	 * Set subject
	 * @var string
	 */
	$subject = sprintf(	_x( 'Your message to %s has been sent', 'mail subject', 'house' ),
		get_bloginfo( 'name' )
	);
	/**
	 * Set additional headers
	 * @var string
	 */
	$additional_headers = sprintf( 'Reply-To: %s', get_option( 'admin_email' ) );
	/**
	 * Set email body
	 * @var string
	 */
	$body = sprintf( __( 'Hello %s,', 'house' ), '[reservation-name]' ) . "\n\n"

			. sprintf( __( 'Your message to %s has been sent.', 'house' ), get_bloginfo( 'name' ) )
			. __( 'Please, do not reply to this email. We will get back to you as soon as possible.', 'house' ) . "\n\n"

			. __( 'You have provided following info:', 'house' ) . "\n\n"
			. sprintf( __( 'Name: %s', 'house' ), '[reservation-name]' ) . "\n"
			. sprintf( __( 'Phone: %s', 'house' ), '[reservation-phone]' ) . "\n"
			. sprintf( __( 'Email: %s', 'house' ), '[reservation-email]' ) . "\n"
			. sprintf( __( 'Guests number: %s', 'house' ), '[reservation-guests]' ) . "\n"
			. sprintf( __( 'Reservation date: %s', 'house' ), '[reservation-date]' ) . "\n"
			. sprintf( __( 'Reservation time: %s', 'house' ), '[reservation-time]' ) . "\n"
			. __( 'Message:', 'house' ) . "\n"
			. '[reservation-msg]' . "\n\n\n"

			. sprintf( __( 'This e-mail was sent from %1$s, on %2$s at %3$s.', 'house' ),
				'[_url]',
				'[_date]',
				'[_time]'
			);

	$template = array(
		'active'             => true,
		'subject'            => $subject,
		'sender'             => $sender,
		'body'               => $body,
		'recipient'          => $recipient,
		'additional_headers' => $additional_headers,
		'attachments'        => '',
		'use_html'           => 0,
		'exclude_blank'      => 1
	);

	return $template;
}
