<?php
/**
 * Post queries for ajax loading
 *
 * Ajax load more posts functionality. Uses House Buzz plugin.
 *
 * @package WordPress
 * @subpackage House Buzz
 */
/**
 * Hooks
 */
add_action( 'wp_ajax_nopriv_ajax_load_posts', 'ajax_load_posts' );
add_action( 'wp_ajax_ajax_load_posts', 'ajax_load_posts' );
/**
 * Prepare AJAX query args
 *
 * We need to filter Buzz plugin's query args and add
 * stuff needed for the ajax.
 *
 * @param  array $args   Query args
 * @return array         Returns filtered query args
 */
function get_ajax_query_args( $args ) {
	global $globalSite;

	$all = get_all_posts_ids( 'twitter_data_wrap', 'option' );

	$posts_num = $globalSite['posts_per_page'];
	$ppp = ( isset( $_POST["ppp"] ) ) ? $_POST["ppp"] : $posts_num;
	$page = ( isset( $_POST['pageNumber'] ) ) ? $_POST['pageNumber'] : 0;

	// header( "Content-Type: text/html" );

	$args['posts_per_page'] = $ppp;
	$args['paged'] = $page;
	$args['post__in'] = $all;

	return $args;
}
/**
 * Get Ajax query
 *
 * Get prepared query args and run the query.
 * @see  get_ajax_query_args()
 *
 * @return obj|error Returns wp_query object
 */
function get_ajax_query() {
	global $wp_query;
	/**
	 * Fire filter for modifying query
	 * @see get_ajax_query_args()
	 */
	add_filter( 'buzz_posts_query', 'get_ajax_query_args' );
	/**
	 * Get the query
	 * @var obj
	 */
	if ( function_exists( 'get_all_posts_query' ) ) {
		$query = get_all_posts_query( 'twitter_data_wrap', 'option' );
	} else {
		$query = $wp_query;
	}

	return apply_filters( 'ajax_query', $query );

	/**
	 * Remove filter for modifying query
	 * so that it doesn't effect other queries
	 *
	 * @see get_ajax_query_args()
	 */
	remove_filter( 'buzz_posts_query', 'get_ajax_query_args' );
}
/**
 * Get blog posts ids 
 *
 * Create query and get ids for all posts that 
 * meet the query. We will use these ids as argument
 * for ajax query.
 * 
 * @return array Returns array of post ids
 */
function house_get_blog_posts_ids() {

	$args = array(
		'posts_per_page' => -1,
		'post_type' => 'post',
		'tax_query' => array( array(
				'taxonomy' => 'post_format',
	            'field' => 'slug',
	            'terms' => array( 'post-format-image' ),
	            'operator' => 'NOT IN'
			)
		),
	);

	$posts = get_posts( $args );

	$ids = array();

	foreach ( $posts as $post ) {
		$ids[] = $post->ID;
	}

	return $ids;
}
/**
 * Prepare query for ajax
 * 
 * @return obj|Error new WP_Query
 */
function house_get_blog_posts() {
	global $globalSite;
	$all = house_get_blog_posts_ids();

	$ppp = ( isset( $_POST["ppp"] ) ) ? $_POST["ppp"] : $globalSite['posts_per_page'];
	$page = ( isset( $_POST['pageNumber'] ) ) ? $_POST['pageNumber'] : 0;

	$args = array(
		'post_type'      => 'post',
		'posts_per_page' => $ppp,
		'paged'          => $page,
		'post__not_in' => get_option( 'sticky_posts' ),
		'tax_query' => array(
	        array(
	            'taxonomy' => 'post_format',
	            'field' => 'slug',
	            'terms' => array( 'post-format-image' ),
	            'operator' => 'NOT IN'
	        )
	    )
	);

	$query = new WP_Query( $args );
	return $query;
}
/**
 * AJAX action handle
 * @return mix Returns posts
 */
function ajax_load_posts() {
	// global $globalSites;
	/**
	 * Fire filter for modifying query
	 * @see get_ajax_query_args()
	 */
	// add_filter( 'buzz_posts_query', 'get_ajax_query_args' );
	/**
	 * Get the query
	 * @var obj
	 */
	$query = house_get_blog_posts();


	$out = '';

	if ( $query->have_posts() ) :  while ( $query->have_posts() ) : $query->the_post();

	get_template_part( 'content', 'buzz' );

	endwhile; endif;

	/**
	 * Always reset custom queries
	 */
	wp_reset_postdata();

	die( $out );
	/**
	 * Remove filter for modifying query
	 * so that it doesn't effect other queries
	 *
	 * @see get_ajax_query_args()
	 */
	// remove_filter( 'buzz_posts_query', 'get_ajax_query_args' );
}
/**
 * Set new globals needed for use in javascript part of ajax
 */
global $globalSite;

	/**
	 * Get the prepared query and store total page number in globals
	 */
	$query = house_get_blog_posts();
	$globalSite['ajaxpages'] = $query->max_num_pages;

	/**
	 * Store ajax action handle in globals
	 */
	$globalSite['ajax_action'] = 'ajax_load_posts';

/**
 * Get related posts
 *
 * Get all posts categories and find posts with at least
 * one mutual category. Display 4 posts by default.
 *
 * @param  integer $number Number of posts to return
 * @return obj|Error          Retrns WP_Query object
 */
function house_get_related_posts( $number = 4 ) {
	/**
	 * Get all post categories
	 * @var array of term objects
	 */
	$categories = get_the_category();
	/**
	 * Declare our custom array for ids
	 * @var array
	 */
	$cat_ids = array();
	/**
	 * Get term id for each category
	 * and store it in our array
	 */
	foreach ( $categories as $cat ) {
		$cat_ids[] = $cat->term_id;
	}
	/**
	 * Set query arguments:
	 * posts in same post type with at least one mutual category but
	 * not the post we are already on
	 * @var array
	 */
	$args = [
		'post_type'      => get_post_type(),
		'posts_per_page' => $number,
		'category__in'   => $cat_ids,
		'post__not_in'   => [get_the_ID()],
		'tax_query' => array(
	        array(
	            'taxonomy' => 'post_format',
	            'field' => 'slug',
	            'terms' => array( 'post-format-image' ),
	            'operator' => 'NOT IN'
	        )
	    )
	];

	$query = new WP_Query( $args );

	return $query;
}