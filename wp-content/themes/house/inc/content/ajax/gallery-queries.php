<?php
/**
 * Gallery queries for ajax loading
 *
 * Ajax load more posts functionality.
 *
 * @package WordPress
 */
/**
 * Hooks
 */
add_action( 'wp_ajax_nopriv_ajax_load_gallery', 'ajax_load_gallery' );
add_action( 'wp_ajax_ajax_load_gallery', 'ajax_load_gallery' );
/**
 * Get blog posts ids 
 *
 * Create query and get ids for all posts that 
 * meet the query. We will use these ids as argument
 * for ajax query.
 * 
 * @return array Returns array of post ids
 */
function house_get_gallery_posts_ids() {

	$args = array(
		'posts_per_page' => -1,
		'post_type' => 'post',
		'tax_query' => array( array(
				'taxonomy' => 'post_format',
	            'field' => 'slug',
	            'terms' => 'post-format-image'
			)
		),
	);

	$posts = get_posts( $args );

	$ids = array();

	foreach ( $posts as $post ) {
		$ids[] = $post->ID;
	}

	return $ids;
}
/**
 * Prepare query for ajax
 * 
 * @return obj|Error new WP_Query
 */
function house_get_gallery_posts() {
	global $globalSite;
	$all = house_get_gallery_posts_ids();

	$ppp = ( isset( $_POST["ppp"] ) ) ? $_POST["ppp"] : 12;
	$page = ( isset( $_POST['pageNumber'] ) ) ? $_POST['pageNumber'] : 0;

	$args = array(
		'post_type'      => 'post',
		'posts_per_page' => $ppp,
		'post__in'       => $all,
		'paged'          => $page
	);

	$query = new WP_Query( $args );
	return $query;
}
/**
 * AJAX action handle
 * @return mix Returns posts
 */
function ajax_load_gallery() {
	/**
	 * Get the query
	 * @var obj
	 */
	$query = house_get_gallery_posts();


	$out = '';

	get_template_part( 'content', 'gallery' );

	die( $out );
}
/**
 * Set new globals needed for use in javascript part of ajax
 */
global $globalSite;

	/**
	 * Get the prepared query and store total page number in globals
	 */
	$query = house_get_gallery_posts();
	$globalSite['ajax_gallery_pages'] = $query->max_num_pages;

	/**
	 * Store ajax action handle in globals
	 */
	$globalSite['ajax_gallery_action'] = 'ajax_load_gallery';