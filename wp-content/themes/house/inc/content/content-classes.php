<?php
/**
 * Extends the default WordPress classes
 *
 * Add more classes to body and article tags
 *
 * @package WordPress
 */

/**
 * Hooks
 */
add_filter( 'body_class', 'house_body_class' );
add_filter( 'post_class' , 'house_cpt_post_class' );

/**
 * HTML classes
 *
 * Add custom classes for <html> as per frontend.
 *
 * @return string Returns custom classes
 */
function house_html_class() {

	$class = '';

	if ( is_front_page() ) {
		$class = 'homepage';
	}

	echo $class;
}
/**
 * Body classes
 * @param array Existing class values.
 * @return array Filtered class values.
 *
 */
function house_body_class( $classes ) {

	// Single author classes
	// if there is only one author with published posts - add class 'single-author'
	if ( ! is_multi_author() ) {
		$classes[] = 'single-author';
	} else {
		$classes[] = 'multi-author';
	}

	if ( has_post_format() ) {
		if ( is_tax( 'post_format' ) ) {
			$classes[] = 'archive-format';
			$classes[] = 'archive-format-' . get_post_format();
		} elseif ( is_single() ) {
			$classes[] = 'single-format';
		}
	}

	// template classes - singular and not
	if ( is_singular() ) {
		$classes[] = 'singular';
	} else {
		$classes[] = 'not-singular';
	}

	// date archive classes
	if ( is_year() ) {
		$classes[] = 'date-year';
	}
	if ( is_month() ) {
		$classes[] = 'date-month';
	}
	if ( is_day() ) {
		$classes[] = 'date-day';
	}

	// finally return classes
	return $classes;
}
/**
 * Post (article) classes
 */
function house_cpt_post_class( $classes ) {
	global $current_class, $post, $number_class;
	// get post type
	$post_type = get_post_type( get_the_ID() );

	// Odd/even classes
	if ( 'page' != $post_type ) {
		$classes[] = $current_class;
		$current_class = ( $current_class == 'odd' ) ? 'even' : 'odd';
	}

	// Featured image classes
	// add special classes based on whether we have featured image or not
	// posts and pages included
	if ( has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	} else {
		$classes[] = 'no-post-thumbnail';
	}

	// Taxonomies classes for custom post types
	// if post type is not 'post' or 'page' - add 'attachment' if you wish
	if ( ! in_array( $post_type, array( 'post', 'page' ) ) ) : {
		// go to taxonomies array
		$post_type_taxonomies = get_object_taxonomies( $post_type );

		foreach ( $post_type_taxonomies as $taxonomy ) {
			// get all terms for each taxonomy
			$terms = get_the_terms( get_the_ID(), $taxonomy );
			// if we have any term
			if ( ! empty( $terms ) ) {
				// loop through each of them
				foreach ( $terms as $term ) {
					$taxonomy	= $term->taxonomy; // this will get taxonomy slug
					$slug		= $term->slug; // this will get term slug
					// finally build our classes in WordPress' default format for categories and tags
					$classes[] = $taxonomy . '-' . $slug;
				}
			}
		}

		return $classes;
	}
	endif;

	// Ordinal number classes for articles on archive pages
	if ( ! is_singular() ) {

		if ( $number_class %1 == 0 ) {
			$classes[] = 'post-count-' . $number_class;
		}

    	$number_class++;
	}

	return $classes;
}

// set the first class to be 'odd'
global $current_class, $number_class;
$current_class = 'odd';
$number_class = 1;

/**
 * MAIN classes
 *
 * Add custom classes for <main> as per frontend.
 *
 * @return string Returns custom classes
 */
function house_main_class() {

	// Set class
	if ( is_single() ) :
	    $class = 'animation fade-in-top';
	else : 
	    $class = '';
	endif;

	echo $class;
}