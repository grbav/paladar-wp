<?php
/**
 * Custom functions not related to anything specific
 */

/**
 * Trim string
 *
 * When we need limited number of characters for a string (i.e excerpt )
 *
 * @link http://stackoverflow.com/questions/12423407/limiting-characters-retrived-from-a-database-field#answer-12423453
 *
 * @param  string 	$str    		String to be trimmed
 * @param  integer 	$length 		Number of characters
 * @return string         			Trimmed string
 */
function trim_string( $str, $length, $after = '...' ) {

	if ( ! ( strlen( $str ) <= $length ) ) {
		$str = substr( $str, 0, strpos( $str, ' ', $length ) ) . ' ' . $after;
	}

	return $str;
}

/**
 * Admin menu Chrome fix
 *
 * Dashboard menu gets broken on hover in Chrome.
 *
 * @link https://core.trac.wordpress.org/ticket/33199
 * @link http://wordpress.stackexchange.com/questions/200096/admin-sidebar-items-overlapping-in-admin-panel
 */
add_action( 'admin_enqueue_scripts', 'chrome_fix' );

function chrome_fix() {

	if ( strpos( $_SERVER[ 'HTTP_USER_AGENT' ], 'Chrome' ) !== false ) {
		wp_add_inline_style( 'wp-admin', '#adminmenu { transform: translateZ(0) }' );
	}
}
/**
 * Define irregular plurals
 *
 * @return array Array of irregular plurals
 */
function irregular_plurals() {

	$irregulars = array(
		'man'           => 'men',
		'woman'         => 'women',
		'fungus'        => 'fungi',
		'thief'         => 'thieves',
		'medium'        => 'media',
		'person'        => 'people',
		'echo'          => 'echoes',
		'hero'          => 'heroes',
		'potato'        => 'potatoes',
		'veto'          => 'vetoes',
		'auto'          => 'autos',
		'memo'          => 'memos',
		'pimento'       => 'pimentos',
		'pro'           => 'pros',
		'knife'         => 'knives',
		'leaf'          => 'leaves',
		'bus'           => 'busses',
		'child'         => 'children',
		'quiz'          => 'quizzes',
		# words whose singular and plural forms are the same
		'equipment'     => 'equipment',
		'fish'          => 'fish',
		'information'   => 'information',
		'money'         => 'money',
		'moose'         => 'moose',
		'news'          => 'news',
		'rice'          => 'rice',
		'series'        => 'series',
		'sheep'         => 'sheep',
		'species'       => 'species'
	);

	return $irregulars;
}

/**
 * Pluralize string
 *
 * Check if string is irregular and, relevant to its ending,
 * create plural form.
 *
 * @param  string $string Singular that needs to be pluralized
 * @return string         Returns pluralized sting
 */
function house_pluralize( $string ) {

	$irregulars = irregular_plurals();

	$es = array( 's', 'z', 'ch', 'sh', 'x' );

	$last_letter = substr( $string, -1 );

	if ( array_key_exists( $string, $irregulars ) ) {

		return $irregulars[$string];

	} elseif ( $last_letter == 'y' ) {

		if ( substr( $string, -2 ) == 'ey' ) {

			return substr_replace( $string, 'ies', -2, 2 );

		} else {

			return substr_replace( $string, 'ies', -1, 1 );

		}

	} elseif ( in_array( substr( $string, -2 ), $es ) || in_array( substr( $string, -1 ), $es ) ) {

		return $string . 'es';

	} else {

		return $string . 's';

	}
}

/**
 * Get website link
 *
 * Get the url and build link with domain name (host) as link label. Used mostly
 * for displaying website's links (for partners, team members etc).
 *
 * @uses FILTER_VALIDATE_URL
 * @link http://php.net/manual/en/filter.filters.validate.php
 *
 * @param  string  $url   URL
 * @param  string  $class Class for the link
 * @param  boolean $blank Should link be opened in new tab or not
 * @return string         Returns either link markup or error message if url not valid
 */
function get_house_website_link( $url, $class = '', $blank = true ) {
	// check if url is valid
	if ( filter_var( $url, FILTER_VALIDATE_URL ) === false ) {
		return sprintf( __( '%s is not a valid URL', 'house' ), $url );
	} else {
		$parse = parse_url( $url );
		$website = $parse['host'];

		// check if there's a class
		if ( ! empty( $class ) ) {
			$class = ' class="' . $class . '"';
		} else {
			$class = '';
		}

		// open link in new tab or not?
		if ( $blank === true ) {
			$target = 'target="_blank"';
		} else {
			$target = '';
		}

		return '<a href="' . $url . '" ' . $target . $class . '>' . $website . '</a>';
	}
}
/**
 * Website link
 *
 * Echoes get_house_website_link().
 *
 * @param  string  $url   URL
 * @param  string  $class Class for the link
 * @param  boolean $blank Should link be opened in new tab or not
 * @return string         Echoes either link markup or error message if url not valid
 */
function house_website_link( $url, $class = '', $blank = true ) {
	echo get_house_website_link( $url, $class, $blank );
}

/**
 * Get latest sticky post
 */
function house_get_latest_sticky() {
	/**
	 * Get all sticky posts
	 */
	$sticky = get_option( 'sticky_posts' );

	/**
	 * Sort the stickies with the newest ones at the top
	 */
	rsort( $sticky );

	/**
	 * Get the 5 newest stickies (change 5 for a different number)
	 */
	$sticky = array_slice( $sticky, 0, 1 );

	/**
	  * Query sticky posts
	  */
	$loop = new WP_Query( array( 'post__in' => $sticky, 'ignore_sticky_posts' => 1 ) );
	/**
	 * The Loop
	 */
	if ( $loop->have_posts() ) {
		while ( $loop->have_posts() ) {
			$loop->the_post();
		?>
		<div class="grid-item grid-item-double">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
				<a href="<?php the_permalink(); ?>">
					<div class="wp-post-image img-to-bgr" style="background-image: url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full-featured' ); ?>');"></div><!-- /.wp-post-image -->
				</a>
					<h2 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
				</header><!-- /.event-item-image -->
				<footer class="entry-footer">
					<span class="posted-on">
						<time class="entry-date" datetime="2016-03-15T12:00:49+00:00"><?php the_date(); ?></time>
					</span>
				</footer>
				<div class="entry-content">
					<?php
						/**
					     * Get the blog page paragraph
					     */
						acf_field( 'paragraph', true, '<p>', '...</p>' );
					?>
					<p><?php echo content_trim_words( 20 ); ?></p>
					<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'house' ); ?></a>
				</div><!-- /.entry-content -->
			</article>
	  	</div><!-- /.grid-item -->
	<?php
		}
	}
	/**
	 * Restore original Post Data
	 */
	wp_reset_postdata();
}

/**
 * Get the title for events ctp
 */
function events_title( $max = 55, $title = '' ) {

	$title = get_the_title();

	if( strlen( $title ) > $max ) {

		return substr( $title, 0, $max ) . '...';

	} else {

		return $title;

	}

}
/**
 * Number results per page for search page
 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/pre_get_posts
 */
function house_search_posts_per_page( $query ) {
    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( is_search() ) {
        // Display 12 posts per page for search results
        $query->set( 'posts_per_page', 12 );
        return;
    }
}
add_action( 'pre_get_posts', 'house_search_posts_per_page' );

/**
 * Join posts and postmeta tables
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 * @link http://adambalee.com
 */
function house_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {
        $join .=' LEFT JOIN ' . $wpdb->postmeta . ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'house_search_join' );

/**
 * Modify the search query with posts_where
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function house_search_where( $where ) {
    global $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(" . $wpdb->posts . ".post_title LIKE $1) OR (" . $wpdb->postmeta . ".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'house_search_where' );

/**
 * Prevent duplicates
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function house_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'house_search_distinct' );

/**
 * Get the currency
 *
 * Get the currency value from customizer
 * @return string
 */
function house_get_the_currency() {
	/**
	 * Get the currency
	 */
	if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
		if ( get_theme_mod( 'currency' ) ) {
			$currencies = ['dollar' => '&#36;', 'euro' => '&euro;', 'pound' => '&pound;'];
			foreach ( $currencies  as $key => $value ) {
				if ( $key == get_theme_mod( 'currency' ) ) {
					$currency = $value;

					return $currency;
				}
			}
		} else {
			$currency = '';

			return $currency;
		}
	}
}

function house_restaurant_open_time( $hours, $minutes ) {
	$output = '';
	$time_h = get_theme_mod( $hours );
	$time_m = get_theme_mod( $minutes );

	if ( $time_h && $time_m ) :
		$time = $time_h . ':' . $time_m;
		$output = date( 'g:ia', strtotime( $time ) );
	endif;

	return $output;
}

function house_restaurant_open_times_text( $before = '', $after = '' ) {
	if ( get_theme_mod( 'reservation_opening_hours_text' ) ) :
		$workday_start = house_restaurant_open_time( 'working_day_open_hours', 'working_day_open_minutes' );
		$workday_end   = house_restaurant_open_time( 'working_day_close_hours', 'working_day_close_minutes' );
		$weekend_start = house_restaurant_open_time( 'weekend_day_open_hours', 'weekend_day_open_minutes' );
		$weekend_end   = house_restaurant_open_time( 'weekend_day_close_hours', 'weekend_day_close_minutes' );
		$placeholders = array( '[workday_start]', '[workday_end]', '[weekend_start]', '[weekend_end]' );
		$times = array( $workday_start, $workday_end, $weekend_start, $weekend_end );
		$text = get_theme_mod( 'reservation_opening_hours_text' );
		$output = str_replace( $placeholders, $times, $text );

		return $before . nl2br( htmlentities( $output ) ) . $after;
	endif;
}

/**
 * Change the Login Logo
 */
function house_login_logo() {
	if ( function_exists( 'the_custom_logo' ) ) {
		$custom_logo_id = get_theme_mod( 'custom_logo' ); ?>
	    <style type="text/css">
	        #login h1 a, .login h1 a {
	            background-image: url(<?php echo wp_get_attachment_image_src( $custom_logo_id , 'full' )[0]; ?>);
				height: 65px;
				width: 320px;
				background-size: contain;
				background-repeat: no-repeat;
	        	padding-bottom: 30px;
	        }
	    </style>
	<?php }
}
add_action( 'login_enqueue_scripts', 'house_login_logo' );

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyB3OL6KptBHL2dHZg48-vHJL9YLebE9_jY');
}
 
add_action('acf/init', 'my_acf_init');