<div class="reservation">
	<?php
		/**
		 * Get social icons
		 */
		get_template_part( 'partials/site/reservation', 'header' );
	?>
	<div class="container container--small">
		<?php
			/**
			 * Get reservation form
			 */
			if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) :

				/**
				 * Get the reservation book form Heading
				 */
				if ( get_theme_mod( 'reservation_book_form_heading' ) ) :
					echo '<h3>' . get_theme_mod( 'reservation_book_form_heading' ) . '</h3>';
				endif;

				if ( get_theme_mod( 'reservation_book_form' ) ) :

					/**
                     * Get contact form if CF7 plugin is active and we have contact form ID
                     */
                    if ( house_is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) :
						
						/**
						 * Get shortcode with ID from customizer
						 */
						echo do_shortcode( '[contact-form-7 id="' . get_theme_mod( 'reservation_book_form' ) . '"]' );
					endif;

				endif;

			endif;
		?>
	</div><!-- /.container container--small -->
</div><!-- /.reservation -->