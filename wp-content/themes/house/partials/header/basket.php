<div class="basket">
	<div class="basket-nav">
		<header><?php _e( 'Your Basket', 'house' ); ?> 
			<span class="products-quantity">(3)</span>
		</header>
		<button type="button" class="btn js-btn-close">
			
				  <?php echo house_svg_icon( 'close' ); ?>

		</button>
	</div><!-- /.basket-nav -->
	<ul class="products-list">
		<li class="product-item">
			<div>
				<button type="button" class="btn js-remove-product">
					
				  <?php echo house_svg_icon( 'close' ); ?>

				</button> 
				<span class="product-name">Simple Salad</span>
			</div> 
			<div>
				<button type="button" class="btn">
					
				  <?php echo house_svg_icon( 'minus' ); ?>

				</button> 
				<span class="product-number">1</span> 
				<button type="button" class="btn">
					
				  <?php echo house_svg_icon( 'plus' ); ?>

				</button>
				<span class="product-total">20</span>
			</div>
		</li>
	</ul><!-- /.product-list -->
	<div class="products-total">
		<span>Total</span>
		<span class="total-price">90</span>
	</div><!-- /.total -->
	<a href="javascript:;" class="btn btn--secondary btn--full">Order now</a>
</div><!-- /.basket -->