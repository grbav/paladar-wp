<?php
/**
 * Flexible sections
 *
 * Template part for rendering ACF flexible sections
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'home_content_fields';
/**
 * Define path to template parts
 * @var string
 */
$path = 'partials/flexible/section';
$specific = 'partials/flexible/home/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// hero slider
	'hero_slider' => [
		'dir'      => $specific,
		'template' => 'hero-slider',
	],

	// featured text
	'featured_text' => [
		'dir'      => $specific,
		'template' => 'featured-text',
	],

	// menu
	'menu' => [
		'dir'      => $specific,
		'template' => 'menu',
	],

	// reservations
	'reservations' => [
		'dir'      => $specific,
		'template' => 'reservations',
	],

	// today menu
	'today_menu' => [
		'dir'      => $specific,
		'template' => 'today-menu',
	],

	// opening times
	'opening_times' => [
		'dir'      => $specific,
		'template' => 'opening-times',
	],

	// blog posts
	'blog_posts' => [
		'dir'      => $specific,
		'template' => 'blog-posts',
	],

	// instagram photos
	'instagram_photos' => [
		'dir'      => $specific,
		'template' => 'instagram-photos',
	],

	// hr
	'hr' => [
		'dir'      => $path,
		'template' => 'hr',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )