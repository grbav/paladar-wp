<?php
/**
 * Flexible sections
 *
 * Template part for rendering ACF flexible sections
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'blog_post_content_fields';
/**
 * Define path to template parts
 * @var string
 */
$path = 'partials/flexible/section';
$specific = 'partials/flexible/blog-post/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// h3
	'heading_h3' => [
		'dir'      => $path,
		'template' => 'heading-h3',
	],

	// lead paragraph
	'lead' => [
		'dir'      => $path,
		'template' => 'lead',
	],

	// regular paragraph
	'paragraph' => [
		'dir'      => $path,
		'template' => 'paragraph',
	],

	// Content (multiple paragraphs)
	'content' => [
		'dir'      => $path,
		'template' => 'content',
	],

	// blockquote
	'blockquote' => [
		'dir'      => $path,
		'template' => 'blockquote',
	],

	// image - size full, all sizes ready
	'image' => [
		'dir'      => $path,
		'template' => 'image',
	],

	// slider
	'slider' => [
		'dir'      => $specific,
		'template' => 'slider',
	],
	
	// video
	'video' => [
		'dir'      => $specific,
		'template' => 'video',
	],

	// Recipe
	'recipe' => [
		'dir'      => $specific,
		'template' => 'recipe',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )