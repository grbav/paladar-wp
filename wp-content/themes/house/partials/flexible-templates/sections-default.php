<?php
/**
 * Default sections
 *
 * Template part for rendering ACF default sections
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define default field ID
 * @var string
 */
$flexible_field = 'default_content_fields';
/**
 * Define path to template parts
 * @var string
 */
$path = 'partials/flexible/section';
$blog = 'partials/flexible/blog-post/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// h2
	'heading_h2' => [
		'dir'      => $path,
		'template' => 'heading-h2',
	],

	// lead paragraph
	'lead' => [
		'dir'      => $path,
		'template' => 'lead',
	],

	// textarea with 'Automatically add paragraphs' formating
	'content' => [
		'dir'      => $path,
		'template' => 'content',
	],

	// blockquote
	'blockquote' => [
		'dir'      => $path,
		'template' => 'blockquote',
	],

	// image - size full, all sizes ready
	'image' => [
		'dir'      => $path,
		'template' => 'image',
	],

	// slider
	'slider' => [
		'dir'      => $blog,
		'template' => 'slider',
	],

	// video
	'video' => [
		'dir'      => $blog,
		'template' => 'video',
	],

	// hr
	'hr' => [
		'dir'      => $path,
		'template' => 'hr',
	],

];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )