<?php
/**
 * Flexible sections
 *
 * Template part for rendering ACF flexible sections
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Define flexible field ID
 * @var string
 */
$flexible_field = 'event_content_fields';
/**
 * Define path to template parts
 * @var string
 */
$path = 'partials/flexible/section';
/**
 * Define fields
 * @var array
 */
$templates = [

	// lead paragraph
	'lead' => [
		'dir'      => $path,
		'template' => 'lead',
	],

	// regular paragraph
	'paragraph' => [
		'dir'      => $path,
		'template' => 'paragraph',
	],
];

/**
 * Start the loop
 */
while ( the_flexible_field( $flexible_field ) ) :

	foreach ( $templates as $id => $t ) :

		if ( get_row_layout() == $id ) :

			get_template_part( $t['dir'], $t['template'] );

		endif; // get_row_layout()

	endforeach; // $templates as $id => $t

endwhile; // the_flexible_field( $flexible_field )