<?php
/**
 * Blockquote
 *
 * Template part for rendering ACF flexible sections - blockquote
 *
 * Used in flexible-templates/
 *         - sections-blog-post.php
 *
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( acf_sub_field( 'blockquote', false ) ) : ?>

	<blockquote>
		<?php
			acf_sub_field( 'blockquote', true, '<p>', '</p>' );
			acf_sub_field( 'cite', true, '<cite>', '</cite>' );
		?>
	</blockquote>

<?php endif;