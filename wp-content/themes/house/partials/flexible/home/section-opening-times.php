<?php
/**
 * Opening times
 *
 * Template part for rendering ACF flexible sections - opening times
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<div class="container">
	<div class="info-box">
		<div class="info-box__icon">

			<?php echo house_svg_icon( 'clock' ); ?>

		</div>
		<?php echo house_restaurant_open_times_text( '<p class="reservation-opening-hours">', '</p>' ); ?>
	</div>
	<div class="spacer-black"></div><!-- /.spacer-black -->
</div><!-- /.container -->