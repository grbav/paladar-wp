<?php
/**
 * Instagram photos
 *
 * Template part for rendering ACF flexible sections - instagram photos
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
$photos = house_get_instagram_media( acf_sub_field( 'username', false ), 6 ); 
	if ( $photos ) : ?>

	<section class="instagram-section">
		<div class="flex-row flex-row--small animation fade-in">

			<?php
				foreach ( $photos as $photo ) :
					$photo = $photo->imageHighResolutionUrl;
			?>

				<div class="col-2@md col-4@sm col-4">
					<div class="img-to-bgr" style="background-image: url('<?php echo $photo; ?>');">
					</div><!-- /.instagram -->
				</div><!-- /.col-2@md col-4@sm col-4 -->

			<?php endforeach; ?>

		</div><!-- /.flex-row -->
	</section><!-- /.instagram -->

<?php endif;