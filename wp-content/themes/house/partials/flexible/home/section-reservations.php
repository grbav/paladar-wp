<?php
/**
 * Reservations
 *
 * Template part for rendering ACF flexible sections - reservations
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<section class="reservation-section animation fade-top">
	<div class="reservation-section-top">
		<div class="container">
			<div class="flex-row--bottom">
				<div class="col-7">
					<div class="media media-white">
						<div class="media-counter"></div>
						<?php
							acf_sub_field( 'heading', true, '<h2 class="media-heading">', '</h2>' );
							acf_sub_field( 'paragraph', true, '<p class="media-text">', '</p>' );
						?>
					</div><!-- /.media.media-white -->
				</div><!-- /.col-7 -->
				<div class="col-5">

					<?php
						/**
						 * Get button url
						 */
						if ( acf_sub_field( 'button_url', false ) ) :

							$btn_url = acf_sub_field( 'button_url', false ); ?>

							<div class="media text-right">
								<?php
									/**
									 * Get button label
									 */
									if ( acf_sub_field( 'button_label', false ) ) :

										$btn_label = acf_sub_field( 'button_label', false );

									else :

										$btn_label = __( 'Findout more', 'house' );

									endif;

									echo '<a href="' . $btn_url . '" class="btn btn--secondary btn--white">' . $btn_label . '</a>';
								?>
								</div><!-- /.media -->

					<?php endif; ?>

				</div><!-- /.col-5 -->
				<div class="col-12">
					<div class="spacer-white"></div><!-- /.spacer-white -->
				</div><!-- /.col-12 -->
			</div><!-- /.flex-row--bottom -->
		</div><!-- /.container -->
	</div><!-- /.reservation-section-top -->

	<?php if ( get_sub_field( 'add_slider' ) == true && have_rows( 'slider' ) ) : ?>

		<div class="img-slider-wrap">
			<div class="owl-carousel img-slider">

				<?php 

					while ( have_rows( 'slider' ) ) : the_row();
						$slide = acf_sub_field( 'slide', false );

					echo '<div class="img-slider__item" style="background-image: url(' . $slide['url'] . ');"></div><!-- end of .img-slider__item -->';

				endwhile; ?>

			</div><!-- end of .img-slider -->
		</div><!-- /.img-slider-wrap -->

	<?php endif; ?>

</section><!-- /.reservation-section -->