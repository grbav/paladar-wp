<?php
/**
 * Slider
 *
 * Template part for rendering ACF flexible sections - slider
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'home_slider' ) ) : ?>

	<section class="media-slider-section animation fade-top">
		<div class="media-slider-wrap">
			<div class="owl-carousel media-slider">

			<?php 
				while ( have_rows( 'home_slider' ) ) : the_row();

					if ( acf_sub_field( 'image', false ) ) : 
						$image = acf_sub_field( 'image', false ); ?>
						
						<div class="owl-item-wrap">
							<div class="media-slider__item" style="background-image: url('<?php echo $image['sizes']['large']; ?>');"></div>
							<div class="media-slider__content fade-in-top">
								<div class="media-slider-count"></div>
								<?php
									/**
									 * Get heading
									 */
									acf_sub_field( 'heading', true, '<h2 class="media-slider-heading">', '</h2>' );

									/**
									 * Get button label
									 */
									if ( acf_sub_field( 'button_label', false ) ) :

										$btn_label = acf_sub_field( 'button_label', false );

									else :

										$btn_label = __( 'Findout more', 'house' );

									endif;

									/**
									 * Get button url
									 */
									$btn_url = acf_sub_field( 'button_url', false );

									echo '<a href="' . $btn_url . '" class="btn btn--secondary">' . $btn_label . '</a>';
								?>
							</div><!-- /.media-slider__content -->
						</div><!-- /.owl-item-wrap -->

					<?php endif; 

				endwhile; ?>

			</div>
			<a href="#next" class="scroll-to-next"><?php _e( 'scroll down', 'house' ); ?></a>
		</div><!-- /.media-slider-wrap -->
	</section><!-- /.media-slider-section -->

<?php endif; ?>