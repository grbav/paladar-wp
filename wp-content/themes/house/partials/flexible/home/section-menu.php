<?php
/**
 * Menu
 *
 * Template part for rendering ACF flexible sections - menu
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<section class="menu-section animation fade-top">
	<div class="container">
		<div class="flex-row--bottom">
			<div class="col-4">
				<div class="media">
					<div class="media-counter"></div>
					<?php
						acf_sub_field( 'heading', true, '<h2 class="media-heading">', '</h2>' );
						acf_sub_field( 'paragraph', true, '<p class="media-text">', '</p>' );

						/**
						 * Get button label
						 */
						if ( acf_sub_field( 'button_label', false ) ) :

							$btn_label = acf_sub_field( 'button_label', false );

						else :

							$btn_label = __( 'Findout more', 'house' );

						endif;

						/**
						 * Get button url
						 */
						$btn_url = acf_sub_field( 'button_url', false );

						echo '<a href="' . $btn_url . '" class="btn btn--primary">' . $btn_label . '</a>';
					?>
				</div>
			</div>
			<div class="col-8">
				<div class="media">
					<?php
						$image = acf_sub_field( 'image', false );
						if ( $image ) :

							echo '<div class="media-img" style="background-image: url(' . $image['url'] . ');">';

						endif;
					?>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container -->
</section><!-- /.menu-section -->