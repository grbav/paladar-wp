<?php
/**
 * Hero Slider
 *
 * Template part for rendering ACF flexible sections - hero slider
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'hero_slider' ) ) : ?>

	<section class="hero-slider media-slider-section animation fade-top">
		<div class="img-slider-wrap">
			<div class="owl-carousel img-slider">

				<?php 

					while ( have_rows( 'hero_slider' ) ) : the_row();
						$image = acf_sub_field( 'image', false );

					echo '<div class="img-slider__item" style="background-image: url(' . $image['url'] . ');"></div><!-- end of .img-slider__item -->';

				endwhile; ?>

			</div><!-- end of .img-slider -->
		</div><!-- /.img-slider-wrap -->
	</section><!-- /.media-slider-section -->

<?php endif; ?>