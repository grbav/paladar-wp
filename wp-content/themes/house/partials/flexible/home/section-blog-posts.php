<?php
/**
 * Buzz
 *
 * Template part for rendering ACF flexible sections - buzz
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
$query = house_get_blog_posts();

if ( $query->have_posts() ) : ?>

	<section class="buzz-section">
		<div class="container">
			<div class="media media-center">
				<div class="media-counter"></div>
				<h2 class="media-heading"><?php _e( 'buzz', 'house' ); ?></h2>
			</div><!-- /.media.media-white -->
			<div class="grid grid-blog animation fade-top clearfix">

				<?php while ( $query->have_posts() ) : $query->the_post();

				  	get_template_part( 'content', 'buzz' );

				endwhile; ?>

			</div><!-- /.grid -->
		<?php
			/**
			 * Get 'posts_per_page' setting
			 * @var int
			 */
			$posts_per_page = $query->query['posts_per_page'];
			/**
			 * Get total number of posts found by query
			 * @var int
			 */
			$found_posts = $query->found_posts;
			/**
			 * Show load more button only if
			 * number of found posts if larger than 'posts_per_page'.
			 * We don't want button that does nothin'.
			 */
			if ( $found_posts > $posts_per_page ) : ?>

			<div class="text-center btn-load-more">
				<a href="javascript:;" class="btn btn--primary" id="more_posts"><?php _e( 'Load more', 'house' ); ?></a>
			</div>
			<?php endif; //$found_posts > $posts_per_page
		?>
		</div><!-- /.container -->
	</section><!-- /.buzz-section -->

<?php endif; wp_reset_query();
