<?php
/**
 * Today Menu
 *
 * Template part for rendering ACF flexible sections - today menu
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

/**
 * If featured image exists
 * display section
 */
if ( acf_sub_field( 'featured_image', false ) ) :
	/**
	 * Get featured image
	 */
	$image = acf_sub_field( 'featured_image', false );
?>

	<section class="today-on-menu-section animation fade-top">
		<div class="container">
			<div class="flex-row--bottom">
				<div class="col-7">
					<div class="media">
						<div class="media-img" style="background-image: url('<?php echo $image['url']; ?>');">
						</div>
					</div>
				</div>

				<?php 
					/**
					 * Get page ID by page template
					 */
					$args = [
						'posts_per_page' => 1,
					    'post_type' => 'page',
					    'fields' => 'ids',
					    'meta_key'       => '_wp_page_template',
					    'meta_value' => 'templates/template-menu.php'
					];
					$pages = get_posts( $args );
					$page_ID = $pages[0];

					/**
					 * If today menu exists on menu page
					 * display menu
					 */
					if ( have_rows( 'today', $page_ID ) ) :
				?>

				<div class="col-5">
					<div class="media">
						<div class="media-counter"></div>
						<h2 class="media-heading"><?php _e( 'today', 'house' ); ?></h2>
						<div class="media-list media-list--bottom">
								
							<?php 
								while ( have_rows( 'today', $page_ID ) ) : the_row();
									if ( have_rows( 'meals' ) ) : while ( have_rows( 'meals' ) ) : the_row();
									$post_ID = acf_sub_field( 'meal', false );
								?>
							
								<div class="media-list__item">
									<span class="media-item__part">
										<dl>
											<dt><?php echo get_the_title( $post_ID ); ?></dt>
											<?php
												/**
												 * Get ingredients
												 */
												$ing_today = get_field( 'ingredients', $post_ID );
												$ing_today = array_slice( $ing_today, 0, 1 );
												echo '<dd>' . $ing_today[0]['ingredient'] . '</dd>';
											?>
										</dl>
									</span>
									<?php
										/**
										 * Get the currency
										 */
										$currency = house_get_the_currency();
										/**
										 * Get price
										 */
										acf_field( 'price', true, '<span class="media-item__part">' . $currency, '</span>', $post_ID );
									?>
								</div>

							<?php endwhile; endif; endwhile; ?>
						
						</div><!-- .media-list -->
					</div>
				</div>

				<?php endif; ?>

			</div>
		</div><!-- /.container -->
	</section><!-- /.today-on-menu-section -->

<?php endif;