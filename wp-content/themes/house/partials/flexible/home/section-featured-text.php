<?php
/**
 * Featured text
 *
 * Template part for rendering ACF flexible sections - featured text
 *
 * Used in flexible-templates/
 *         - sections-home.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>
<section class="animation fade-top">
	<div class="info-box">
		<div class="info-box__icon">

			<?php echo house_svg_icon( 'paladar' ); ?>
			
		</div>
		
		<?php acf_sub_field( 'paragraph', true, '<p>', '</p>' ); ?>

	</div><!-- /.info-box -->
</section><!-- /.info-box -->
