<?php
/**
 * Image
 *
 * Template part for rendering ACF flexible sections - image
 *
 * Used in flexible-templates/
 *         - sections-blog-post.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

/**
 * Translators: field id, echo (false returns value), before, after
 */
$image = acf_sub_field( 'image', false );

if ( $image ) :
	/**
	 * Prepare sizes
	 * @var string
	 */
	$thumbnail = $image['sizes']['thumbnail'];
	$medium    = $image['sizes']['medium'];
	$large     = $image['sizes']['large'];
	$full      = $image['url'];
?>
<figure>
	<img src="<?php echo $full; ?>" >
	<?php acf_sub_field( 'images_caption', true, '<figcaption>', '</figcaption>' ); ?>
</figure>

<?php endif; // $image