<?php
/**
 * Slider
 *
 * Template part for rendering ACF flexible sections - slider
 *
 * Used in flexible-templates/
 *         - sections-blog-post.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( have_rows( 'slides' ) ) : ?>

	<div class="img-slider-with-caption-wrap">
		<div class="owl-carousel img-slider-caption">

			<?php 
				while ( have_rows( 'slides' ) ) : the_row();
				
					/**
					 * Translators: field id, echo (false returns value), before, after
					 */
					$image = acf_sub_field( 'image', false );

					if ( $image ) :
						/**
						 * Prepare sizes
						 * @var string
						 */
						$thumbnail = $image['sizes']['thumbnail'];
						$medium    = $image['sizes']['medium'];
						$large     = $image['sizes']['large'];
						$full      = $image['url'];
			?>

				<div class="img-slider__item" style="background-image: url('<?php echo $image['url']; ?>');">
					<?php
						/**
						 * Get the image's title
						 */
						acf_sub_field( 'image_title', true, '<div class="picture-subtitle">', '</div>' );
					?>
				</div>

			<?php 
					endif;
				endwhile; ?>

		</div>
		<div class="subtitle-text"></div>
	</div>

<?php endif;