<?php
/**
 * Video
 *
 * Template part for rendering ACF flexible sections - video
 *
 * Used in flexible-templates/
 *         - sections-blog-post.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

/**
 * No point in starting anything without video url
 */
if ( ! get_sub_field( 'video_url' ) ) {
	return;
}

/**
 * Video url - text field
 */

$video = acf_sub_field( 'video_url', false );
/**
 * Get video id
 * @var string
 */

$video_id = set_video_id( $video );

if ( ! empty( acf_sub_field( 'video_cover', false ) ) ) :
	/**
	 * Cover image src - field returns url to full size
	 */
	$cover = acf_sub_field( 'video_cover', false );
	$cover = $cover['url'];
else :
	$cover = set_video_image_src( $video );
endif;

/**
 * Button label - text field
 */
if ( get_sub_field( 'button_label' ) ) {
	$label = acf_sub_field( 'button_label', false );
} else {
	$label = __( 'Play video', 'house' );
}

if ( ! empty( acf_sub_field( 'video_url', false ) ) ) : ?>

	<div class="video-wrap" data-url="<?php echo $video; ?>" data-id="<?php echo $video_id; ?>" style="background-image:url('<?php echo $cover; ?>');" >
		<button class="btn btn-play"></button>
	</div><!-- / .video-wrap -->

<?php endif;