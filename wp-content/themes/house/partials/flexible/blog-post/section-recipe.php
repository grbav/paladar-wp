<?php
/**
 * Recipe
 *
 * Template part for rendering ACF flexible sections - recipe
 *
 * Used in flexible-templates/
 *         - sections-blog-post.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */

/**
 * Get object from post
 */
$recipe = acf_sub_field( 'recipe', false );
/**
 * Get ID from object
 */
$post_ID = $recipe->ID; ?>

<div class="recipe">

	<?php if ( get_field( 'time_for_prepare', $post_ID ) || get_field( 'time_for_prepare', $post_ID ) || get_field( 'serves', $post_ID ) ) : ?>

		<div class="recipe-info">

			<?php
				/**
				 * If time exists
				 */
				if ( get_field( 'time_for_prepare', $post_ID ) ) : ?>
					<span><?php _e( 'Prep', 'house' ); ?></span>
					<?php acf_field( 'time_for_prepare', true, '<span>', '</span>', $post_ID );
				endif; 

				/**
				 * Check if time or serves exists
				 */
				if ( get_field( 'time_for_prepare', $post_ID ) && get_field( 'serves', $post_ID ) ) :
					echo '<span> | </span>'; 
				endif;

				/**
				 * If serves exists
				 */
				if ( get_field( 'time_for_prepare', $post_ID ) ) : ?>
					<span><?php _e( 'Serves', 'house' ); ?></span> 
				  	<?php acf_field( 'serves', true, '<span>', '</span>', $post_ID );	
				endif; ?> 

		</div><!-- /.recipe-info -->

	<?php endif; ?>

	<h2 class="recipe-name"><?php echo get_the_title( $post_ID ); ?></h2><!-- /.recipe-heading -->
	<!-- Image slider with caption -->

	<?php
		/**
		 * Get images array from gallery
		 */
		$images = get_field( 'images', $post_ID );

		/**
		 * Check does gallery exists
		 */
		if ( $images ) : 
	?>

		<div class="img-slider-with-caption-wrap">
			<div class="owl-carousel img-slider-caption">

				<?php foreach( $images as $image ): ?>

					<div class="img-slider__item" style="background-image: url('<?php echo $image['url']; ?>');">
						<div class="picture-subtitle"><?php echo $image['title']; ?></div>
					</div>

				<?php endforeach; ?>

			</div>
			<div class="subtitle-text"></div>
		</div>

	<?php endif;

	/**
	 * Check does method or ingredients extists
	 */
	if ( get_field( 'method', $post_ID ) || get_field( 'ingredients', $post_ID ) ) : ?>

		<div class="flex-row">
		
			<?php if ( have_rows( 'method', $post_ID ) ) : ?>

				<div class="col-7">
					<div class="media">
						<h4 class="media-header"><?php _e( 'method', 'house' ); ?></h4>
						<ol class="num-list">

							<?php while ( have_rows( 'method', $post_ID ) ) : the_row();

								acf_sub_field( 'step', true, '<li>', '</li>', $post_ID );

							endwhile; ?>

						</ol>
					</div>
				</div>

			<?php endif;

			if ( have_rows( 'ingredients', $post_ID ) ) : ?>

			<div class="col-5">
				<div class="media">
					<h4 class="media-header">ingredients</h4>
					<div class="media-list media-list--center">

						<?php while ( have_rows( 'ingredients', $post_ID ) ) : the_row(); ?>

							<div class="media-list__item">

								<?php 
									acf_sub_field( 'ingredient', true, '<span class="media-item__part">', '</span>', $post_ID );
									acf_sub_field( 'quantity', true, '<span class="media-item__part">', '</span>', $post_ID );
								?>
								
							</div>

						<?php endwhile; ?>

					</div><!-- .media-list -->
				</div>
			</div>

			<?php endif; ?>

		</div>

	<?php endif; ?>

</div><!-- /.recipe -->