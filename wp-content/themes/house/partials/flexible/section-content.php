<?php
/**
 * Content
 *
 * Template part for rendering ACF flexible sections - content
 *
 * Idea is to use this for 'textarea' with 'Automatically add paragraphs'
 *
 * Used in flexible-templates/
 *         - sections.php
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
/**
 * Translators: field id, echo (false returns value), before, after
 */
acf_sub_field( 'content' );