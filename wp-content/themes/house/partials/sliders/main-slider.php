<?php
/**
 * Main slider
 */

if ( have_rows( 'main_slider' ) ) : ?>

	<div class="media-slider-wrap">
		<div class="owl-carousel media-slider">

			<?php while ( have_rows( 'main_slider' ) ) : the_row(); 
				$meal_ID = get_sub_field( 'meal' );
			?>

				<div class="owl-item-wrap">
					<div class="media-slider__item" style="background-image: url('<?php echo get_the_post_thumbnail_url( $meal_ID ); ?>');"></div>
					<div class="media-slider__content fade-in-top">
						<div class="media-slider-count"></div>
						<?php 
							/**
							 * Get the type of meal
							 */
							acf_sub_field( 'type_of_meal', true, '<span class="meal-category">', '</span>' ); ?>
							<h2 class="media-slider-heading"><?php echo get_the_title( $meal_ID ); ?></h2>
							<?php
								/**
								 * Get the currency
								 */
								$currency = house_get_the_currency();
								/**
								 * Get the price
								 */
								if ( get_field( 'price', $meal_ID ) ) :
									echo '<span class="price">' . $currency . get_field( 'price', $meal_ID ) . '</span>';
								endif;
							?>
					</div>
				</div><!-- /.owl-item-wrap -->

			<?php endwhile; ?>

		</div><!-- /.media-slider -->
		<a href="#next" class="scroll-to-next"><?php _e( 'scroll down', 'house' ); ?></a>
	</div><!-- /.media-slider-wrap -->

<?php endif;