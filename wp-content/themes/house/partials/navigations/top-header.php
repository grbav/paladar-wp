<?php
/**
 * Top header template part
 *
 * Template part for rendering top header.
 *
 * @package WordPress
 */

/**
 * Check if the site is being previewed in the Customizer
 */
if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) :
	/**
	 * Check if top menu exists in customizer
	 */
	if ( get_theme_mod( 'top_menu' ) ) :
		/**
		 * If top menu is "yes",
		 * display top menu
		 */
		if ( get_theme_mod( 'top_menu' ) == 'yes' ) : ?>

			<div id="top-bar" class="top-bar">
			    <div class="top-bar-left">
			        <div class="social-nav">
			            
						<?php
							/**
							 * Get all populated social links from customizer
							 *
							 * There are other helper functions available, e.g. get specific
							 * profile link or just url etc. @see inc/branding/social.php
							 */
							if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
								all_social_profiles();
							}

							/**
							 * Get the custom social profiles
							 */
							for ( $i = 1; $i <= 3; $i++ ) {
								if ( get_theme_mod( 'social_profiles_other_' . $i ) && get_theme_mod( 'social_profiles_other_' . $i . '_label' ) ) :

									$label = get_theme_mod( 'social_profiles_other_' . $i . '_label' );
									$url = get_theme_mod( 'social_profiles_other_' . $i );
								
									echo '<a href="' . $url . '" target="_blank">' . $label . '</a>';

								endif;
							} ?>

			        </div>
			        <!-- /.social-nav -->

					<?php 
		        		if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) :

							if ( get_theme_mod( 'topbar_phone_number' ) || get_theme_mod( 'topbar_email' ) ) : ?>

						        <div class="email-phone">
						        	<?php
						        		/**
						        		 * Get the phone number
						        		 */
						        		$phone = '<a href="tel:' . get_theme_mod( 'topbar_phone_number' ) . '">' . get_theme_mod( 'topbar_phone_number' ) . '</a>';
					        			echo '<span id="top-bar-phone">' . $phone . '</span>';
					        			/**
						        		 * Get the email
						        		 */
					        			$email = '<a href="mailto:' . get_theme_mod( 'topbar_email' ) . '">' . get_theme_mod( 'topbar_email' ) . '</a>';
					        			echo '<span id="top-bar-email">' . $email . '</span>';
						        	?>
						        </div>
						        <!-- /.email-phone -->

							<?php endif;

						endif; ?>

			    </div>
			    <!-- /.top-bar-left -->
			    <div class="top-bar-right">

					<?php if ( has_nav_menu( 'top-bar' ) ) : ?>

				    	<nav id="site-navigation" class="main-navigation nav-main" role="navigation">

							<?php wp_nav_menu( array( 'theme_location' => 'top-bar', 'container' => false ) ); ?>

						</nav><!-- #site-navigation -->

					<?php endif;

		        	/**
		        	 * Check if the site is being previewed in the Customizer
		        	 */
		        	if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) :
		        		/**
		        		 * Check if top bar button label and button url exist 
		        		 */
		        		if ( get_theme_mod( 'topbar_btn_label' ) && get_theme_mod( 'topbar_btn_url' ) ) :
			        		/**
			        		 * Get the top bar button
			        		 */
		        			$btn = '<a href="' . get_theme_mod( 'topbar_btn_url' ) . '" target="_blank">' . get_theme_mod( 'topbar_btn_label' ) . '</a>';
		        			echo '<span id="top-bar-btn">' . $btn . '</span>';
		        		endif;
		        	endif; ?>

			    </div>
			    <!-- /.top-bar-right -->
			</div>
			<!-- /.top-bar -->
		
		<?php endif;
	endif;
endif;