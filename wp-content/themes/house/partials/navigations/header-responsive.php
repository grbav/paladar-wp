<?php
/**
 * Main navigation template part
 *
 * Template part for rendering main navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<nav id="responsive-navigation" class="responsive-navigation">
	<ul class="menu">
		<?php
			/**
			 * Get header primary navigation
			 */
			wp_nav_menu( array( 'theme_location' => 'header-primary', 'class' => 'menu', 'container' => '', 'items_wrap' => '%3$s' ) );
			/**
			 * Get header secondary navigation
			 */
			wp_nav_menu( array( 'theme_location' => 'header-secondary', 'class' => 'menu', 'container' => '', 'items_wrap' => '%3$s' ) );
		?>
	</ul>
</nav>