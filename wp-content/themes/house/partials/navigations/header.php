<?php
/**
 * Main navigation template part
 *
 * Template part for rendering main navigation.
 *
 * @link https://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 * @package WordPress
 */
?>
<button type="button" class="btn js-btn-burger"><span class="burger"></span></button>
<nav id="site-navigation" class="main-navigation primary-navigation" role="navigation">
	<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'house' ); ?>"><?php _e( 'Skip to content', 'house' ); ?></a>
	
	<?php 
		wp_nav_menu( array( 'theme_location' => 'header-primary', 'class' => 'menu', 'container' => '' ) );
		
		/**
		 * Get logo
		 */
		get_template_part( 'partials/site/global', 'branding' );

		wp_nav_menu( array( 'theme_location' => 'header-secondary', 'class' => 'menu', 'container' => '' ) );
	?>

</nav><!-- #site-navigation -->