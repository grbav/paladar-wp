<?php
/**
 * Events filter
 *
 * Template part for rendering events filter
 *
 * @package WordPress
 */
/**
 * Check if we have at least one event
 */
if ( house_count_all_events() > 0 ) : ?>
	<div class="filter">
		<nav id="filters" class="filter-wrap">
			<div class="filter-responsive-element">
				<span class="filter-responsive-element-holder"><?php _e( 'All events', 'house' ); ?></span>
				<div class="filter-responsive-element-burger"><span></span></div>
			</div><!-- filter-responsive-element -->

			<ul class="filter-nav">
				<li>
					<button class="filter-button" data-filter="*">
						<span id="events-all" class="filter-name"><?php _e( 'All events', 'house' ); ?></span>
						<span class="filter-count"><?php echo house_count_all_events(); ?></span>
					</button>
				</li>

				<?php
					wp_reset_query();
					/**
					 * Check if we have at least one future event
					 */
					if ( house_count_future_events() > 0 ) : ?>
						<li>
							<button class="filter-button" data-filter=".coming">
								<span id="events-future" class="filter-name"><?php _e( 'Coming soon', 'house' ); ?></span>
								<span class="filter-count"><?php echo house_count_future_events(); ?></span>
							</button>
						</li>
					<?php endif; // house_count_future_events() > 0

					wp_reset_query();
					/**
					 * Check if we have at least one past event
					 */
					if ( house_count_past_events() > 0 ) : ?>
						<li>
							<button class="filter-button" data-filter=".past">
								<span id="events-past" class="filter-name"><?php _e( 'Past events', 'house' ); ?></span>
								<span class="filter-count"><?php echo house_count_past_events(); ?></span>
							</button>
						</li>
					<?php endif; // house_count_past_events() > 0
				?>
			</ul><!-- filter-nav -->
		</nav><!-- filter-wrap -->
	</div><!-- filter -->
<?php endif; // house_count_all_events() > 0
wp_reset_query();