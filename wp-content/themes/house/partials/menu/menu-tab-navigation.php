<?php
/**
 * Tab navigation
 */

/**
 * Today
 */
if ( have_rows( 'today' ) ) : 
	echo '<li><a href="#tab-0">' . __( 'Today', 'house' ) . '</a></li>';
endif;

/**
 * Get other menus
 */
if ( have_rows( 'tabs' ) ) : while ( have_rows( 'tabs' ) ) : the_row();

	$tab_item = acf_sub_field( 'add_food_or_drink', false );
	/**
	 * Get the tab's name
	 */
	$tab_label = $tab_item->name;
	/**
	 * Get the tab's slug
	 */
	$tab_slug = $tab_item->slug;

	echo '<li><a href="#tab-' . $tab_slug . '">' . $tab_label . '</a></li>';

endwhile; endif;