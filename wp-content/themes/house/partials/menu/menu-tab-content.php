<?php
/**
 * Tab content
 */

/**
 * Get content for today menu
 */
get_template_part( 'partials/menu/today-menu' );

/**
 * Get the taxonomy name
 */
$taxonomy = 'Meal';

if ( have_rows( 'tabs' ) ) : while ( have_rows( 'tabs' ) ) : the_row(); 

	$tab_item = acf_sub_field( 'add_food_or_drink', false );
	/**
	 * Get the sub tab's id
	 */
	$tab_id = $tab_item->term_taxonomy_id;
	/**
	 * Get the tab's slug
	 */
	$tab_slug = $tab_item->slug; ?>

	<div id="tab-<?php echo $tab_slug; ?>" class="tabs__item">

		<?php
			if ( acf_sub_field( 'add_sub_categories', false ) === true ) :

		 		if ( have_rows( 'add_subcategories' ) ) : while ( have_rows( 'add_subcategories' ) ) : the_row(); ?>

					<div class="order-menu">

						<?php
							/**
							  * Get the sub category object
							  */ 
							$sub_category = acf_sub_field( 'add_sub_category', false );
							/**
							 * Get the sub category id
							 */
							$sub_id = $sub_category->term_taxonomy_id;
							/**
							 * Get the sub category name
							 */
							$sub_name = $sub_category->name;
						?>
						<h2 class="order-menu-heading"><?php echo $sub_name; ?></h2><!-- /.order-menu-heading -->

						<?php
							/**
							 * WP query by term ID
							 */
							$args = array(
								'post_type' => 'menu',
								'posts_per_page' => -1,
								'tax_query' => array(
								    array(
								    	'taxonomy' => $taxonomy,
								    	'field' => 'id',
								    	'terms' => $sub_id
								    )
								) 
							);
							$query = new WP_Query( $args );

							if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
						?>

						<div class="order-menu-item-wrap">
							<div class="order-menu-item">
								<div class="item-info">
									<?php
										/**
										 * Get post thumbnail if exists
										 */
										if ( has_post_thumbnail() ) :

											echo '<div class="item-image" style="background-image: url(' . get_the_post_thumbnail_url() . ');">';
										/**
										 * Get the currency
										 */
										$currency = house_get_the_currency();
										/**
										 * Get price
										 */
										acf_field( 'price', true, '<span class="item-price">' . $currency, '</span>', get_the_ID() );

											echo '</div><!-- /.item-image -->';

										endif;
									?>
									<div class="item-desc">
										<?php 
											the_title( '<h5>', '</h5>' );

											/**
											 * Get ingredients
											 */
											$ing = get_field( 'ingredients', get_the_ID() );
											$show = array_slice( $ing, 0, 4 );
											$hidden = array_slice( $ing, 4);
											$counter = count($hidden);

											foreach ( $show as $s ) :

												echo '<span>' . $s['ingredient'] . '</span>, ';
											
											endforeach;

											if ( count($ing) == 5 ) :
												echo '<span>+' . $counter . ' ' . __( 'suplement', 'house' ) . '</span>';
											elseif ( count($ing) > 4 ) :
												echo '<span>+' . $counter . ' ' . __( 'suplements', 'house' ) . '</span>';
											endif;
										?>
									</div><!-- /.item-desc -->
								</div><!-- /.item-info -->
							</div><!-- /.order-menu-item -->
							<div class="item-image-full"> <!-- On click show this element -->
								<?php
									/**
									 * Get post thumbnail if exists
									 */
									if ( has_post_thumbnail() ) :

										echo '<img src="' . get_the_post_thumbnail_url() . '" alt="" />';

									endif;
									/**
									 * Get the currency
									 */
									$currency = house_get_the_currency();
									/**
									 * Get price
									 */
									acf_field( 'price', true, '<span class="item-price item-price-black">' . $currency, '</span>', get_the_ID() );
								?>
							</div><!-- /.item-image-full -->
						</div><!-- /.order-menu-item-wrap -->

						<?php endwhile; endif; wp_reset_query(); ?>

					</div><!-- /.order-menu -->

				<?php endwhile; endif; 

			else : ?>

				<!-- List meals in parent menu -->
				
				<?php
					/**
					 * WP query by term ID
					 */
					$args = array(
						'post_type' => 'menu',
						'posts_per_page' => -1,
						'tax_query' => array(
						    array(
						    	'taxonomy' => $taxonomy,
						    	'field' => 'id',
						    	'terms' => $tab_id,
						    	'include_children' => false
						    )
						) 
					);
					$query = new WP_Query( $args );

					if ( $query->have_posts() ) :
				?>

					<div class="order-menu">

						<?php while ( $query->have_posts() ) : $query->the_post(); ?>

						<div class="order-menu-item-wrap">
							<div class="order-menu-item">
								<div class="item-info">
									<?php
										/**
										 * Get post thumbnail if exists
										 */
										if ( has_post_thumbnail() ) :

											echo '<div class="item-image" style="background-image: url(' . get_the_post_thumbnail_url() . ');">';
										/**
										 * Get the currency
										 */
										$currency = house_get_the_currency();
										/**
										 * Get price
										 */
										acf_field( 'price', true, '<span class="item-price">' . $currency, '</span>', get_the_ID() );

											echo '</div><!-- /.item-image -->';

										endif;
									?>
									<div class="item-desc">
										<?php 
											the_title( '<h5>', '</h5>' );

											/**
											 * Get ingredients
											 */
											$ing = get_field( 'ingredients', get_the_ID() );
											$show = array_slice( $ing, 0, 4 );
											$hidden = array_slice( $ing, 4);
											$counter = count($hidden);

											foreach ( $show as $s ) :

												echo '<span>' . $s['ingredient'] . '</span>, ';
											
											endforeach;

											if ( count($ing) == 5 ) :
												echo '<span>+' . $counter . ' ' . __( 'suplement', 'house' ) . '</span>';
											elseif ( count($ing) > 4 ) :
												echo '<span>+' . $counter . ' ' . __( 'suplements', 'house' ) . '</span>';
											endif;
										?>
									</div><!-- /.item-desc -->
								</div><!-- /.item-info -->
							</div><!-- /.order-menu-item -->
							<div class="item-image-full"> <!-- On click show this element -->
								<?php
									/**
									 * Get post thumbnail if exists
									 */
									if ( has_post_thumbnail() ) :

										echo '<img src="' . get_the_post_thumbnail_url() . '" alt="" />';

									endif;
									/**
									 * Get the currency
									 */
									$currency = house_get_the_currency();
									/**
									 * Get price
									 */
									acf_field( 'price', true, '<span class="item-price item-price-black">' . $currency, '</span>', get_the_ID() );
								?>
							</div><!-- /.item-image-full -->
						</div><!-- /.order-menu-item-wrap -->

						<?php endwhile; ?>

					</div><!-- /.order-menu -->

				<?php endif; wp_reset_query();

				endif; ?>

			</div>

<?php endwhile; endif;