<?php
/**
 * Today content
 */

if ( have_rows( 'today' ) ) : ?>

	<div id="tab-0" class="tabs__item">

		<?php while ( have_rows( 'today' ) ) : the_row(); ?>

			<div class="order-menu">
				<?php acf_sub_field( 'meal_type', true, '<h2 class="order-menu-heading">', '</h2>' ); ?>

				<?php 
					if ( have_rows( 'meals' ) ) : while ( have_rows( 'meals' ) ) : the_row();
						$post_ID = acf_sub_field( 'meal', false );
				?>

					<div class="order-menu-item-wrap">
						<div class="order-menu-item">
							<div class="item-info">
								<?php
									/**
									 * Get post thumbnail if exists
									 */
									if ( has_post_thumbnail( $post_ID ) ) :

										echo '<div class="item-image" style="background-image: url(' . get_the_post_thumbnail_url( $post_ID ) . ');">';
									/**
									 * Get the currency
									 */
									$currency = house_get_the_currency();
									/**
									 * Get price
									 */
									acf_field( 'price', true, '<span class="item-price">' . $currency, '</span>', $post_ID );

										echo '</div><!-- /.item-image -->';

									endif;
								?>
								<div class="item-desc">
									<h5><?php echo get_the_title( $post_ID ); ?></h5>

									<?php
										/**
										 * Get ingredients
										 */
										$ing_today = get_field( 'ingredients', $post_ID );
										$show_today = array_slice( $ing_today, 0, 4 );
										$hidden_today = array_slice( $ing_today, 4);
										$count_today = count($hidden_today);

										foreach ( $show_today as $s ) :

											echo '<span>' . $s['ingredient'] . '</span>, ';
										
										endforeach;

										if ( count($ing_today) == 5 ) :
											echo '<span>+' . $count_today . ' ' . __( 'suplement', 'house' ) . '</span>';
										elseif ( count($ing_today) > 4 ) :
											echo '<span>+' . $count_today . ' ' . __( 'suplements', 'house' ) . '</span>';
										endif;
									?>

								</div><!-- /.item-desc -->
							</div><!-- /.item-info -->
						</div><!-- /.order-menu-item -->
						<div class="item-image-full"> <!-- On click show this element -->
							<?php
								/**
								 * Get post thumbnail if exists
								 */
								if ( has_post_thumbnail( $post_ID ) ) :

									echo '<img src="' . get_the_post_thumbnail_url( $post_ID ) . '" alt="" />';

								endif;
								/**
								 * Get the currency
								 */
								$currency = house_get_the_currency();
								/**
								 * Get price
								 */
								acf_field( 'price', true, '<span class="item-price item-price-black">' . $currency, '</span>', $post_ID );
							?>
						</div><!-- /.item-image-full -->
					</div><!-- /.order-menu-item-wrap -->

				<?php endwhile; endif; ?>

			</div><!-- /.order-menu -->

		<?php endwhile; ?>

	</div>

<?php endif;