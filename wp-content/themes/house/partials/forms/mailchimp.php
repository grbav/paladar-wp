<?php
/**
 * MailChimp
 *
 * Template part for rendering MailChimp form
 *
 * @package WordPress
 */
global $globalSite;
/**
 * Don't even bother if we have no API Key or list ID
 */
if ( ! get_theme_mod( 'mailchimp_api_key' ) || ! get_theme_mod( 'mailchimp_list_id' ) ) {
	return;
}
/**
 * API Key
 * Account -> Extras -> API Keys
 *
 * @var MailChimp
 */
$apikey = get_theme_mod( 'mailchimp_api_key' );
/**
 * List ID
 * @var string
 */
$list_id = get_theme_mod( 'mailchimp_list_id' );
/**
 * Check if email address is submitted
 * and set $email var
 */
if ( ! empty( $_POST['email'] ) ) {
	$email = $_POST['email'];
} else {
	$email = '';
}
/**
 * Define vars for user feedback message and message class
 * @var string
 */
$globalSite['message'] = '';
$globalSite['class'] = '';
/**
 * If email is submitted add subscriber to the list
 * and build the thank you message
 */
if ( isset( $_POST['email'] ) && ! empty( $_POST['email'] ) ) {
	/**
	 * Get the MailChimp response
	 * @var array
	 */
	$response = get_mailchimp_response( $list_id, $email, $apikey );

	/**
	 * If status is not 'subscribed' we have error
	 */
	if ( $response['status'] !== 'subscribed' ) {
		// user feedback message
		$message = sprintf( __( 'Error: %s.', 'house' ), $response['title'] );
		// message class
		$class = 'error1';

	} else {

		// user feedback message
		if ( get_theme_mod( 'mailchimp_thank_you_message' ) ) {
			$message = get_theme_mod( 'mailchimp_thank_you_message' );
		} else {
			$message = __( 'Thank you for subscribing.', 'house' );
		}

		// message class
		$class = 'success';
	}
		$globalSite['class'] = $class;
		$globalSite['message'] = $message;
}
/**
 * If empty email field is submitted
 */
elseif ( isset( $_POST['email'] ) && empty( $_POST['email'] ) ) {

	// user feedback message
	if ( get_theme_mod( 'mailchimp_empty_field_message' ) ) {
		$message = get_theme_mod( 'mailchimp_empty_field_message' );
	} else {
		$message = __( 'Address field is empty. Please type your email address and try submitting again.', 'house' );
	}

	// message class
	$class = 'error2';
}

/**
 * We are sending action to #subscribe as it is the id of the whole section.
 * This way, upon submitting user will be scrolled down to the section
 * and see feedback messages.
 */
?>
<form action="#subscribe" method="POST" class="newsletter-form">
	<?php if ( isset( $_POST['email'] ) && ! empty( $_POST['email'] ) && ( ( ! isset( $response ) || $response['status'] !== 'subscribed' ) ) ) : ?>
		<input type="email" name="email" id="email" class="input input--primary" placeholder="<?php esc_attr_e( 'Email address', 'house' ); ?>" value="<?php echo $_POST['email']; ?>">
	<?php else : ?>
		<input type="email" name="email" id="email" class="input input--primary" placeholder="<?php esc_attr_e( 'Email address', 'house' ); ?>">
	<?php endif; // isset( $_POST['email'] ) && ! empty( $_POST['email'] ) ?>
	<div class="newsletter-inner-wrap">
		<button type="button" class="btn js-newsletter-close">
			<?php echo house_svg_icon( 'close' ); ?>
		</button>
		<input type="submit" value="Subscribe" name="submit" id="submit" class="btn btn--secondary">
	</div>
</form>
