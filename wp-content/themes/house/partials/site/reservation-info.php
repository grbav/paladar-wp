<?php
/**
 * Reservation Info
 *
 * Template part for rendering custom content for reservation header.
 * This can be populated from customizer.
 *
 * @package WordPress
 */

/**
 * Show Reservation Info section only if we have background image
 */
if ( get_theme_mod( 'reservation_bg_image' ) ) : ?>

	<div class="reservation-intro" style="background-image: url('<?php echo get_theme_mod( 'reservation_bg_image' ); ?>');">
		<button type="button" class="btn btn-close-reservation">
				<?php echo house_svg_icon( 'close' ); ?>
		</button>

		<div class="reservation-info">
			<?php echo house_svg_icon( 'restaurant' ); ?>

			<?php if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) :
					if ( get_theme_mod( 'reservation_info_title' ) ) : ?>
					<h1 class="reservation-heading"><?php echo get_theme_mod( 'reservation_info_title' ); ?></h1>
			<?php endif; endif; ?>

			<div class="reservation-desc">
				<?php
					/**
					 * Get phone number
					 */
					if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) :
						if ( get_theme_mod( 'reservation_phone_number' ) ) : ?>
							<span class="reservation-phone-number"><?php echo get_theme_mod( 'reservation_phone_number' ); ?></span>
					<?php endif; endif;
					/**
					 * Get the working time
					 */
					if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) :
						echo house_restaurant_open_times_text( '<p class="reservation-opening-hours">', '</p>' );
					endif; ?>
			</div><!-- /.reseravion-desc -->
		</div><!-- /.reservation-info -->
	</div><!-- /.reservation-intro -->

<?php endif; ?>