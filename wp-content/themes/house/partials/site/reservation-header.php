<?php
/**
 * Reservation Header
 *
 * Template part for rendering custom content for reservation header.
 * This can be populated from customizer.
 *
 * @package WordPress
 */
	
/**
 * Get custom header text from customizer
 */
if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) : ?>

	<div class="reservation-intro" style="background-image: url('<?php echo get_theme_mod( 'reservation_bg_image' ); ?>');">
		<button type="button" class="btn btn-close-reservation">
				<?php echo house_svg_icon( 'close' ); ?>
		</button>
		<div class="reservation-info">
			
				<?php echo house_svg_icon( 'restaurant' ); ?>

			<h1 class="reservation-heading"><?php _e( 'make a reservation', 'house' ); ?></h1><!-- /.reseravion-heading -->
			<div class="reservation-desc">
				<?php
					/**
					 * Get phone number
					 */
					if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
						if ( get_theme_mod( 'reservation_phone_number' ) ) :
							echo '<span>' . get_theme_mod( 'reservation_phone_number' ) . '</span>';
						endif;
					}
					/**
					 * Get the working time
					 */
					_e( 'Our restaurant is open for all guests every working day from ' . house_get_the_working_time( 'working_day_open_hours', 'working_day_open_minutes' ) . ' to ' . house_get_the_working_time( 'working_day_close_hours', 'working_day_close_minutes' ) . ' and weekends from ' . house_get_the_working_time( 'weekend_day_open_hours', 'weekend_day_open_minutes' ) . ' to ' . house_get_the_working_time( 'working_day_close_hours', 'working_day_close_minutes' ) . '. Bon appetite!', 'house' );
				?>
			</div><!-- /.reseravion-desc -->
		</div><!-- /.reservation-info -->
	</div><!-- /.reservation-intro -->

<?php endif; ?>