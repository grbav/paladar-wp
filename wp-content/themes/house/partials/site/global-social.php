<?php
/**
 * Global social
 *
 * Template part for rendering social profile links.
 *
 * @package WordPress
 */

/**
 * Get all populated social links from customizer
 *
 * There are other helper functions available, e.g. get specific
 * profile link or just url etc. @see inc/branding/social.php
 */
if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
	all_social_profiles( $class = '', $icon = false, $label = true, $before = '<li>', $after = '</li>' );

	/**
	 * Get the custom social profiles
	 */
	for ( $i = 1; $i <= 3; $i++ ) {
		if ( get_theme_mod( 'social_profiles_other_' . $i ) && get_theme_mod( 'social_profiles_other_' . $i . '_label' ) ) :

			$label = get_theme_mod( 'social_profiles_other_' . $i . '_label' );
			$url = get_theme_mod( 'social_profiles_other_' . $i );
		
			echo '<li><a href="' . $url . '" target="_blank">' . $label . '</a></li>';

		endif;
	}
}