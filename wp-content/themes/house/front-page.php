<?php
/**
 * Front page
 *
 * Page template for rendering home page.
 *
 * @package  WordPress
 */
get_header();

	// Frontpage set to Static page
	if ( get_option( 'show_on_front' ) == 'page' ) :
		/**
		 * Get flexible content fields if any
		 */
		if ( function_exists( 'get_field' ) && get_field( 'home_content_fields' ) ) :

			get_template_part( 'partials/flexible-templates/sections', 'home' );

		endif; // function_exists( 'get_field' ) && get_field( 'home_sections' )

	// Frontpage set to Latest posts
	elseif ( get_option( 'show_on_front' ) == 'posts' ) :

		if ( have_posts() ) :  ?>

			<div class="container">
				<section class="grid grid-blog">

					<?php while ( have_posts() ) : the_post();

						get_template_part( 'content', get_post_type() );

					endwhile; // have_posts() ?>

				</section>

				<?php house_simple_content_pagination( 'pagination' ); ?>

			</div><!-- /.container -->

		<?php endif; // have_posts()

	endif; // get_option( 'show_on_front' )

get_footer();