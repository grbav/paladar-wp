<?php
/**
 * Template Name: Gallery
 *
 * Page template for rendering gallery page.
 *
 * @package  WordPress
 */
get_header(); ?>

	<div class="container">

		<div class="filter">
			<nav id="filters">
				<div class="filter-responsive-element">
			  		<span class="filter-responsive-element-holder"><?php _e( 'All images', 'house' ); ?></span>
					<div class="filter-responsive-element-burger">
						<span></span>
					</div>
				</div>
			    <ul class="filter-nav">
			    	<?php
			    		/**
			    		 * Get terms
			    		 */
						$terms = [];
			    		if ( have_rows( 'gallery' ) ) :

							while ( have_rows( 'gallery' ) ) : the_row();

								$category = acf_sub_field( 'category', false );
								/**
								 * Get slug for term
								 */
								$terms[] = $category->slug;

							endwhile;

						endif; ?>
						<li>
							<button class="filter-button" data-filter="*">
								<span class="filter-name"><?php _e( 'All images', 'house' ); ?></span>
								<span class="filter-count"><?php echo count($terms); ?></span>
							</button>
					    </li>
						<?php
							/**
							 * Display term's slug,
							 * and number of posts in each term's slug
							 */
							$values = array_count_values($terms);
							foreach ( $values as $key => $value ) {

								$name = str_replace( '-', ' ', $key );

								echo '<li><button class="filter-button" data-filter=".' . $key . '"><span class="filter-name">' . $name . '</span><span class="filter-count">' . $value . '</span></button>';

							} ?>

				</ul>
			</nav>
		</div>

		<section class="grid grid-gallery popup-gallery">

			<?php get_template_part( 'content', 'gallery' ); ?>

		</section><!-- /.grid -->

	</div><!-- /.container -->

	<section class="restaurant-info">
		<div class="info-box info-box--white animation fade-in">
			<div class="info-box__icon">

				<?php echo house_svg_icon( 'clock' ); ?>

			</div>
			<?php echo house_restaurant_open_times_text( '<p class="reservation-opening-hours">', '</p>' ); ?>
		</div>
	</section>

<?php get_footer();