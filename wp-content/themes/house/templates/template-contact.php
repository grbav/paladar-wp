<?php
/**
 * Template Name: Contact
 *
 * Page template for rendering contact page.
 *
 * @package  WordPress
 */
get_header();

	/*
	 *	Get Featured Image array
	 */
	$image = acf_field( 'featured_image', false );
?>
	<div class="contact-wrap">
		<div class="contact-info">
			<address class="contact-address">
				<?php
					acf_field( 'restaurant_name', true, '<span class="contact-name">', '</span>' );
					acf_field( 'street_and_number', true, '<span>', '</span>' );
					acf_field( 'postal_code', true, '<span>', '</span>' );
					acf_field( 'city', true, '<span>', '</span>' );
					acf_field( 'country', true, '<span>', '</span>' );
					acf_field( 'phone', true, '<span class="contact-phone">', '</span>' );
					/*
					 *	Get e-mail address
					 */
					$email = acf_field( 'e-mail', false );
					echo '<a class="contact-email" href="mailto:' . $email . '">' . $email . '</a>';
				?>
			</address>

			<?php
				/*
				 *	Check if button is enabled
				 */
				if ( acf_field( 'add_button_for_google_map', false ) == true ) :

					/*
					 *	Button label
					 */
					if ( acf_field( 'button_label', false ) ) :
						$btn_label 	= acf_field( 'button_label', false );
					else :
						$btn_label 	= 'Get directions';
					endif;
					/**
					 * Get the button url
					 */
					$btn_url 	= acf_field( 'button_url', false );

					/*
					 *	Echo link for google map
					 */
					echo '<a class="contact-directions" href="' . $btn_url . '" target="_blank" >' . $btn_label . '</a>';

				endif;
			?>
		</div><!-- /.contact-info -->
		<div class="contact" style="background-image: url('<?php echo $image['url'] ?>');"></div>
	</div><!-- /.contact-wrap -->

	<div id="map" data-lng="<?php echo get_field( 'map' )['lng']; ?>" data-lat="<?php echo get_field( 'map' )['lat']; ?>"></div><!-- /#map -->

	<div class="container container--small">
		<?php
	        /**
	         * Get the WYSIWYG content
	         */
	        acf_field( 'content' );

			/**
	         * Get contact form if CF7 plugin is active and we have contact form ID
	         */
	        if ( house_is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) && get_field( 'contact_form_id' ) ) :

	            echo do_shortcode( '[contact-form-7 id="' . get_field( 'contact_form_id' ) . '"]' );

	        endif; // house_is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) && get_field( 'contact_form_id' )

		?>
	</div><!-- /.container -->

	<section class="restaurant-info">
		<div class="info-box info-box--white animation fade-in">
			<div class="info-box__icon">

				<?php echo house_svg_icon( 'clock' ); ?>

			</div>
			<?php echo house_restaurant_open_times_text( '<p class="reservation-opening-hours">', '</p>' ); ?>
		</div>
	</section>

<?php get_footer();