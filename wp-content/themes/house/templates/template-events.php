<?php
/**
 * Template Name: Events
 *
 * Page template for rendering page event page content.
 *
 * @package  WordPress
 */
get_header(); ?>

	<div class="container">

		<?php
			/**
			 * Get filters
			 */
			get_template_part( 'partials/navigations/events', 'filter' );
			/**
			 * All events
			 * @var obj
			 */
			$query_all = house_get_all_events_query();

			if ( $query_all->have_posts() ) : ?>
				<section id="container-events" class="grid">
					<?php
						while ( $query_all->have_posts() ) : $query_all->the_post();
							get_template_part( 'content', 'event' );
						endwhile;
					?>
				</section>
			<?php endif; wp_reset_query();
		?>

	</div><!-- /.container -->

<?php get_footer();