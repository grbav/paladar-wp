<?php
/**
 * Template Name: Menu Page
 *
 * Page template for rendering menu page content.
 *
 * @package  WordPress
 */
get_header();

	/**
	 * Get the main slider
	 */
	get_template_part( 'partials/sliders/main', 'slider' ); ?>

	<div class="container container--small" id="next">
		<article>
			<div class="entry-content">
				<div class="tabs">
					<ul class="tabs__nav">

						<?php
							/**
							 * Get tabs navigation
							 */
							get_template_part( 'partials/menu/menu-tab-navigation' );
						?>
					</ul>
					<?php
						/**
						 * Get tabs navigation
						 */
						get_template_part( 'partials/menu/menu-tab-content' );
					?>
				</div><!-- /.tabs -->
			</div><!-- /.entry-content -->
		</article><!-- / article -->
	</div><!-- /.container -->

	<section class="restaurant-info">
		<div class="info-box info-box--white animation fade-in in-view">
			<div class="info-box__icon">
				<?php echo house_svg_icon( 'clock' ); ?>
			</div>
			<?php echo house_restaurant_open_times_text( '<p class="reservation-opening-hours">', '</p>' ); ?>
		</div>
	</section>

<?php get_footer();