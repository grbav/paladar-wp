<?php
/**
 * The footer
 *
 * Contains footer content and the closing of the
 * body and html.
 *
 * @package WordPress
 */
global $globalSite;
?>
	
	</main><!-- #content -->

	<footer id="mainfooter" class="footer-main" role="contentinfo">
		<div class="footer-inner-wrap">
			<div class="footer-top">
				<div class="logo-address">
					<?php
						/**
						 * Get logo
						 */
						get_template_part( 'partials/site/global', 'branding' );
						/*
						 *	Check if address, phone or email are filled
						 */
						if ( get_theme_mod( 'footer_address_text' ) || get_theme_mod( 'footer_phone_text' ) || get_theme_mod( 'footer_email_text' ) ) :
			
							echo '<address class="contact-data">';
			
								/**
								 * Get footer address
								 */
								if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
									if ( get_theme_mod( 'footer_address_text' ) ) :
										echo '<span class="address">' . get_theme_mod( 'footer_address_text' ) . '</span>';
									endif;
								}
								/**
								 * Get footer phone
								 */
								if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
									if ( get_theme_mod( 'footer_phone_text' ) ) :
										echo '<span class="phone">' . get_theme_mod( 'footer_phone_text' ) . '</span>';
									endif;
								}
								/**
								 * Get footer email
								 */
								if ( get_theme_mod( 'footer_phone_text' ) && get_theme_mod( 'footer_email_text' ) ) :
			
									echo ' | ';
			
								endif;
								/**
								 * Get footer email
								 */
								if ( function_exists( 'get_theme_mod' ) || is_customize_preview() ) {
									if ( get_theme_mod( 'footer_email_text' ) ) :
										echo '<span class="email"><a href="mailto:' . get_theme_mod( 'footer_email_text' ) . '">' . get_theme_mod( 'footer_email_text' ) . '</a></span>';
									endif;
								}
								
			
							echo '</address>';
			
						endif;
					?>
			
				</div><!-- /.logo-address -->
			
				<?php
					/**
					 * Get main navigation with logo
					 */
					get_template_part( 'partials/navigations/footer' );
				?>
			</div>
			<div class="footer-bottom">
				<div class="address-holder"></div><!-- /.address-holder -->
			
				<div class="footer-bottom-right">
					<ul class="social-links v-line">
						<?php
							/**
							 * Get social icons
							 */
							get_template_part( 'partials/site/global', 'social' );
						?>
					</ul>
					<div class="wrap">
						<a href="<?php echo esc_url( 'www.thehouselondon.com/', 'http' ); ?>" target="_blank"><?php _e( 'Made at The House', 'house'); ?></a>
					</div><!-- /.wrap -->
				</div>
			</div>
		</div><!-- /.footer-inner-wrap -->
	</footer><!-- #mainfooter -->

<?php wp_footer(); ?>
</body>
</html>