<?php
/**
 * Format Image
 *
 * The default template for displaying content for image post format.
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 */
/**
 * Get category slug as class name for fillter
 */
$category_slug = get_the_category();
$slug = $category_slug[0]->slug;

/**
 * Get the featured image url
 * if one is set
 */
if ( has_post_thumbnail() ) { ?>
	<a href="<?php the_post_thumbnail_url(); ?>" class="grid-item <?php echo $slug; ?>"><img src="<?php the_post_thumbnail_url(); ?>" alt="" /></a>
<?php }