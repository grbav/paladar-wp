<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 */
get_header(); ?>

	<div class="container">
		<section class="grid grid-blog">

			<div class="grid-sizer"></div><!-- /.grid-sizer -->

			<?php if ( is_sticky() ) :

			  	/**
			  	 * Get Latest sticky post
			  	 */
				house_get_latest_sticky();

			endif;

			$query = house_get_blog_posts();

			if ( $query->have_posts() ) :?>

				<?php
				/* Start the Loop */
				while ( $query->have_posts() ) : $query->the_post();

					/**
					 * If we have template part for post format and
					 * we are on post format single.
					 *
					 * WordPress will first look for 'format-FORMAT_NAME.php',
					 * if none found fallback is 'format.php'. If that one is missing as well,
					 * it'll look for 'content.php'
					 */
					if ( has_post_format( get_post_format() ) ) {
						get_template_part( 'format', get_post_format() );
					}
					/**
					 * If we have template part for custom post type and
					 * we are on custom post type single.
					 *
					 * WordPress will first look for 'content-CPT_NAME.php',
					 * if none found fallback is 'content.php'.
					 */
					elseif ( post_type_exists( get_post_type() ) ) {
						get_template_part( 'content', get_post_type() );
					}
					/**
					 * If, in any case, none from above applies
					 */
					else {
						get_template_part( 'content' );
					}

				endwhile; ?>

			<?php else : ?>
				<?php get_template_part( 'content', 'none' ); ?>
			<?php endif; // end have_posts() check ?>

		</section>
		<?php
			/**
			 * Get 'posts_per_page' setting
			 * @var int
			 */
			$posts_per_page = $query->query['posts_per_page'];
			/**
			 * Get total number of posts found by query
			 * @var int
			 */
			$found_posts = $query->found_posts;
			/**
			 * Show load more button only if
			 * number of found posts if larger than 'posts_per_page'.
			 * We don't want button that does nothin'.
			 */
			if ( $found_posts > $posts_per_page ) : ?>

			<div class="text-center btn-load-more">
				<a href="javascript:;" class="btn btn--primary" id="more_posts"><?php _e( 'Load more', 'house' ); ?></a>
			</div>
			<?php endif; //$found_posts > $posts_per_page
		?>
	</div><!-- /.container -->

<?php get_footer(); ?>