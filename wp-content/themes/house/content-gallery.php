<?php
/**
 * The default template for displaying content events.
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */


if ( have_rows( 'gallery' ) ) :

	while ( have_rows( 'gallery' ) ) : the_row();

		/**
		 * Translators: field id, echo (false returns value), before, after
		 */
		$image = acf_sub_field( 'photo', false );

		if ( $image ) :
			/**
			 * Prepare sizes
			 * @var string
			 */
			$full = $image['url'];

			/**
			 * Get the category
			 */
			$category = acf_sub_field( 'category', false );
			
			echo '<a href="' . $full . '" class="grid-item ' . $category->slug . '"><img src="' . $full . '" alt="" /></a>';
		
		endif; // $image ?>

	<?php endwhile;

endif;