<?php
/**
 * Event
 *
 * The Template for displaying all single event posts.
 *
 * @package WordPress
 */
get_header(); ?>

	<div class="container">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="entry-content">
				<div class="event-item-full">

					<?php
						/**
						 * Get the featured image
						 * if one is set
						 */
						if ( has_post_thumbnail() ) :

							echo '<div class="event-item-image" style="background-image: url(' . get_the_post_thumbnail_url() . ');"></div><!-- /.event-item-image -->';

						endif;
					?>

		  			<div class="event-info">
			  			<div class="event-time">
			  				<time><?php echo get_the_date(); ?></time>
							<time>
								<?php
									/**
									 * Get starting time, which is the post publish time
									 */
									echo get_post_time( 'h:i A' );
									/**
									 * Get ending time, if any
									 */
									acf_field( 'end_time', true, ' - ' );
								?>
							</time>
			  			</div><!-- /.event-date -->
			  			<div class="event-icon">

			  				<?php echo house_svg_icon( 'cutlery' ); ?>

			  			</div><!-- /.event-icon -->

			  			<?php
			  				/**
			  				 * Get event's title
			  				 */
			  				the_title( '<h2 class="event-title">', '</h2>' );

			  				/**
			  				 * Get location (taxonomies)
			  				 */
			  				acf_field( 'event_location', true, '<small class="event-location">', '</small>' );

							/**
							 * Get flexible content fields
							 *
							 * @uses ACF Pro plugin
							 */
							if ( function_exists( 'get_field' ) && get_field( 'event_content_fields' ) ) :

								get_template_part( 'partials/flexible-templates/sections', 'event' );

							endif; ?>

							<div class="share-attend">
								<?php if ( get_field( 'attend_url' ) ) : ?>
			  						<div class="attend">
										<?php
										/**
										 * Get button label
										 */
										if ( get_field( 'attend_label' ) ) :

											$btn_label = get_field( 'attend_label' );

										else :

											$btn_label = __( 'Attend', 'house' );

										endif;

										echo '<a href="' . get_field( 'attend_url' ) . '" class="btn btn--secondary">' . $btn_label . '</a>'; ?>
									</div><!-- /.attend -->
								<?php endif; // get_field( 'attend_url' )

									/**
									 * Get share links
									 */
									echo house_share_post( 'event' );
								?>
							</div>


		  			</div><!-- /.event-info -->
		  		</div><!-- /.event-item -->
			</div><!-- /.entry-content -->
		</article><!-- / article -->
	</div><!-- /.container -->

<?php get_footer();