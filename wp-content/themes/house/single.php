<?php
/**
 * Single
 *
 * The Template for displaying all single posts.
 *
 * @package WordPress
 */
get_header(); ?>

	<div class="container container--small">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<div class="entry-meta">
					<span class="posted-on">
						<?php
							$archive_year  = get_the_time('Y');
							$archive_month = get_the_time('m');
							$archive_day   = get_the_time('d');
						?>
						<time class="entry-date published" datetime="<?php echo get_the_date( 'c' ); ?>"><?php echo get_the_date(); ?></time>
					</span>
					<span class="byline">
						<span class="author vcard">
							<span>&#47;</span> <?php printf( __( 'By %s', 'house' ),  get_the_author() ); ?>
						</span>
					</span>
				</div><!-- /.entry-meta -->
				<?php
					the_title( '<h1 class="entry-title">', '</h1>' );
					if ( has_post_thumbnail() ) {
						/**
						 * Get width and height for featured image
						 * @var array
						 */
						$featured_image = house_get_featured_image_size();
						/**
						 * Make the difference for different image orientation
						 */
						if ( $featured_image['width'] > $featured_image['height'] ) :
							// This image is landscape oriented
							$src = get_the_post_thumbnail_url( get_the_ID(), 'full' );
							$class = '';
						elseif ( $featured_image['width'] < $featured_image['height'] ) :
							// This image is portrait oriented
							$src = get_the_post_thumbnail_url( get_the_ID(), 'large' );
							$class = 'portrait';
						endif; // $featured_image['width'] > $featured_image['height']

						echo '<div class="wp-post-image ' . $class . '" style="background-image: url(' . $src . ');"></div>';
					}
				?>
			</header>

			<div class="entry-content">

				<?php
				// start loop
				while ( have_posts() ) : the_post();

					/**
					 * Get flexible content fields if any,
					 * otherwise get regular content
					 *
					 * @uses ACF Pro plugin
					 */
					if ( function_exists( 'get_field' ) && get_field( 'blog_post_content_fields' ) ) :

						get_template_part( 'partials/flexible-templates/sections', 'blog-post' );

					else :

						get_template_part( 'partials/content/post-content' );

					endif; // get_field( 'content_fields' ) ?>

			</div><!-- /.entry-content -->

			<footer class="entry-footer">

				<span class="tags-links">
					<span><?php _e( 'Tags', 'house' ); ?> </span>
					<?php
						echo get_the_tag_list( '', ' / ' );
					?>
				</span>

				<?php
					/**
					 * Get share links
					 */
					echo house_share_post();
				?>
			</footer><!-- /.entry-footer -->

		</article>
	</div><!-- /.container container--small -->

	<?php
	/**
	 * Always reset query
	 */
	wp_reset_query();

		/**
		 * Related Articles
		 *
		 * Get related posts query
		 */
		$query = house_get_related_posts();

		if ( $query->have_posts() ) : ?>

			<div class="related-articles">
				<div class="container">
					<h2><?php _e( 'Related articles', 'house' ); ?></h2>
					<div class="grid grid-blog">

					<?php while ( $query->have_posts() ) : $query->the_post();

						get_template_part( 'content', 'related' );

					endwhile; // $query->have_posts() ?>

					</div><!-- /.grid -->
				</div><!-- /.container -->
			</div><!-- /.related-articles -->

		<?php endif; // $query->have_posts()
		/**
		 * Always reset query
		 */
		wp_reset_query(); ?>

		<div class="spacer"></div><!-- /.spacer -->

			<?php
			/**
			 * Get the comments list and form
			 * @link https://developer.wordpress.org/reference/functions/comments_template/
			 */
			comments_template( '', true );

		endwhile; // end of the loop.

get_footer();