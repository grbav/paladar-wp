<?php
/**
 * The default template for displaying content buzz.
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
?>

<div class="grid-item">
	<div class="article">
		<div class="entry-header">
			<?php
				/**
				 * Get the featured image
				 * if one is set
				 */
				if ( has_post_thumbnail() ) :

					echo '<a href="' . get_permalink() . '">' . get_the_post_thumbnail( get_the_ID(), 'full', array( 'class' => 'wp-post-image' ) )  . '</a>';
				
				endif;
			?>
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		</div><!-- /.event-item-image -->
		<div class="entry-footer">
			<span class="posted-on">
				<time class="entry-date" datetime="<?php echo get_the_date( 'c' ); ?>"><?php echo get_the_date(); ?></time>
			</span>
		</div>
		<div class="entry-content">
			<?php acf_field( 'paragraph', true, '<p>', '...</p>' ); ?>
			<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'house' ); ?></a>
		</div><!-- /.entry-content -->
	</div>
</div><!-- /.grid-item -->