<?php
/**
 * Comments
 *
 * Template part for rendering comments list and comment form. Used on single posts - insights.
 *
 * @package WordPress
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area comments mt+ mb++">

	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title mb">
			<?php
				$comments_number = get_comments_number();
				if ( 1 === $comments_number ) {
					/* translators: %s: post title */
					printf( __( 'One comment', 'comments title', 'house' ) );
				} else {
					printf(
						/* translators: 1: number of comments, 2: post title */
						_nx(
							'%1$s comment',
							'%1$s comments',
							$comments_number,
							'comments title',
							'house'
						),
						number_format_i18n( $comments_number )
					);
				}
			?>
		</h2>

		<?php the_comments_navigation(); ?>

		<ol class="commentlist">
			<?php
				/**
				 * List the comments with custom markup for the actual
				 * comment. @see house_comment()
				 */
				wp_list_comments( array(
					'style'             => 'ol',
					'type'              => 'comment',
					'reply_text'        => __( 'Reply', 'house' ),
					'reverse_top_level' => false,
					'short_ping'        => false,
					'avatar_size'       => 53,
					'callback'          => 'house_comment',
				) );
			?>
		</ol><!-- commentlist -->

		<?php the_comments_navigation(); ?>

	<?php endif; // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'house' ); ?></p>
	<?php endif; ?>

	<?php
		/**
		 * Custom comment form
		 */
		if ( function_exists( 'house_comment_form' ) ) {
			house_comment_form();
		}
	?>

</div><!-- comments-area comments mt+ mb++ -->
