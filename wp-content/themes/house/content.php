<?php
/**
 * The default template for displaying content.
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Advanced Custom Fields PRO
 */
if ( is_single() ) :
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * Get post title
		 */
		get_template_part( 'partials/content/title-singular' );

		/**
		 * Get flexible content fields if any,
		 * otherwise get regular content
		 *
		 * @uses ACF Pro plugin
		 */
		if ( function_exists( 'get_field' ) && get_field( 'all_content_fields' ) ) :

			get_template_part( 'partials/flexible-templates/sections' );

		else :

			get_template_part( 'partials/content/post-content' );

		endif; // get_field( 'content_fields' )

		/**
		 * Get post author meta
		 */
		get_template_part( 'partials/meta/post-author' );
	?>

</article><!-- #post -->
<?php
/**
 * On search archive
 */
elseif ( is_search() ) :

	/**
	 * Get archive blog content
	 */
	get_template_part( 'partials/content/archive-blog' );

/**
 * Any else condition
 */
else :

	/**
	 * Get archive blog content
	 */
	get_template_part( 'partials/content/archive-blog' );

endif;